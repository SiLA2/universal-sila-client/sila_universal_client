package com.silastandard.silatest.server;

import com.google.common.io.ByteStreams;
import com.silastandard.silatest.common.Constant;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerOuterClass;
import sila2.org.silastandard.usc.test.imageservice.v1.ImageServiceGrpc;
import sila2.org.silastandard.usc.test.imageservice.v1.ImageServiceOuterClass;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;

import java.io.IOException;
import java.util.Objects;

@Slf4j
public class ImageService extends ImageServiceGrpc.ImageServiceImplBase {
    private final BinaryDatabase binaryDatabase;

    public ImageService(@NonNull final BinaryDatabase binaryDatabase) {
        this.binaryDatabase = binaryDatabase;
    }

    @Override
    public void getImage(ImageServiceOuterClass.GetImage_Parameters request, StreamObserver<ImageServiceOuterClass.GetImage_Responses> responseObserver) {
        try {
            final AuthorizationServiceOuterClass.Metadata_AccessToken accessToken = Constant.ACCESS_TOKEN_CTX_KEY.get();
            log.info("getImage: Received Access Token from Metadata [{}]", accessToken);
            final LockControllerOuterClass.Metadata_LockIdentifier lockIdentifier = Constant.LOCK_IDENTIFIER_CTX_KEY.get();
            log.info("getImage: Received Lock Identifier from Metadata [{}]", lockIdentifier);
            CoreFeatureValidators.LockValidator.checkUnlockTemporary(lockIdentifier.getLockIdentifier());
            CoreFeatureValidators.AccessTokenValidator.checkAccessToken(accessToken.getAccessToken().getValue());
            byte[] fileContent = ByteStreams.toByteArray(
                    Objects.requireNonNull(
                            ImageService.class.getResourceAsStream("/com/silastandard/silatest/example.jpg")
                    )
            );
            //final String base64 = Base64.getEncoder().encodeToString(fileContent);
            responseObserver.onNext(ImageServiceOuterClass.GetImage_Responses.newBuilder().setImage(SiLABinary.fromBytes(fileContent)).build());
            responseObserver.onCompleted();
        } catch (IOException e) {
            responseObserver.onError(e);
        }

    }
}
