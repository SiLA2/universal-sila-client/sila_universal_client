package com.silastandard.silatest.server;

import io.grpc.stub.StreamObserver;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.usc.test.binaryservice.v1.BinaryServiceGrpc;
import sila2.org.silastandard.usc.test.binaryservice.v1.BinaryServiceOuterClass;

@Slf4j
@NoArgsConstructor
public class BinaryService extends BinaryServiceGrpc.BinaryServiceImplBase {
    @Override
    public void testList(BinaryServiceOuterClass.TestList_Parameters request, StreamObserver<BinaryServiceOuterClass.TestList_Responses> responseObserver) {
        responseObserver.onNext(
                BinaryServiceOuterClass.TestList_Responses
                        .newBuilder()
                        .addAllB(request.getAList())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void testParams(BinaryServiceOuterClass.TestParams_Parameters request, StreamObserver<BinaryServiceOuterClass.TestParams_Responses> responseObserver) {
        responseObserver.onNext(
                BinaryServiceOuterClass.TestParams_Responses
                        .newBuilder()
                        .setRA(request.getA())
                        .setRB(request.getB())
                        .build()
        );
        responseObserver.onCompleted();
    }
}
