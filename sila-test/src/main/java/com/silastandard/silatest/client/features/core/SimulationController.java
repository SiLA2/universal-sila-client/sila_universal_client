package com.silastandard.silatest.client.features.core;

import io.grpc.Channel;
import sila2.org.silastandard.core.simulationcontroller.v1.SimulationControllerGrpc;
import sila2.org.silastandard.core.simulationcontroller.v1.SimulationControllerOuterClass;

public class SimulationController {
    private final SimulationControllerGrpc.SimulationControllerBlockingStub stub;

    public SimulationController(Channel channel) {
        stub = SimulationControllerGrpc.newBlockingStub(channel);
    }

    public void startSimulationMode() {
        SimulationControllerOuterClass.StartSimulationMode_Responses responses =
                stub.startSimulationMode(SimulationControllerOuterClass.StartSimulationMode_Parameters
                        .newBuilder()
                        .build()
                );
    }

    public void startRealMode() {
        SimulationControllerOuterClass.StartRealMode_Responses responses =
                stub.startRealMode(SimulationControllerOuterClass.StartRealMode_Parameters.newBuilder().build());
    }

    public boolean isSimulationMode() {
        SimulationControllerOuterClass.Get_SimulationMode_Responses responses =
                stub.getSimulationMode(
                        SimulationControllerOuterClass.Get_SimulationMode_Parameters.newBuilder().build()
                );
        return responses.getSimulationMode().getValue();
    }
}
