package com.silastandard.silatest.client.features.core.commands;

import io.grpc.Channel;
import sila2.org.silastandard.core.commands.pausecontroller.v2.PauseControllerGrpc;
import sila2.org.silastandard.core.commands.pausecontroller.v2.PauseControllerOuterClass;

import java.util.Iterator;

public class PauseController {
    private final PauseControllerGrpc.PauseControllerBlockingStub stub;

    public PauseController(Channel channel) {
        stub = PauseControllerGrpc.newBlockingStub(channel);
    }

    public void pause() {
        PauseControllerOuterClass.Pause_Responses responses =
                stub.pause(PauseControllerOuterClass.Pause_Parameters.newBuilder().build());
    }

    public void resume() {
        PauseControllerOuterClass.Resume_Responses responses =
                stub.resume(PauseControllerOuterClass.Resume_Parameters.newBuilder().build());
    }

    public void getPaused() {
        // todo make observable
        Iterator<PauseControllerOuterClass.Subscribe_PausedCommands_Responses> responses =
                stub.subscribePausedCommands(PauseControllerOuterClass.Subscribe_PausedCommands_Parameters.newBuilder().build());
    }
}
