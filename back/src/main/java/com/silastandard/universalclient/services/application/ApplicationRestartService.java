package com.silastandard.universalclient.services.application;


import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;

/**
 * Inspired from https://github.com/spring-cloud/spring-cloud-commons/blob/main/spring-cloud-context/src/main/java/org/springframework/cloud/context/restart/RestartEndpoint.java
 */
@Slf4j
@Service
public class ApplicationRestartService implements ApplicationListener<ApplicationReadyEvent> {
    private ConfigurableApplicationContext context;
    @Getter
    private SpringApplication application;
    private String[] args;
    private ApplicationReadyEvent event;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        this.event = event;
        if (this.context == null) {
            this.context = this.event.getApplicationContext();
            this.args = this.event.getArgs();
            this.application = this.event.getSpringApplication();
        }
    }

    @WriteOperation
    public Object restart() {
        Thread thread = new Thread(this::safeRestart);
        thread.setDaemon(false);
        thread.start();
        return Collections.singletonMap("message", "Restarting");
    }

    private Boolean safeRestart() {
        try {
            doRestart();
            log.info("Restarted");
            return true;
        }
        catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.info("Could not doRestart", e);
            }
            else {
                log.info("Could not doRestart: " + e.getMessage());
            }
            return false;
        }
    }

    // @ManagedOperation
    public synchronized ConfigurableApplicationContext doRestart() {
        if (this.context != null) {
            this.application.setEnvironment(this.context.getEnvironment());
            close();
            // If running in a webapp then the context classloader is probably going to
            // die so we need to revert to a safe place before starting again
            overrideClassLoaderForRestart();
            this.context = this.application.run(this.args);
        }
        return this.context;
    }

    private void close() {
        ApplicationContext context = this.context;
        while (context instanceof Closeable) {
            try {
                ((Closeable) context).close();
            }
            catch (IOException e) {
                log.error("Cannot close context: " + context.getId(), e);
            }
            context = context.getParent();
        }
    }

    // @ManagedAttribute
    public boolean isRunning() {
        if (this.context != null) {
            return this.context.isRunning();
        }
        return false;
    }

    private void overrideClassLoaderForRestart() {
        ClassUtils.overrideThreadContextClassLoader(this.application.getClass().getClassLoader());
    }
}