package com.silastandard.universalclient.services.impl;

import com.google.common.collect.ImmutableSet;
import com.silastandard.universalclient.common.models.db.Call;
import com.silastandard.universalclient.common.models.db.Client;
import com.silastandard.universalclient.common.models.db.Server;
import com.silastandard.universalclient.repositories.ClientRepository;
import com.silastandard.universalclient.repositories.ServerRepository;
import com.silastandard.universalclient.services.ClientService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ServerRepository serverRepository;

    @Autowired
    public ClientServiceImpl(
            @NonNull final ClientRepository clientRepository,
            @NonNull final ServerRepository serverRepository
    ) {
        this.clientRepository = clientRepository;
        this.serverRepository = serverRepository;
    }

    @Override
    public Client save(@NonNull final Client client) {
        return this.clientRepository.save(client);
    }

    @Override
    public Optional<Client> getClient(@NonNull final String uuid) {
        return this.clientRepository.findById(uuid);
    }

    @Override
    public Set<Client> getAllClients() {
        return ImmutableSet.copyOf(this.clientRepository.findAll());
    }

    @Override
    public Set<Client> getAllServerClients(@NonNull final Server server) {
        final Set<Client> clients = new HashSet<>();
        clientRepository.findAll().forEach(client -> {
            if (client.getServers().contains(server)) clients.add(client);
        });
        return clients;
    }

    @Override
    public boolean deleteClientCall(@NonNull final Call call, @NonNull final Client client) {
        final Set<Call> clientCalls = client.getCalls();
        clientCalls.remove(call);
        final Client savedClient = this.clientRepository.save(client);
        return !savedClient.getCalls().contains(call);
    }

    @Override
    public boolean deleteClientServer(@NonNull final Server server, @NonNull final Client client) {
        final Set<Server> clientServers = client.getServers();
        clientServers.remove(server);
        final Client savedClient = this.clientRepository.save(client);
        return !savedClient.getServers().contains(server);
    }

    @Override
    public Server addBookmarkedServers(@NonNull final Client client, @NonNull final Server server) {
        final Set<Server> clientServers = client.getServers();
        clientServers.add(server);
        client.setServers(clientServers);
        this.save(client);
        final Set<Client> serverClients = server.getClients();
        serverClients.add(client);
        server.setClients(serverClients);
        return this.serverRepository.save(server);
    }

    @Override
    public Server deleteBookmarkedServers(Client client, Server server) {
        final Set<Server> clientServers = client.getServers();
        clientServers.remove(server);
        client.setServers(clientServers);
        this.save(client);
        final Set<Client> serverClients = server.getClients();
        serverClients.remove(client);
        server.setClients(serverClients);
        return this.serverRepository.save(server);
    }

    @Override
    public Client createOrRetrieveClient(@NonNull final String uuid) {
        final Client client;
        final Optional<Client> optionalClient = getClient(uuid);

        if (optionalClient.isPresent()) {
            return optionalClient.get();
        }
        client = new Client();
        client.setClientUuid(uuid);
        // todo: get client name from method parameters
        client.setName("test-client");
        client.setServers(new HashSet<>());
        return clientRepository.save(client);
    }
}
