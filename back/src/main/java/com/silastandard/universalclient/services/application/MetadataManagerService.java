package com.silastandard.universalclient.services.application;

import com.jayway.jsonpath.PathNotFoundException;
import com.silastandard.universalclient.common.models.sila.SiLACallModel;
import com.silastandard.universalclient.utils.UscJsonUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sila_java.library.core.models.Feature;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.SiLACall;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
public class MetadataManagerService {

    private final Map<UUID, Map<String, Set<String>>> fcpAffectedByMetadata = new HashMap<>();

    /**
     * Return a immutable Map of fcp affected by metadata of a server
     *
     * @param serverUUID  The server UUID
     * @param fqiMetadata The fqi metadata
     * @return a modifiable Map of fcp affected by metadata of a server
     */
    public Set<String> getFcpAffectedByMetadataForMetadata(
            @NonNull final UUID serverUUID, @NonNull final String fqiMetadata
    ) {
        return Collections.unmodifiableSet(
                this.getFcpAffectedByMetadataForServer(serverUUID).getOrDefault(fqiMetadata, Collections.emptySet())
        );
    }

    /**
     * Return a immutable Set of fully qualified metadata identifier of a server
     *
     * @param serverUUID  The server UUID
     * @return a immutable Set of fully qualified metadata identifier of a server
     */
    public Set<String> getServerMetadata(
            @NonNull final UUID serverUUID
    ) {
        return Collections.unmodifiableSet(
                this.getFcpAffectedByMetadataForServer(serverUUID).keySet()
        );
    }

    public void onServerAddedCacheAffectedFPC(@NonNull final Server server) {
        server.getFeatures().forEach(feature -> {
            feature.getMetadata().forEach(metadata -> {
                final String fullyQualifiedMetadataIdentifier = getFullyQualifiedMetadataIdentifier(feature, metadata);
                final SiLACallModel siLACall = createFPCAffectedMetadataCallModelFromSiLA(metadata, feature, server);
                try {
                    this.addFCPAffectedByMetadataEntry(
                            server.getConfiguration().getUuid(),
                            feature,
                            metadata,
                            ServerManager.getInstance().getServerCallManager()
                                    .runAsync(ExecutableServerCall
                                            .newBuilder(siLACall.toSiLACall())
                                            .withTimeout(Duration.ofSeconds(10))
                                            .build())
                                    .get()
                    );
                    log.debug("Added Affected FPC list for Metadata [{}]", fullyQualifiedMetadataIdentifier);
                } catch (final ExecutionException | InterruptedException | RuntimeException e) {
                    // todo handle exception properly
                    log.warn(
                            "Failed to add FCPA Affected By Metadata {} for server {}",
                            fullyQualifiedMetadataIdentifier, server.getConfiguration().getUuid(), e
                    );
                }
            });
        });
    }

    public void onServerRemovedAffectedFPC(@NonNull final UUID serverUUID) {
        this.fcpAffectedByMetadata.remove(serverUUID);
    }

    /**
     * Private utility that return a mutable Map of fcp affected by metadata of a server
     * @param serverUUID The server UUID
     * @return a modifiable Map of fcp affected by metadata of a server
     */
    private Map<String, Set<String>> getFcpAffectedByMetadataForServer(@NonNull final UUID serverUUID) {
        return this.fcpAffectedByMetadata.getOrDefault(serverUUID, new HashMap<>());
    }

    private void addFCPAffectedByMetadataEntry(
            @NonNull final UUID serverUUID,
            @NonNull final Feature feature,
            @NonNull final Feature.Metadata metadata,
            @NonNull final String result
    ) {
        try {
            final Set<String> fpcAffectedByMetadata = UscJsonUtils.extractValueFieldFromSet(result);
            this.fcpAffectedByMetadata.computeIfAbsent(serverUUID, uuid -> new HashMap<>())
                    .computeIfAbsent(getFullyQualifiedMetadataIdentifier(feature, metadata), s -> new HashSet<>())
                    .addAll(fpcAffectedByMetadata);
        } catch (final PathNotFoundException e) {
            log.warn("Get_FPCAffectedByMetadata result raised PathNotFoundException " +
                    "when searching 'value' field within {}", result);
            throw new RuntimeException(e);
        }
    }

    private static SiLACallModel createFPCAffectedMetadataCallModelFromSiLA(
            @NonNull final sila_java.library.core.models.Feature.Metadata metadata,
            @NonNull final sila_java.library.core.models.Feature feature,
            @NonNull final sila_java.library.manager.models.Server server
    ) {
        SiLACallModel callModel = new SiLACallModel();
        callModel.setCallId(metadata.getIdentifier());
        callModel.setType(SiLACall.Type.GET_FCP_AFFECTED_BY_METADATA);
        callModel.setFullyQualifiedFeatureId(getFullyQualifiedFeatureIdentifier(feature));
        callModel.setServerId(UUID.fromString(server.getConfiguration().getUuid().toString()));
        return callModel;
    }

    // todo move to sila_java
    private static String getFullyQualifiedMetadataIdentifier(
            @NonNull final Feature feature, @NonNull final Feature.Metadata metadata
    ) {
        return String.join("/", getFullyQualifiedFeatureIdentifier(feature), "Metadata", metadata.getIdentifier());
    }

    // todo move to sila_java
    private static String getFullyQualifiedFeatureIdentifier(@NonNull final Feature feature) {
        final int endOfMajorVersion = feature.getFeatureVersion().indexOf(".");
        final String majorVersion = "v" + ((endOfMajorVersion < 0) ?
                feature.getFeatureVersion() :
                feature.getFeatureVersion().substring(0, endOfMajorVersion));
        return String.join("/", feature.getOriginator(), feature.getCategory(), feature.getIdentifier(), majorVersion);
    }
}
