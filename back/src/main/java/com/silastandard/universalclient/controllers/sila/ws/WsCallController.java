package com.silastandard.universalclient.controllers.sila.ws;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.protobuf.DynamicMessage;
import com.silastandard.universalclient.common.models.sila.*;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.CallListener;
import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.CallStarted;
import sila_java.library.manager.models.SiLACall;

import java.util.*;

@Slf4j
@Controller
public class WsCallController implements CallListener {
    private final WebSocketMessenger websocketMessenger;
    // todo make threadsafe
    @Getter
    private final Map<UUID, CachedCall> callsCache = new HashMap<>();
    // todo make threadsafe
    @Getter
    private final Map<UUID, Multimap<String, UUID>> callsTypeServerMap = new HashMap<>();

    // todo remove entry from cache after a specific delay

    @Autowired
    public WsCallController(@NonNull final WebSocketMessenger websocketMessenger) {
        this.websocketMessenger = websocketMessenger;
        ServerManager.getInstance().getServerCallManager().addListener(this);
    }

    // todo handle server remove/update

    private static String getFullyQualifiedIdentifierFromCall(SiLACall siLACall) {
        return (getFullyQualifiedIdentifierFromIds(siLACall.getServerId(), siLACall.getFullyQualifiedFeatureId(), siLACall.getCallId()));
    }

    public static String getFullyQualifiedIdentifierFromIds(UUID serverId, String featureId, String callId) {
        return serverId.toString() + '/' + featureId + '/' + callId;
    }

    @Override
    public void onStart(
            @NonNull final CallStarted callInProgress
    ) {
        log.debug("On call progress " + callInProgress.getSiLACall().getIdentifier());
        this.callsCache.put(callInProgress.getSiLACall().getIdentifier(), new CachedCall(callInProgress.getSiLACall()));
        final UUID serverId = callInProgress.getSiLACall().getServerId();
        // todo make thread safe
        this.callsTypeServerMap.putIfAbsent(serverId, HashMultimap.create());
        final Multimap<String, UUID> strings = this.callsTypeServerMap.get(serverId);
        if (strings != null) {
            strings.put(getFullyQualifiedIdentifierFromCall(callInProgress.getSiLACall()), callInProgress.getSiLACall().getIdentifier());
        } else {
            log.warn("No cached call type for server {}", serverId);
        }
        this.websocketMessenger.send("call/progress", callInProgress, null);
    }

    @Override
    public void onComplete(@NonNull final CallCompleted callCompleted) {
        log.debug("On call complete  " + callCompleted.getSiLACall().getIdentifier());
        this.callsCache.remove(callCompleted.getSiLACall().getIdentifier());
        final UUID serverId = callCompleted.getSiLACall().getServerId();
        final Multimap<String, UUID> strings = this.callsTypeServerMap.get(serverId);
        if (strings != null) {
            strings.remove(getFullyQualifiedIdentifierFromCall(callCompleted.getSiLACall()), callCompleted.getSiLACall().getIdentifier());
            if (strings.isEmpty()) {
                this.callsTypeServerMap.remove(serverId);
            }
        }
        this.websocketMessenger.send("call/complete", callCompleted, null);
    }

    @Override
    public void onError(@NonNull final CallErrored callErrored) {
        // todo synchronize with the persistent error in executeGrpcSiLACallThroughServerManager
        //  the error message are not necessarily the same
        log.debug("On call error " + callErrored.getSiLACall().getIdentifier());
        this.callsCache.remove(callErrored.getSiLACall().getIdentifier());
        final UUID serverId = callErrored.getSiLACall().getServerId();
        final Multimap<String, UUID> strings = this.callsTypeServerMap.get(serverId);
        if (strings != null) {
            strings.remove(getFullyQualifiedIdentifierFromCall(callErrored.getSiLACall()), callErrored.getSiLACall().getIdentifier());
            if (strings.isEmpty()) {
                this.callsTypeServerMap.remove(serverId);
            }
        }
        this.websocketMessenger.send("call/error", callErrored, null);
    }

    @Override
    public void onObservablePropertyUpdate(SiLACall baseCall, String value) {
        log.debug("On observable property update " + baseCall.getIdentifier());
        this.callsCache.putIfAbsent(baseCall.getIdentifier(), new CachedCall(baseCall));
        this.callsCache.get(baseCall.getIdentifier()).setLastPropertyValue(value);
        this.websocketMessenger.send("call/propertyUpdate", new ObservablePropertyResponse(baseCall, value), null);
    }

    @Override
    public void onObservableCommandInit(SiLACall baseCall, SiLAFramework.CommandConfirmation command) {
        log.debug("Observable command init " + command.getCommandExecutionUUID());
        this.callsCache.putIfAbsent(baseCall.getIdentifier(), new CachedCall(baseCall));
        this.callsCache.get(baseCall.getIdentifier()).setLastCommandConfirmation(command);
        this.websocketMessenger.send("call/init", new ObservableCommandInit(baseCall, command), null);
    }

    @Override
    public void onObservableCommandExecutionInfo(SiLACall baseCall, SiLAFramework.ExecutionInfo executionInfo) {
        log.debug("Observable command exec info " + executionInfo.getCommandStatus());
        this.callsCache.putIfAbsent(baseCall.getIdentifier(), new CachedCall(baseCall));
        this.callsCache.get(baseCall.getIdentifier()).setLastCommandExecutionInfo(executionInfo);
        this.websocketMessenger.send("call/exec-info", new ObservableExecutionInfo(baseCall, executionInfo), null);
    }

    @Override
    public void onObservableIntermediateResponse(SiLACall baseCall, DynamicMessage responseType) {
        log.debug("Observable command intermediate response " + responseType.toString());
        this.callsCache.putIfAbsent(baseCall.getIdentifier(), new CachedCall(baseCall));
        this.callsCache.get(baseCall.getIdentifier()).setLastIntermediateResponse(responseType);
        this.websocketMessenger.send("call/intermediate", new ObservableIntermediateResponse(baseCall, responseType), null);
    }
}
