package com.silastandard.universalclient.controllers.sila.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static sila_java.library.core.utils.Utils.cleanupXMLString;
import static sila_java.library.core.utils.Utils.validateFeatureXML;

@Slf4j
@RestController
@RequestMapping("/xml")
public class XmlValidationController {

    @PostMapping("/validate/feature")
    public ResponseEntity<?> validateXml(@RequestBody String xml) {
        try {
            validateFeatureXML(cleanupXMLString(xml));
            return ResponseEntity.ok(true);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        } catch (Exception e) {
            log.info("An error occurred during XML validation", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("An error occurred during XML validation");
        }
    }
}
