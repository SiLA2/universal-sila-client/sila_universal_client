package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Call;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface CallRepository extends CrudRepository<Call, UUID> {
    Set<Call> findByRelativeIdentifier(String relativeIdentifier);

    Set<Call> findAllByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifierAndEndDateNull(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String commandRelativeIdentifier
    );

    Set<Call> findAllByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifierAndEndDateNull(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String propertyRelativeIdentifier
    );

    Call findFirstByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifierAndErrorFalseAndClientClientUuidOrderByEndDateDesc(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String commandRelativeIdentifier,
            String clientUuid
    );

    Call findFirstByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifierAndErrorFalseAndClientClientUuidOrderByEndDateDesc(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String propertyRelativeIdentifier,
            String clientId
    );

    Set<Call> findAllByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifier(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String commandRelativeIdentifier
    );

    Set<Call> findAllByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifier(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String propertyRelativeIdentifier
    );

    void deleteAllByCommand_Feature_Server_ServerUuidAndCommand_Feature_FullyQualifiedIdentifierAndCommand_RelativeIdentifier(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String commandRelativeIdentifier
    );

    void deleteAllByProperty_Feature_Server_ServerUuidAndProperty_Feature_FullyQualifiedIdentifierAndProperty_RelativeIdentifier(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String propertyRelativeIdentifier
    );


    Optional<Call> findByUuid(UUID uuid);

}

