package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Metadata;
import org.springframework.data.repository.CrudRepository;

public interface MetadataRepository extends CrudRepository<Metadata, Long> {
    Metadata findByFeature_Server_ServerUuidAndFeature_FullyQualifiedIdentifierAndRelativeIdentifier(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String metadataRelativeIdentifier
    );
}
