package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Command;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

public interface CommandRepository extends CrudRepository<Command, Long> {
    Command findByFeature_Server_ServerUuidAndFeature_FullyQualifiedIdentifierAndRelativeIdentifier(
            String serverUuid,
            String featureFullyQualifiedIdentifier,
            String commandRelativeIdentifier
    );
}
