package com.silastandard.universalclient.repositories;

import com.silastandard.universalclient.common.models.db.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, String> {

}
