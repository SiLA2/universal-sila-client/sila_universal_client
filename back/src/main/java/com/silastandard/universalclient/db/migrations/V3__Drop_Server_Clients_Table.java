package com.silastandard.universalclient.db.migrations;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.util.List;

public class V3__Drop_Server_Clients_Table extends BaseJavaMigration {
    private static final String TABLE_NAME = "SERVER_CLIENTS";

    @Override
    public void migrate(Context context) throws Exception {
        final SingleConnectionDataSource connectionDataSource = new SingleConnectionDataSource(context.getConnection(), true);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(connectionDataSource);

        final List<String> rows = jdbcTemplate.queryForList("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + TABLE_NAME + "'", String.class);
        if (rows.isEmpty()) {
            // nothing to migrate table does not exist
            return;
        }

        // Drop table if exist
        jdbcTemplate.execute("DROP TABLE IF EXISTS " + TABLE_NAME );
    }
}