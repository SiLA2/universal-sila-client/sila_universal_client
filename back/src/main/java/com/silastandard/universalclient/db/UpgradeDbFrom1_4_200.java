package com.silastandard.universalclient.db;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.stream.Stream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class UpgradeDbFrom1_4_200 {

    public static boolean databaseRequireUpgrade(
            @NonNull final String dbUrl, @NonNull final String user, @NonNull final String password
    ) {
        try (Connection conn = DriverManager.getConnection(dbUrl, user,password)) {
            // Nothing
        } catch (Throwable e) {
            final String invalidVersion = "The write format 1 is smaller than the supported format";
            Throwable it = e;
            while (it != null) {
                if (it.getMessage().contains(invalidVersion)) {
                    return true;
                }
                it = it.getCause();
            }
            throw new RuntimeException(e);
        }
        return false;
    }

    public static void upgrade(
            @NonNull final String dbUrl, @NonNull final String user, @NonNull final String password, boolean noBackup
    ) {
        final Path dbPath;

        if (!noBackup) {
            try {
                dbPath = getPathFromUrl(dbUrl);
            } catch (URISyntaxException e) {
                throw new RuntimeException("Unable to parse Database path contained in DB URL: " + dbUrl);
            }

            if (!Files.exists(dbPath.getParent())) {
                throw new RuntimeException("The specified Database path is invalid! Make sure that a Database exists at " + dbPath);
            }

            try {
                createBackup(dbPath.getParent().toString());
            } catch (Exception e) {
                throw new RuntimeException("Failed to create backup", e);
            }
        }

        final Properties properties = new Properties();
        properties.setProperty("user", user);
        properties.setProperty("password", password);

        try {
            org.h2.tools.Upgrade.upgrade(dbUrl, properties, 200);
        } catch (Exception e) {
            throw new RuntimeException("Failed to migrate database from version 1.4.200!", e);
        }
    }

    private static Path getPathFromUrl(final String jdbcUrl) throws URISyntaxException {
        final URI uri = new URI(jdbcUrl);
        if (!uri.getSchemeSpecificPart().contains(":file")) {
            throw new RuntimeException("H2 URL must point to a file to be backup.");
        }
        final int semiColonPos = uri.getSchemeSpecificPart().indexOf(';');
        final int endPathPos = (semiColonPos < 0) ? (uri.getSchemeSpecificPart().length() - 1) : semiColonPos;
        final String strPath = uri.getSchemeSpecificPart()
                .substring(0, endPathPos)
                .replace("h2:", "")
                .replace("file:", "");
        return Paths.get(strPath);
    }

    private static void createBackup(String originalFolderPath) throws IOException {
        final Path originalFolder = Paths.get(originalFolderPath);
        final Path backupFolder = Paths.get(originalFolderPath + "/" + getBackupFolderName());

        // Create the backup folder if it doesn't already exist
        if (Files.exists(backupFolder)) {
            throw new RuntimeException("Backup of previous database already exist! Rename or delete " + backupFolder);
        }

        Files.createDirectory(backupFolder);

        // Copy each file in the original folder to the backup folder
        try (Stream<Path> walk = Files.walk(originalFolder)) {
            walk.filter(path -> !Files.isDirectory(path)).forEach(path -> {
                try {
                    Path targetPath = backupFolder.resolve(originalFolder.relativize(path));
                    Files.copy(path, targetPath);
                } catch (IOException e) {
                    throw new RuntimeException("Failed to backup file " + path, e);
                }
            });
        }
    }

    private static String getBackupFolderName() {
        final Date date = new Date();
        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return "backup-" + formatter.format(date);
    }
}
