package com.silastandard.universalclient.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class UscJsonUtils {
    private static final String FPC_AFFECTED_LIST_PATH  = "$.AffectedCalls.*.value";

    public static String toJson(@NonNull final Object object) {
        final ObjectMapper om = new ObjectMapper();
        try {
            return om.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return "{}";
    }

    public static boolean isJSONValid(@NonNull final String toTest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(toTest);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static List<String> extractValueFieldFromList(@NonNull final String source) throws PathNotFoundException {
        return JsonPath.parse(source).read(FPC_AFFECTED_LIST_PATH);
    }

    public static Set<String> extractValueFieldFromSet(@NonNull final String source) throws PathNotFoundException {
        return new HashSet<>(extractValueFieldFromList(source));
    }

    public static String extractValueFieldFromMetadataJson(
            @NonNull final String source,
            @NonNull final String metadataRelativeIdentifier
    ) throws PathNotFoundException {
        final String metadataValuePath = String.format("$.%s.value", metadataRelativeIdentifier);
        return JsonPath.parse(source).read(metadataValuePath, String.class);
    }
}
