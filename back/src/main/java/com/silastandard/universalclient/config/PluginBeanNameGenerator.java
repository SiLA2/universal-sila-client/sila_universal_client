package com.silastandard.universalclient.config;

import com.silastandard.universalclient.plugin.annotations.UscPlugin;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;

import java.util.Map;

@NoArgsConstructor
public class PluginBeanNameGenerator implements BeanNameGenerator {
    private final AnnotationBeanNameGenerator defaultBeanNameGenerator = new AnnotationBeanNameGenerator ();

    /**
     *
     * @param name Name of the plugin
     * @param version currently ignored but might be used in future release
     * @return The bean name for the specified plugin
     */
    public static String getPluginBeanName(String name, String version) {
        return name;
    }

    @Override
    public String generateBeanName(BeanDefinition beanDefinition, BeanDefinitionRegistry beanDefinitionRegistry) {
        if (beanDefinition instanceof AnnotatedBeanDefinition) {
            AnnotatedBeanDefinition annotatedBeanDefinition = (AnnotatedBeanDefinition) beanDefinition;
            if (annotatedBeanDefinition.getMetadata().hasAnnotation(UscPlugin.class.getName())) {
                Map<String, Object> allAnnotationAttributes = annotatedBeanDefinition.getMetadata().getAnnotationAttributes(UscPlugin.class.getName());
                return getPluginBeanName(
                        allAnnotationAttributes.get("name").toString(),
                        allAnnotationAttributes.get("version").toString());
            }
        }
        // return package & class name + '/' + default name
        return beanDefinition.getBeanClassName() + "/" + defaultBeanNameGenerator.generateBeanName(beanDefinition, beanDefinitionRegistry);
    }
}
