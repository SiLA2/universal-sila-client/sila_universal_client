package com.silastandard.universalclient.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.PreDestroy;

@Configuration
@EnableAsync
public class AsyncConfiguration implements WebMvcConfigurer {

    private ThreadPoolTaskExecutor executor;

    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        executor = new ThreadPoolTaskExecutor();
        // todo make it configurable
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("USC-Async-");
        executor.initialize();
        configurer.setTaskExecutor(executor);
    }

    @PreDestroy
    public void destroy() {
        if (executor != null) {
            executor.shutdown();
        }
    }
}