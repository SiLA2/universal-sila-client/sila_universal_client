package com.silastandard.universalclient.config;

import com.silastandard.universalclient.plugin.events.UscEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.ResolvableType;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.lang.Nullable;

import java.util.Arrays;
import java.util.Collection;

@Slf4j
@SpringBootConfiguration
public class EventPublisher {

    @Bean
    public synchronized ApplicationEventMulticaster applicationEventMulticaster() {
        final SimpleApplicationEventMulticaster simpleApplicationEventMulticaster = new SimpleApplicationEventMulticaster() {
            private final ResolvableType uscEventType = ResolvableType.forClass(UscEvent.class);
            @Override
            public void multicastEvent(final ApplicationEvent event, @Nullable ResolvableType eventType) {
                final ResolvableType type = eventType != null ? eventType : ResolvableType.forInstance(event);
                final boolean isUscEvent = Arrays.stream(type.getGenerics()).anyMatch((this.uscEventType::isAssignableFrom));
                if (isUscEvent) {
                    // run sync
                    // todo narrow down listeners?
                    final Collection<ApplicationListener<?>> applicationListeners = getApplicationListeners(event, type);
                    applicationListeners.forEach(l -> invokeListener(l, event));
                } else {
                    // run async
                    super.multicastEvent(event, eventType);
                }
            }
        };
        simpleApplicationEventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return simpleApplicationEventMulticaster;
    }
}