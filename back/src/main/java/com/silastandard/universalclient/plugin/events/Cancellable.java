package com.silastandard.universalclient.plugin.events;

public interface Cancellable {
    boolean isCancelled();
    void setCancelled(boolean cancel);
}
