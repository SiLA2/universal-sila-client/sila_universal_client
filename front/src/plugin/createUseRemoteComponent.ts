import { useEffect, useState } from "react";
import createLoadRemoteModule from "@paciolan/remote-module-loader";
import {CreateLoadRemoteModuleOptions} from "@paciolan/remote-module-loader/dist/lib/loadRemoteModule";

export interface UseRemoteComponentHook {
    (url: string, moduleName: string): [boolean, Error, (...unknown:any) => JSX.Element];
}

export const createUseRemoteComponent = (
    args?: CreateLoadRemoteModuleOptions,
): UseRemoteComponentHook => {
    const loadRemoteModule = createLoadRemoteModule(args);

    // @ts-ignore
    const useRemoteComponent: UseRemoteComponentHook = (url, moduleName = 'default') => {
        const [{ loading, err, component }, setState] = useState<{loading: boolean, err: string | undefined, component: any}>({
            loading: true,
            err: undefined,
            component: undefined
        });

        useEffect(() => {
            let update = setState;
            update({ loading: true, err: undefined, component: undefined });
            loadRemoteModule(url)
                .then(module => {
                    if (module[moduleName]) {
                        return update({loading: false, err: undefined, component: module[moduleName]});
                    } else {
                        console.log('Plugin at ' + url + ' does not implement ' + moduleName)
                        return update({loading: false, err: 'not implemented', component: undefined});
                    }
                })
                .catch(err => update({ loading: false, err, component: undefined }));

            return () => {
                // invalidate update function for stale closures
                update = () => {
                    // this function is left intentionally blank
                };
            };
        }, [url, moduleName]);

        return [loading, err, component];
    };

    return useRemoteComponent;
};