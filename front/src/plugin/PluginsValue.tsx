import {useContext} from "react";
import {PluginContextType, PluginsContext} from "./PluginsProvider";
import PluginImplementation from "./PluginImplementation";

export const PluginsValue = <T extends keyof PluginImplementation>(key: T): Exclude<PluginImplementation[T], null | undefined>[] => {
    const pluginsContext = useContext<PluginContextType>(PluginsContext);
    const values = Object.entries(pluginsContext)
        .map(([url, plugin]) => [url, plugin[key]])
        .filter(([, value]) => !!value)
        .map(([url, value]) => value)
        .filter(c => !!c) as Exclude<PluginImplementation[T], null | undefined>[];
    return (values);
}

export default PluginsValue;