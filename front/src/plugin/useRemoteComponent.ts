import {createUseRemoteComponent} from "./createUseRemoteComponent";
/**
 * We import directly from dist to prevent the following warning:
 *  Module not found: Can't resolve 'remote-component.config.js'
 */
import { createRequires } from "@paciolan/remote-component/dist/createRequires";
import {resolve} from "./remote-component.config";

const requires = createRequires(() => resolve);

export const useRemoteComponent = createUseRemoteComponent({ requires });
