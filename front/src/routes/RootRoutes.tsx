import React from "react";
import {Navigate, Outlet, RouterProvider, createHashRouter} from "react-router-dom";
import Dashboard from "../pages/dashboard/Dashboard";
import Tools from "../pages/tools/Tools";
import Dev from "../pages/dev/Dev";
import Help from "../pages/help/Help";
import Workspace from "../pages/workspace/Workspace";
import Settings from "../pages/settings/Settings";
import RootLayout from "../pages/RootLayout";
import ServerIdOutlet from "../pages/server/ServerIdOutlet";
import PluginsValue from "../plugin/PluginsValue";
import RootRouterErrorBoundary from "../components/RootRouterErrorBoundary";
import {Display404OrLoading} from "../components/Display404OrLoading";

export default function RootRoutes(): JSX.Element {
    const pluginsRootRoutes = (PluginsValue('PluginRootRoute') || [])
        .flat()
        .map(v => v.routes)
        .flat();
    const pluginsServerRootRoutes = (PluginsValue('PluginRootServerRoute') || [])
        .flat()
        .map(v => v.routes)
        .flat();
    const router = createHashRouter([
        {
            path: "/", element: <RootLayout />, errorElement: <RootRouterErrorBoundary/>,
            children: [
                {index: true, element: <Navigate to={"/dashboard"} />},
                {path: "dashboard/*", element: <Dashboard />},
                {
                    path: "server", element: <Outlet />,
                    children: [
                        {
                            index: true, element: <Navigate to={"/dashboard"} />
                        },
                        {
                            path: ":id", element: <ServerIdOutlet />,
                            children: [
                                {index: true, element: <Navigate to={"info"} />},
                                {path: "info", element: null},
                                {path: "property", element: null},
                                {path: "command", element: null},
                                ...pluginsServerRootRoutes.map(r => ({...r, element: null}))
                            ]
                        }
                    ]
                },
                {path: "tools", element: <Tools />},
                {path: "dev", element: <Dev />},
                {path: "help", element: <Help />},
                {path: "workspace", element: <Workspace />},
                {path: "settings", element: <Settings />},
                ...pluginsRootRoutes,
                {path: '*', element: <Display404OrLoading />},
            ],
        },
        {path: '*', element: <RootLayout />},
    ]);

    return (
        <RouterProvider router={router} />
    );
}