import {
    SELECT_ALL_SERVER_FEATURE,
    SERVER_FEATURE_SELECTION, SERVER_FEATURE_SELECTIONS, UNSELECT_ALL_SERVER_FEATURE
} from "./actionTypes";

export type ServerFeatureKeyValueState = {
    [serverUuid: string]: ServerFeatureSelectedKeyValue;
}

export type ServerFeatureSelectedKeyValue = {
    [featureIdentifier: string]: boolean;
}

export type ServerFeatureSelectionUpdate = {
    serverUuid: string;
    featureIdentifier: string;
    selected: boolean;
}

export type ServerFeatureSelectionsUpdate = {
    serverUuid: string;
    featuresSelection: ServerFeatureSelectedKeyValue;
}

export type ServerIdentifier = {
    serverUuid: string;
}

export type ServerFeatureSelection = {
    type: typeof SERVER_FEATURE_SELECTION;
    payload: ServerFeatureSelectionUpdate;
};

export type ServerFeatureSelections = {
    type: typeof SERVER_FEATURE_SELECTIONS;
    payload: ServerFeatureSelectionsUpdate;
};

export type SelectAllServerFeature = {
    type: typeof SELECT_ALL_SERVER_FEATURE;
    payload: ServerIdentifier;
};

export type UnselectAllServerFeature = {
    type: typeof UNSELECT_ALL_SERVER_FEATURE;
    payload: ServerIdentifier;
};

export type ServerFeatureActions =
    | ServerFeatureSelection
    | ServerFeatureSelections
    | SelectAllServerFeature
    | UnselectAllServerFeature;