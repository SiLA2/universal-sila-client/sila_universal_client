import {
    SelectAllServerFeature,
    ServerFeatureSelection, ServerFeatureSelections, ServerFeatureSelectionsUpdate,
    ServerFeatureSelectionUpdate, ServerIdentifier,
    UnselectAllServerFeature,
} from "./types";
import {
    SELECT_ALL_SERVER_FEATURE,
    SERVER_FEATURE_SELECTION,
    SERVER_FEATURE_SELECTIONS,
    UNSELECT_ALL_SERVER_FEATURE
} from "./actionTypes";

export const serverFeatureSelection = (
    payload: ServerFeatureSelectionUpdate
): ServerFeatureSelection => ({
    type: SERVER_FEATURE_SELECTION,
    payload: payload
});

export const serverFeatureSelections = (
    payload: ServerFeatureSelectionsUpdate
): ServerFeatureSelections => ({
    type: SERVER_FEATURE_SELECTIONS,
    payload: payload
});

export const selectAllServerFeature = (
    payload: ServerIdentifier
): SelectAllServerFeature => ({
    type: SELECT_ALL_SERVER_FEATURE,
    payload: payload
});

export const unselectAllServerFeature = (
    payload: ServerIdentifier
): UnselectAllServerFeature => ({
    type: UNSELECT_ALL_SERVER_FEATURE,
    payload: payload
});