import { AppState } from "../../../rootReducer";
import { ServersTabs} from "./types";

export const getServerOpen = (state: AppState) => state.serverOpen;

export const getCurrentServerOpen = (state: AppState): string | undefined => state.serverOpen.serverUuidOpen;
export const getServerOpenTabs = (state: AppState): ServersTabs => state.serverOpen.serversTabs;
