import {
    SET_ACTIVE_SERVER_TAB, CLOSE_SERVER_TAB,
} from "./actionTypes";

export type Tab = 'info' | 'command' | 'property' | string;

export type ServersTabs = {
    [serverUuid: string]: Tab;
}

export type ServerOpenKeyValueState = {
    serverUuidOpen?: string;
    serversTabs: ServersTabs;
}

export type ServerTab = {
    serverUuid: string;
    tab?: Tab;
}

export type ServerUUID = {
    serverUuid: string;
}

export type ServerOpenRemoveTab = {
    type: typeof CLOSE_SERVER_TAB;
    payload: ServerUUID;
};

export type ServerCurrentOpenSet = {
    type: typeof SET_ACTIVE_SERVER_TAB;
    payload: ServerTab;
};

export type ServerOpenActions =
    | ServerOpenRemoveTab
    | ServerCurrentOpenSet;