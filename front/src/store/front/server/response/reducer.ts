import {
    CommandResponse,
    CommandResponseActions,
    CommandResponseState, CommandResponseUUIDMap,
} from "./types";
import {
    ADD_COMMAND_RESPONSE,
    ADD_COMMAND_RESPONSES,
    REMOVE_COMMAND_RESPONSE,
    REMOVE_COMMAND_RESPONSES,
} from "./actionTypes";

import {
    WS_CALL_PROGRESS,
    WS_CALL_COMPLETE,
    WS_CALL_ERROR,
    WS_CALL_INIT,
    WS_CALL_EXEC_INFO,
    WS_CALL_INTERMEDIATE,
    WS_PROPERTY_UPDATE
} from "../../../sila/websocket/actionTypes";
import {getFullyQualifiedCallId} from "../../../../utils/server";
import {ProtoCommandExecutionUUID} from "../../../../generated/SiLAFramework";

const initialState: CommandResponseState = {
    responseUuidMap: {},
    responseFullIdMap: {}
};

export default function Reducer(state: CommandResponseState = initialState, action: CommandResponseActions): CommandResponseState {
    switch (action.type) {
        case WS_CALL_PROGRESS: {
            const call = action.payload.siLACall;
            let stateCall: CommandResponse = state.responseUuidMap[call.identifier];
            if (!stateCall) {
                stateCall = {
                    commandIdentifier: call.callId,
                    fullyQualifiedFeatureId: call.fullyQualifiedFeatureId,
                    uuid: call.identifier,
                    type: call.type,
                    intermediateResponse: [],
                    metadatas: call.metadatas,
                    parameter: call.parameters, // todo init state
                    serverUuid: call.serverId,
                    state: undefined,
                    success: true,
                    timestamp: new Date(),
                    value: ''
                };
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(stateCall);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [call.identifier]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId]
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [call.identifier]: {
                        ...stateCall
                    }
                }
            };
        }
        case WS_CALL_COMPLETE: {
            const call = action.payload.siLACall;
            let stateCall: CommandResponse = state.responseUuidMap[call.identifier];
            if (!stateCall) {
                stateCall = {
                    commandIdentifier: call.callId,
                    fullyQualifiedFeatureId: call.fullyQualifiedFeatureId,
                    uuid: call.identifier,
                    type: call.type,
                    intermediateResponse: [],
                    metadatas: call.metadatas,
                    parameter: call.parameters, // todo init state
                    serverUuid: call.serverId,
                    state: undefined,
                    success: true,
                    timestamp: new Date(),
                    value: action.payload.result
                };
            } else {
                stateCall.success = true;
                stateCall.value = action.payload.result;
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(stateCall);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [call.identifier]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId]
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [call.identifier]: {
                        ...stateCall
                    }
                }
            };
        }
        case WS_CALL_ERROR: {
            const call = action.payload.siLACall;
            let stateCall: CommandResponse = state.responseUuidMap[call.identifier];
            if (!stateCall) {
                stateCall = {
                    commandIdentifier: call.callId,
                    fullyQualifiedFeatureId: call.fullyQualifiedFeatureId,
                    uuid: call.identifier,
                    type: call.type,
                    intermediateResponse: [],
                    metadatas: call.metadatas,
                    parameter: call.parameters, // todo init state
                    serverUuid: call.serverId,
                    state: undefined,
                    success: false,
                    timestamp: new Date(),
                    value: action.payload.error
                };
            } else {
                stateCall.success = false;
                stateCall.value = action.payload.error;
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(stateCall);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [call.identifier]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId],
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [call.identifier]: {
                        ...stateCall
                    }
                }
            };
        }
        case WS_CALL_INIT: {
            const call = action.payload.baseCall;
            let stateCall: CommandResponse = state.responseUuidMap[call.identifier];
            if (!stateCall) {
                stateCall = {
                    commandIdentifier: call.callId,
                    commandExecutionUUID: (action.payload.command.commandExecutionUUID as ProtoCommandExecutionUUID).value,
                    fullyQualifiedFeatureId: call.fullyQualifiedFeatureId,
                    uuid: call.identifier,
                    type: call.type,
                    intermediateResponse: [],
                    metadatas: call.metadatas,
                    parameter: call.parameters, // todo init state
                    serverUuid: call.serverId,
                    state: undefined,
                    success: true,
                    timestamp: new Date(),
                    value: ''
                };
            } else {
                stateCall.commandExecutionUUID = (action.payload.command.commandExecutionUUID as ProtoCommandExecutionUUID).value;
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(stateCall);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [call.identifier]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId]
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [call.identifier]: {
                        ...stateCall
                    }
                }
            };
        }
        case WS_CALL_EXEC_INFO: {
            const call = action.payload.baseCall;
            let stateCall: CommandResponse = state.responseUuidMap[call.identifier];
            if (!stateCall) {
                stateCall = {
                    commandIdentifier: call.callId,
                    fullyQualifiedFeatureId: call.fullyQualifiedFeatureId,
                    uuid: call.identifier,
                    type: call.type,
                    intermediateResponse: [],
                    metadatas: call.metadatas,
                    parameter: call.parameters, // todo init state
                    serverUuid: call.serverId,
                    state: action.payload.command,
                    success: true,
                    timestamp: new Date(),
                    value: undefined
                };
            } else {
                stateCall.state = action.payload.command;
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(stateCall);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [call.identifier]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId]
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [call.identifier]: {
                        ...stateCall
                    }
                }
            };
        }
        case WS_CALL_INTERMEDIATE: {
            const call = action.payload.baseCall;
            let stateCall: CommandResponse = state.responseUuidMap[call.identifier];
            if (!stateCall) {
                stateCall = {
                    commandIdentifier: call.callId,
                    fullyQualifiedFeatureId: call.fullyQualifiedFeatureId,
                    uuid: call.identifier,
                    type: call.type,
                    intermediateResponse: [action.payload.responseType],
                    metadatas: call.metadatas,
                    parameter: call.parameters,
                    serverUuid: call.serverId,
                    state: undefined,
                    success: true,
                    timestamp: new Date(),
                    value: null
                };
            } else {
                stateCall.intermediateResponse = [...stateCall.intermediateResponse, action.payload.responseType];
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(stateCall);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [call.identifier]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId]
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [call.identifier]: {
                        ...stateCall
                    }
                }
            };
        }
        case WS_PROPERTY_UPDATE: {
            const call = action.payload.baseCall;
            let stateCall: CommandResponse = state.responseUuidMap[call.identifier];
            if (!stateCall) {
                stateCall = {
                    commandIdentifier: call.callId,
                    fullyQualifiedFeatureId: call.fullyQualifiedFeatureId,
                    uuid: call.identifier,
                    type: call.type,
                    intermediateResponse: [action.payload.value],
                    metadatas: call.metadatas,
                    parameter: call.parameters,
                    serverUuid: call.serverId,
                    state: undefined,
                    success: true,
                    timestamp: new Date(),
                    value: null
                };
            } else {
                stateCall.intermediateResponse = [...stateCall.intermediateResponse, action.payload.value];
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(stateCall);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [call.identifier]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId]
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [call.identifier]: {
                        ...stateCall
                    }
                }
            };
        }
        case ADD_COMMAND_RESPONSE: {
            const fullyQualifiedCallId = getFullyQualifiedCallId(action.payload);
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: {
                        [action.payload.uuid]: null,
                        ...state.responseFullIdMap[fullyQualifiedCallId]
                    }
                },
                responseUuidMap: {
                    ...state.responseUuidMap,
                    [action.payload.uuid]: {
                        ...action.payload
                    }
                }
            };
        }
        case ADD_COMMAND_RESPONSES: {
            return action.payload.responses.reduce((acc, response) => {
                const fullyQualifiedCallId = getFullyQualifiedCallId(response);
                return {
                    ...acc,
                    responseFullIdMap: {
                        ...acc.responseFullIdMap,
                        [fullyQualifiedCallId]: {
                            [response.uuid]: null,
                            ...acc.responseFullIdMap[fullyQualifiedCallId]
                        }
                    },
                    responseUuidMap: {
                        ...acc.responseUuidMap,
                        [response.uuid]: {
                            ...response
                        }
                    }
                }
            }, state);
        }
        case REMOVE_COMMAND_RESPONSE: {
            const response = state.responseUuidMap[action.payload.uuid];
            if (!response) {
                return state;
            }
            const fullyQualifiedCallId = getFullyQualifiedCallId(response);
            const {[response.uuid]: removeFullIdMap, ...restFullIdMap } = state.responseFullIdMap[fullyQualifiedCallId]
            const {[response.uuid]: removeUuidMap, ...restUuidMap } = state.responseUuidMap
            return {
                ...state,
                responseFullIdMap: {
                    ...state.responseFullIdMap,
                    [fullyQualifiedCallId]: restFullIdMap
                },
                responseUuidMap: restUuidMap
            }
        }
        case REMOVE_COMMAND_RESPONSES: {
            const fullyQualifiedCallId = getFullyQualifiedCallId(action.payload);
            const responsesToRemove = state.responseFullIdMap[fullyQualifiedCallId] || {};
            const {[fullyQualifiedCallId]: removeFullIdMap, ...restFullIdMap } = state.responseFullIdMap
            return {
                ...state,
                responseFullIdMap: restFullIdMap,
                responseUuidMap: Object.keys(state.responseUuidMap)
                    .filter((id) => responsesToRemove.hasOwnProperty(id))
                    .reduce((acc: CommandResponseUUIDMap, id: string) => {
                        delete acc[id];
                        return acc;
                    }, state.responseUuidMap)
            }
        }
    }
    return state;
};