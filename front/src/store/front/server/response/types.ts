import {
    ADD_COMMAND_RESPONSE,
    ADD_COMMAND_RESPONSES,
    FETCH_RESPONSE_FROM_CACHE_FAILURE,
    FETCH_RESPONSE_FROM_CACHE_REQUEST,
    FETCH_RESPONSE_FROM_CACHE_SUCCESS,
    REMOVE_COMMAND_RESPONSE,
    REMOVE_COMMAND_RESPONSES
} from "./actionTypes";
import {
    WsCallComplete,
    WsCallError,
    WsCallExecInfo,
    WsCallInit,
    WsCallIntermediate,
    WsCallProgress, WsPropertyUpdate
} from "../../../sila/websocket/types";
import {CachedCall, SiLAJavaType} from "../../../../generated/silaModels";

export type CommandResponseUUIDMap = {[uuid: string]: CommandResponse};
export type UUIDSet = {[uuid: string]: null};
export type CommandResponseFullIdMap = {[fullId: string]: UUIDSet};

export type CommandResponseState = {
    responseUuidMap: CommandResponseUUIDMap
    responseFullIdMap: CommandResponseFullIdMap
}

// todo convert any below with proper type
export type CommandResponse = {
    uuid: string;
    serverUuid: string;
    fullyQualifiedFeatureId: string;
    commandIdentifier: string;
    commandExecutionUUID?: string;
    type: SiLAJavaType;
    success: boolean;
    timestamp: Date;
    value: any;
    parameter: any;
    state: any;
    intermediateResponse: any[]
    metadatas: any
}

export type CommandResponses = {
    responses: CommandResponse[]
}

export type CommandResponseIdentifier = {
    uuid: string
}

export type CommandIdentifier = {
    serverUuid: string;
    fullyQualifiedFeatureId: string;
    commandIdentifier: string;
}

export type AddCommandResponse = {
    type: typeof ADD_COMMAND_RESPONSE;
    payload: CommandResponse;
};

export type AddCommandResponses = {
    type: typeof ADD_COMMAND_RESPONSES;
    payload: CommandResponses;
};

export type RemoveCommandResponse = {
    type: typeof REMOVE_COMMAND_RESPONSE;
    payload: CommandResponseIdentifier;
};

export type RemoveCommandResponses = {
    type: typeof REMOVE_COMMAND_RESPONSES;
    payload: CommandIdentifier;
};

export type FetchResponseFromCacheRequestPayload = {
    callId: string
}

export type FetchResponseFromCacheSuccessPayload = {
    callId: string;
    cachedResponse: CachedCall;
}

export type FetchResponseFromCacheFailurePayload = {
    callId: string;
    error: string;
}

export type FetchResponseFromCatchRequest = {
    type: typeof FETCH_RESPONSE_FROM_CACHE_REQUEST;
    payload: FetchResponseFromCacheRequestPayload;
}

export type FetchResponseFromCatchSuccess = {
    type: typeof FETCH_RESPONSE_FROM_CACHE_SUCCESS;
    payload: FetchResponseFromCacheSuccessPayload;
};

export type FetchResponseFromCatchFailure = {
    type: typeof FETCH_RESPONSE_FROM_CACHE_FAILURE;
    payload: FetchResponseFromCacheFailurePayload;
};

export type CommandResponseActions =
    | FetchResponseFromCatchRequest
    | FetchResponseFromCatchSuccess
    | FetchResponseFromCatchFailure
    | WsCallProgress
    | WsCallComplete
    | WsCallError
    | WsCallInit
    | WsCallExecInfo
    | WsCallIntermediate
    | WsPropertyUpdate
    | AddCommandResponse
    | AddCommandResponses
    | RemoveCommandResponses
    | RemoveCommandResponse;