import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {CommandResponse} from "./types";
import {SiLAJavaType} from "../../../../generated/silaModels";

export const getCommandResponse = (state: AppState) => state.commandResponse;
export const getCommandResponseById = (state: AppState, fullId: string) => fullId;
export const getCommandResponseByIdType = (state: AppState, fullId: string, type: SiLAJavaType) => ({fullId, type});

export const getCommandResponseByIdTypes = (state: AppState, fullId: string, type: SiLAJavaType[]) => ({fullId, type});

export const getCommandResponseByIdSelector: Selector<AppState, CommandResponse[]> = createSelector(
    [getCommandResponse, getCommandResponseById],
    (responses, fullId) => Object.keys(responses.responseFullIdMap[fullId] || {})
        .map(uuid => responses.responseUuidMap[uuid])
);

export const getCommandResponseByIdTypeSelector: Selector<AppState, CommandResponse[]> = createSelector(
    [getCommandResponse, getCommandResponseByIdType],
    (responses, filters) => Object.keys(responses.responseFullIdMap[filters.fullId] || {})
        .map(uuid => responses.responseUuidMap[uuid])
        .filter(r => r.type === filters.type)
);

export const getCommandResponseByIdTypesSelector: Selector<AppState, CommandResponse[]> = createSelector(
    [getCommandResponse, getCommandResponseByIdTypes],
    (responses, filters) => Object.keys(responses.responseFullIdMap[filters.fullId] || {})
        .map(uuid => responses.responseUuidMap[uuid])
        .filter(r => filters.type.includes(r.type))
);