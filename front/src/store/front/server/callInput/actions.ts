import {
    ListValueToRemove,
    NewListValueToPush, CallInputInit,
    CallInputListPush,
    CallInputListRemove,
    CallInputUpdate, ValueToInit,
    ValueToUpdate
} from "./types";
import {
    INIT_CALL_INPUT,
    PUSH_CALL_INPUT_LIST_ITEM,
    REMOVE_CALL_INPUT_LIST_ITEM,
    UPDATE_CALL_INPUT_ITEM
} from "./actionTypes";

export const callInputListPush = (
    payload: NewListValueToPush
): CallInputListPush => ({
    type: PUSH_CALL_INPUT_LIST_ITEM,
    payload: payload
});

export const callInputListRemove = (
    payload: ListValueToRemove
): CallInputListRemove => ({
    type: REMOVE_CALL_INPUT_LIST_ITEM,
    payload: payload
});

export const callInputUpdate = (
    payload: ValueToUpdate
): CallInputUpdate => ({
    type: UPDATE_CALL_INPUT_ITEM,
    payload: payload
});

export const callInputInit = (
    payload: ValueToInit
): CallInputInit => ({
    type: INIT_CALL_INPUT,
    payload: payload
});

