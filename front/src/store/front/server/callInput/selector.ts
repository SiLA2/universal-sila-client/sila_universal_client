import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {SiLATypeValueState} from "./types";

export const getCallInput = (state: AppState) => state.callInput;
export const getCallInputParameters = (state: AppState) => state.callInput.parameters;
export const getCallInputParametersById = (state: AppState, id: string) => id;
export const getCallInputMetadatas = (state: AppState) => state.callInput.metadatas;
export const getCallInputMetadatasById = (state: AppState, id: string) => id;

export const getCallInputParametersByIdSelector: Selector<AppState, SiLATypeValueState> = createSelector(
    [getCallInputParameters, getCallInputParametersById],
    (inputParameters, fullyQualifiedId) => inputParameters[fullyQualifiedId]
);
export const getCallInputMetadatasByIdSelector: Selector<AppState, SiLATypeValueState | undefined> = createSelector(
    [getCallInputMetadatas, getCallInputMetadatasById],
    (metatadas, fullyQualifiedId) => metatadas[fullyQualifiedId]
);
