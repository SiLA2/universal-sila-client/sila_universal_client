import {
    INIT_CALL_INPUT,
    PUSH_CALL_INPUT_LIST_ITEM,
    REMOVE_CALL_INPUT_LIST_ITEM,
    UPDATE_CALL_INPUT_ITEM
} from "./actionTypes";
import {BasicValueType} from "../../../../components/server/call/dataTypeForm/type/Basic";
import {ListValueType} from "../../../../components/server/call/dataTypeForm/type/List";
import {DataTypeIdentifierType} from "../../../../components/server/call/dataTypeForm/type/DataTypeIdentifier";
import {ProtoAny} from "../../../../generated/SiLAFramework";

export type FormInputType = "PARAMETER" | "METADATA";

export type CommandsFormState = {
    parameters: CallsParameterFormState;
    metadatas: CallsMetadataFormState;
}

export type CallsParameterFormState = {
    [fullyQualifiedCommandId: string]: SiLATypeValueState;
}

export type CallsMetadataFormState = {
    [serverUuid: string]: SiLATypeValueState;
}

export type SiLATypeValueMap = {
    [stateId: string]: (BasicValueType | DataTypeIdentifierType | SiLATypeValueState)
}

export type SiLATypeValueState = SiLATypeValueMap | ListValueType | ProtoAny;

export type NewListValueToPush = {
    fullyQualifiedCommandId: string;
    stateId: string;
    stateIdToPush: string;
    valueToPush: SiLATypeValueState;
    formType: FormInputType;
    serverId: string;
}

export type ListValueToRemove = {
    fullyQualifiedCommandId: string;
    stateId: string;
    stateIdToRemove: string;
    formType: FormInputType;
    serverId: string;
}

export type ValueToUpdate = {
    fullyQualifiedCommandId: string;
    stateId: string;
    updatedValue: BasicValueType;
    formType: FormInputType;
    serverId: string;
}

export type ValueToInit = {
    fullyQualifiedCommandId: string;
    state: SiLATypeValueState;
    formType: FormInputType;
    serverId: string;
}

export type CallInputListPush = {
    type: typeof PUSH_CALL_INPUT_LIST_ITEM;
    payload: NewListValueToPush;
}

export type CallInputListRemove = {
    type: typeof REMOVE_CALL_INPUT_LIST_ITEM;
    payload: ListValueToRemove;

}

export type CallInputUpdate = {
    type: typeof UPDATE_CALL_INPUT_ITEM;
    payload: ValueToUpdate;
}

export type CallInputInit = {
    type: typeof INIT_CALL_INPUT;
    payload: ValueToInit;
}

export type CallInputAction =
    | CallInputListPush
    | CallInputListRemove
    | CallInputUpdate
    | CallInputInit;