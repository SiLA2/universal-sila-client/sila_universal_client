import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {
    AuthorizationProviderMeta, ConfiguredSiLAClientsMeta, IsLockedMeta,
    PausedCommandsMeta, RecoverableErrorsMeta,
    RuntimeConstraintsMeta,
    ServerCallsAffectedByMetadata,
    ServerMeta,
} from "./types";

export const getServerMeta = (state: AppState) => state.serverMeta;
export const getServerMetaById = (state: AppState, uuid: string) => uuid;

export const getServerMetaByIdSelector: Selector<AppState, ServerMeta> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers[serverUuid]
);

export const getServerMetaConstraintByIdSelector: Selector<AppState, RuntimeConstraintsMeta | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.runtimeConstraints
);

export const getServerMetaPausedCommandsByIdSelector: Selector<AppState, PausedCommandsMeta | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.pausedCommands
);

export const getServerMetaAuthorizationProvidersByIdSelector: Selector<AppState, AuthorizationProviderMeta | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.authorizationProvider
);

export const getServerMetaConfiguredSiLAClientsByIdSelector: Selector<AppState, ConfiguredSiLAClientsMeta | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.configuredSiLAClients
);

export const getServerMetaRecoverableErrorsByIdSelector: Selector<AppState, RecoverableErrorsMeta | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.recoverableErrors
);

export const getServerMetaIsLockedByIdSelector: Selector<AppState, IsLockedMeta | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.isLocked
);

export const getServerMetaAffectedCallsByIdSelector: Selector<AppState, ServerCallsAffectedByMetadata | undefined> = createSelector(
    [getServerMeta, getServerMetaById],
    (servers, serverUuid) => servers?.[serverUuid]?.callAffectedByMetadata
);