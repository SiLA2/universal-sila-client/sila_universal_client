import {
    WsCallComplete,
    WsPropertyUpdate
} from "../../../sila/websocket/types";
import {
    FETCH_CALL_AFFECTED_BY_METADATA_FAILURE,
    FETCH_CALL_AFFECTED_BY_METADATA_REQUEST,
    FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS
} from "./actionTypes";
import {
    ProtoSubscribe_ParametersConstraints_Responses
} from "../../../../generated/sila2/org/silastandard/core/commands/parameterconstraintsprovider/v1/ParameterConstraintsProvider";
import {
    ProtoSubscribe_PausedCommands_Responses
} from "../../../../generated/sila2/org/silastandard/core/commands/pausecontroller/v2/PauseController";
import {
    ProtoSubscribe_RecoverableErrors_Responses
} from "../../../../generated/sila2/org/silastandard/core/errorrecoveryservice/v1/ErrorRecoveryService";
import {
    ProtoSubscribe_IsLocked_Responses
} from "../../../../generated/sila2/org/silastandard/core/lockcontroller/v2/LockController";
import {
    ProtoGet_AuthorizationProvider_Responses
} from "../../../../generated/sila2/org/silastandard/core/authorizationconfigurationservice/v1/AuthorizationConfigurationService";
import {
    ProtoGet_ConfiguredSiLAClients_Responses
} from "../../../../generated/sila2/org/silastandard/core/connectionconfigurationservice/v1/ConnectionConfigurationService";
import {
    ProtoGet_IsLocked_Responses
} from "../../../../generated/sila2/org/silastandard/core/lockcontroller/v1/LockController";

export type ServersMetaState = {
    [serverUuid: string]: ServerMeta;
}

export type ServerMetadataAffectingCalls = {
    [fullyQualifiedMetadata: string]: string[];
}

export type ServerCallsAffectedByMetadata = {
    [fullyQualifiedCall: string]: string[];
}

export type RuntimeConstraintsMeta = {
    v1?: ProtoSubscribe_ParametersConstraints_Responses;
};

export type PausedCommandsMeta = {
    // v1 not supported
    v2?: ProtoSubscribe_PausedCommands_Responses
};

export type AuthorizationProviderMeta = {
    v1?: ProtoGet_AuthorizationProvider_Responses
};

export type ConfiguredSiLAClientsMeta = {
    v1?: ProtoGet_ConfiguredSiLAClients_Responses
};

export type RecoverableErrorsMeta = {
    v1?: ProtoSubscribe_RecoverableErrors_Responses
};

export type IsLockedMeta = {
    v1?: ProtoGet_IsLocked_Responses
    v2?: ProtoSubscribe_IsLocked_Responses
};

export type ServerMeta = {
    runtimeConstraints?: RuntimeConstraintsMeta;
    pausedCommands?: PausedCommandsMeta;
    authorizationProvider?: AuthorizationProviderMeta;
    configuredSiLAClients?: ConfiguredSiLAClientsMeta;
    recoverableErrors?: RecoverableErrorsMeta;
    isLocked?: IsLockedMeta;
    callAffectedByMetadata?: ServerCallsAffectedByMetadata
}

export type FetchCallAffectedByMetadataRequestPayload = {
    serverUuid: string
}

export type FetchCallAffectedByMetadataSuccessPayload = {
    serverUuid: string;
    serverMetadataAffectingCalls: ServerMetadataAffectingCalls;
}

export type FetchCallAffectedByMetadataFailurePayload = {
    serverUuid: string;
    error: string;
}

export type FetchCallAffectedByMetadataRequest = {
    type: typeof FETCH_CALL_AFFECTED_BY_METADATA_REQUEST;
    payload: FetchCallAffectedByMetadataRequestPayload;
}

export type FetchCallAffectedByMetadataSuccess = {
    type: typeof FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS;
    payload: FetchCallAffectedByMetadataSuccessPayload;
};

export type FetchCallAffectedByMetadataFailure = {
    type: typeof FETCH_CALL_AFFECTED_BY_METADATA_FAILURE;
    payload: FetchCallAffectedByMetadataFailurePayload;
};

export type ServerMetaActions =
    | WsPropertyUpdate
    | WsCallComplete
    | FetchCallAffectedByMetadataRequest
    | FetchCallAffectedByMetadataSuccess
    | FetchCallAffectedByMetadataFailure;