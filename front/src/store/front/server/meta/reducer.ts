import {ServerCallsAffectedByMetadata, ServerMetaActions, ServersMetaState,} from "./types";
import {WS_CALL_COMPLETE, WS_PROPERTY_UPDATE} from "../../../sila/websocket/actionTypes";
import {
    FETCH_CALL_AFFECTED_BY_METADATA_FAILURE,
    FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS
} from "./actionTypes";
import {
    ProtoSubscribe_ParametersConstraints_Responses
} from "../../../../generated/sila2/org/silastandard/core/commands/parameterconstraintsprovider/v1/ParameterConstraintsProvider";
import {
    ProtoSubscribe_PausedCommands_Responses
} from "../../../../generated/sila2/org/silastandard/core/commands/pausecontroller/v2/PauseController";
import {
    ProtoGet_AuthorizationProvider_Responses
} from "../../../../generated/sila2/org/silastandard/core/authorizationconfigurationservice/v1/AuthorizationConfigurationService";
import {
    ProtoSubscribe_IsLocked_Responses
} from "../../../../generated/sila2/org/silastandard/core/lockcontroller/v2/LockController";
import {
    ProtoSubscribe_RecoverableErrors_Responses
} from "../../../../generated/sila2/org/silastandard/core/errorrecoveryservice/v1/ErrorRecoveryService";
import {
    ProtoGet_ConfiguredSiLAClients_Responses
} from "../../../../generated/sila2/org/silastandard/core/connectionconfigurationservice/v1/ConnectionConfigurationService";
import {FullyQualifiedIdentifierUtils} from "../../../../utils/fullyQualifiedIdentifier";
import {
    ProtoGet_IsLocked_Responses
} from "../../../../generated/sila2/org/silastandard/core/lockcontroller/v1/LockController";
import {SiLAJavaSiLACall} from "../../../../generated/silaModels";

export const initialServerOpenState: ServersMetaState = {
};

function onResponse(state: ServersMetaState, call: SiLAJavaSiLACall, result: string): ServersMetaState {
    const serverId = call.serverId;
    const featureVersion = FullyQualifiedIdentifierUtils.getFeatureVersion(call.fullyQualifiedFeatureId);
    const isFeatureV1 = featureVersion === 'v1';
    const isFeatureV2 = featureVersion === 'v2';
    if (isFeatureV1 && call.fullyQualifiedFeatureId.startsWith("org.silastandard/core.commands/ParameterConstraintsProvider") && call.callId === "ParametersConstraints") {
        const constraints: ProtoSubscribe_ParametersConstraints_Responses = result as ProtoSubscribe_ParametersConstraints_Responses;
        return {
            ...state,
            [serverId]: {
                ...state[serverId],
                runtimeConstraints: {
                    ...state[serverId]?.runtimeConstraints,
                    v1: {
                        ...constraints
                    }
                }
            }
        }
    }
    if (isFeatureV2 && call.fullyQualifiedFeatureId.startsWith("org.silastandard/core.commands/PauseController") && call.callId === "PausedCommands") {
        const pausedCommands: ProtoSubscribe_PausedCommands_Responses = result as ProtoSubscribe_PausedCommands_Responses;
        return {
            ...state,
            [serverId]: {
                ...state[serverId],
                pausedCommands: {
                    ...state[serverId]?.pausedCommands,
                    v2: {
                        ...pausedCommands
                    }
                }
            }
        }
    }
    if (isFeatureV1 && call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/AuthorizationConfigurationService") && call.callId === "AuthorizationProvider") {
        const authorizationProvider: ProtoGet_AuthorizationProvider_Responses = result as ProtoGet_AuthorizationProvider_Responses;
        return {
            ...state,
            [serverId]: {
                ...state[serverId],
                authorizationProvider: {
                    ...state[serverId]?.authorizationProvider,
                    v1: {
                        ...authorizationProvider
                    }
                }
            }
        }
    }
    if (isFeatureV1 && call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/ConnectionConfigurationService") && call.callId === "ConfiguredSiLAClients") {
        const configuredSiLAClients: ProtoGet_ConfiguredSiLAClients_Responses = result as ProtoGet_ConfiguredSiLAClients_Responses;
        return {
            ...state,
            [serverId]: {
                ...state[serverId],
                configuredSiLAClients: {
                    ...state[serverId]?.configuredSiLAClients,
                    v1: {
                        ...configuredSiLAClients
                    }
                }
            }
        }
    }
    if (isFeatureV1 && call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/ErrorRecoveryService") && call.callId === "RecoverableErrors") {
        const recoverableErrors: ProtoSubscribe_RecoverableErrors_Responses = result as ProtoSubscribe_RecoverableErrors_Responses;
        return {
            ...state,
            [serverId]: {
                ...state[serverId],
                recoverableErrors: {
                    ...state[serverId]?.recoverableErrors,
                    v1: {
                        ...recoverableErrors
                    }
                }
            }
        }
    }
    if (call.fullyQualifiedFeatureId.startsWith("org.silastandard/core/LockController") && call.callId === "IsLocked") {
        let isLocked: undefined | ProtoGet_IsLocked_Responses | ProtoSubscribe_IsLocked_Responses;
        let version: undefined | string;

        if (isFeatureV1) {
            isLocked = result as ProtoGet_IsLocked_Responses;
            version = 'v1';
        } else if (isFeatureV2) {
            isLocked = result as ProtoSubscribe_IsLocked_Responses;
            version = 'v2';
        }

        if (isLocked && version) {
            return {
                ...state,
                [serverId]: {
                    ...state[serverId],
                    isLocked: {
                        ...state[serverId]?.isLocked,
                        [version]: {
                            ...isLocked
                        }
                    }
                }
            }
        }
    }
    return state;
}
export default function Reducer(state: ServersMetaState = initialServerOpenState, action: ServerMetaActions): ServersMetaState {
    switch (action.type) {
        case WS_CALL_COMPLETE: {
            const call = action.payload.siLACall;
            const result = action.payload.result;
            try {
                return onResponse(state, call, JSON.parse(result));
            } catch (e) {
                console.warn('Failed to parse ws complete result', e);
            }
            return state;
        }
        case WS_PROPERTY_UPDATE: {
            const call = action.payload.baseCall;
            const result = action.payload.value;
            return onResponse(state, call, result);
        }
        case FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS: {
            const {serverMetadataAffectingCalls, serverUuid} = action.payload;
            const callsAffectedByMetadata = Object.entries(serverMetadataAffectingCalls).reduce((acc: ServerCallsAffectedByMetadata, entry) => {
                const metadata = entry[0];
                const calls = entry[1];
                return calls.reduce((currentCallsAcc: ServerCallsAffectedByMetadata, call) => {
                    return {
                        ...currentCallsAcc,
                        [call]: [...(currentCallsAcc[call] || []), metadata]
                    }
                }, acc);
            }, {});

            return {
                ...state,
                [serverUuid]: {
                    ...state[serverUuid],
                    callAffectedByMetadata: {
                        ...callsAffectedByMetadata
                    }
                }
            }
        }
        case FETCH_CALL_AFFECTED_BY_METADATA_FAILURE: {
            // todo handle
            return {
                ...state,
            }
        }
    }
    return state;
};