export const FETCH_CALL_AFFECTED_BY_METADATA_REQUEST = "front/server/meta/FETCH_CALL_AFFECTED_BY_METADATA_REQUEST";
export const FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS = "front/server/meta/FETCH_CALL_AFFECTED_BY_METADATA_SUCCESS";
export const FETCH_CALL_AFFECTED_BY_METADATA_FAILURE = "front/server/meta/FETCH_CALL_AFFECTED_BY_METADATA_FAILURE";