import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {FullyQualifiedCallId, ServerActiveCalls} from "./types";

export const getServerActiveCall = (state: AppState): ServerActiveCalls => state.serverActiveCall;
export const getServerIdActiveCall = (state: AppState, serverUuid: string) => serverUuid;


export const getServerIdActiveCallSelector: Selector<AppState, FullyQualifiedCallId> = createSelector(
    [getServerActiveCall, getServerIdActiveCall],
    (activeCalls, serverUuid) => activeCalls[serverUuid]
);