export const SET_SERVER_BOOKMARK = "front/server/bookmark/SET_SERVER_BOOKMARK";
export const ADD_SERVER_BOOKMARK = "front/server/bookmark/ADD_SERVER_BOOKMARK";
export const DELETE_SERVER_BOOKMARK = "front/server/bookmark/DELETE_SERVER_BOOKMARK";
export const FETCH_SERVERS_BOOKMARK_REQUEST = "front/server/bookmark/FETCH_SERVERS_BOOKMARK_REQUEST";
export const FETCH_SERVERS_BOOKMARK_SUCCESS = "front/server/bookmark/FETCH_SERVERS_BOOKMARK_SUCCESS";
export const FETCH_SERVERS_BOOKMARK_FAILURE =  "front/server/bookmark/FETCH_SERVERS_BOOKMARK_FAILURE";