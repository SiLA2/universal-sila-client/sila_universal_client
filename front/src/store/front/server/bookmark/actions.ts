import {
    FetchServersBookmarkFailure,
    FetchServersBookmarkFailurePayload,
    FetchServersBookmarkRequest, FetchServersBookmarkSuccess, FetchServersBookmarkSuccessPayload,
    ServerBookmark, ServerBookmarkAdd, ServerBookmarkDelete,
    ServerBookmarkSwitch, ServersBookmarkAdd, ServersBookmarkDelete,
} from "./types";
import {
    FETCH_SERVERS_BOOKMARK_FAILURE,
    FETCH_SERVERS_BOOKMARK_REQUEST,
    FETCH_SERVERS_BOOKMARK_SUCCESS, ADD_SERVER_BOOKMARK, DELETE_SERVER_BOOKMARK,
    SET_SERVER_BOOKMARK
} from "./actionTypes";

export const serverBookmarkSwitch = (
    payload: ServerBookmarkSwitch
): ServerBookmark => ({
    type: SET_SERVER_BOOKMARK,
    payload: payload
});

export const serverBookmarkAddRequest = (
    payload: ServerBookmarkAdd
): ServersBookmarkAdd => ({
    type: ADD_SERVER_BOOKMARK,
    payload: payload
});

export const serverBookmarkDeleteRequest = (
    payload: ServerBookmarkDelete
): ServersBookmarkDelete => ({
    type: DELETE_SERVER_BOOKMARK,
    payload: payload
});

export const fetchServersBookmarkRequest = (): FetchServersBookmarkRequest => ({
    type: FETCH_SERVERS_BOOKMARK_REQUEST,
});

export const fetchServersBookmarkSuccess = (
    payload: FetchServersBookmarkSuccessPayload
): FetchServersBookmarkSuccess => ({
    type: FETCH_SERVERS_BOOKMARK_SUCCESS,
    payload,
});

export const fetchServersBookmarkFailure = (
    payload: FetchServersBookmarkFailurePayload
): FetchServersBookmarkFailure => ({
    type: FETCH_SERVERS_BOOKMARK_FAILURE,
    payload,
});