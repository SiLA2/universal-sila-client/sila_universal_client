import { AppState } from "../../../rootReducer";

export const getServerBookmarks = (state: AppState) => state.serverBookmark;

