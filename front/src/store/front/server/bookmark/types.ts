import {
    FETCH_SERVERS_BOOKMARK_FAILURE,
    FETCH_SERVERS_BOOKMARK_REQUEST, FETCH_SERVERS_BOOKMARK_SUCCESS, ADD_SERVER_BOOKMARK, DELETE_SERVER_BOOKMARK,
    SET_SERVER_BOOKMARK,
} from "./actionTypes";
import {UscServer} from "../../../../generated/silaModels";

export type ServerBookmarkKeyValueState = {
    [serverUuid: string]: boolean;
}

export type ServerBookmarkSwitch = {
    serverUuid: string;
    bookmarked: boolean;
}

export type ServerBookmarkAdd = {
    serverUuid: string;
}

export type ServerBookmarkDelete = {
    serverUuid: string;
}

export type ServerBookmark = {
    type: typeof SET_SERVER_BOOKMARK;
    payload: ServerBookmarkSwitch;
};

export type FetchServersBookmarkSuccessPayload = {
    servers: UscServer[];
}

export type FetchServersBookmarkFailurePayload = {
    error: string;
}

export type FetchServersBookmarkRequest = {
    type: typeof FETCH_SERVERS_BOOKMARK_REQUEST;
}

export type FetchServersBookmarkSuccess = {
    type: typeof FETCH_SERVERS_BOOKMARK_SUCCESS;
    payload: FetchServersBookmarkSuccessPayload;
};

export type FetchServersBookmarkFailure = {
    type: typeof FETCH_SERVERS_BOOKMARK_FAILURE;
    payload: FetchServersBookmarkFailurePayload;
};

export type ServersBookmarkAdd = {
    type: typeof ADD_SERVER_BOOKMARK;
    payload: ServerBookmarkAdd;
};

export type ServersBookmarkDelete = {
    type: typeof DELETE_SERVER_BOOKMARK;
    payload: ServerBookmarkDelete;
};


export type ServerBookmarkActions =
    | FetchServersBookmarkRequest
    | FetchServersBookmarkSuccess
    | FetchServersBookmarkFailure
    | ServersBookmarkAdd
    | ServersBookmarkDelete
    | ServerBookmark;