
export const FETCH_PLUGINS_REQUEST = "front/app/plugin/FETCH_PLUGINS_REQUEST";
export const FETCH_PLUGINS_SUCCESS = "front/app/plugin/FETCH_PLUGINS_SUCCESS";
export const FETCH_PLUGINS_FAILURE = "front/app/plugin/FETCH_PLUGINS_FAILURE";
export const ON_PLUGINS_LOADING = "front/app/plugin/ON_PLUGINS_LOADING";
export const ON_PLUGINS_LOADED = "front/app/plugin/ON_PLUGIN_LOADED";
