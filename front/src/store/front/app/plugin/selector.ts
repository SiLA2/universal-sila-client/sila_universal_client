import { AppState } from "../../../rootReducer";

export const getAppPlugins = (state: AppState) => state.appPlugins;
export const getAppPluginsLoaded = (state: AppState) => state.appPlugins.loaded;

