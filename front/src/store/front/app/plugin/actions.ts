import {
    FetchPluginsFailure,
    FetchPluginsFailurePayload,
    FetchPluginsRequest,
    FetchPluginsSuccess,
    FetchPluginsSuccessPayload, OnPluginsLoaded,
    OnPluginsLoading,
} from "./types";

import {FETCH_PLUGINS_FAILURE, FETCH_PLUGINS_REQUEST, FETCH_PLUGINS_SUCCESS, ON_PLUGINS_LOADING, ON_PLUGINS_LOADED} from "./actionTypes";

export const fetchPluginsRequest = (): FetchPluginsRequest => ({
    type: FETCH_PLUGINS_REQUEST,
});

export const fetchPluginsSuccess = (
    payload: FetchPluginsSuccessPayload
): FetchPluginsSuccess => ({
    type: FETCH_PLUGINS_SUCCESS,
    payload,
});

export const fetchPluginsFailure = (
    payload: FetchPluginsFailurePayload
): FetchPluginsFailure => ({
    type: FETCH_PLUGINS_FAILURE,
    payload,
});

export const onPluginsLoading = (): OnPluginsLoading => ({
    type: ON_PLUGINS_LOADING,
});

export const onPluginsLoaded = (): OnPluginsLoaded => ({
    type: ON_PLUGINS_LOADED,
});