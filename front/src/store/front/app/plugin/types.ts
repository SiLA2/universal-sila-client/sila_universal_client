import {
    FETCH_PLUGINS_FAILURE,
    FETCH_PLUGINS_REQUEST,
    FETCH_PLUGINS_SUCCESS,
    ON_PLUGINS_LOADING,
    ON_PLUGINS_LOADED
} from "./actionTypes";
import {PluginModel} from "../../../../generated/silaModels";

export type AppPlugins = {
    plugins: PluginModel[],
    loaded: boolean
}

export type FetchPluginsSuccessPayload = {
    plugins: PluginModel[];
}

export type FetchPluginsFailurePayload = {
    error: string;
}

export type FetchPluginsRequest = {
    type: typeof FETCH_PLUGINS_REQUEST;
}

export type OnPluginsLoading = {
    type: typeof ON_PLUGINS_LOADING;
}

export type OnPluginsLoaded = {
    type: typeof ON_PLUGINS_LOADED;
}

export type FetchPluginsSuccess = {
    type: typeof FETCH_PLUGINS_SUCCESS;
    payload: FetchPluginsSuccessPayload;
};

export type FetchPluginsFailure = {
    type: typeof FETCH_PLUGINS_FAILURE;
    payload: FetchPluginsFailurePayload;
};

export type AppSettingActions =
    FetchPluginsRequest |
    FetchPluginsSuccess |
    FetchPluginsFailure |
    OnPluginsLoading |
    OnPluginsLoaded;