import {AppSettingActions, AppPlugins} from "./types";
import {
    FETCH_PLUGINS_FAILURE,
    FETCH_PLUGINS_REQUEST,
    FETCH_PLUGINS_SUCCESS,
    ON_PLUGINS_LOADED,
    ON_PLUGINS_LOADING
} from "./actionTypes";

const initialState: AppPlugins = {
    plugins: [],
    loaded: false
};

export default function Reducer(state: AppPlugins = initialState, action: AppSettingActions): AppPlugins {
    switch (action.type) {
        case FETCH_PLUGINS_REQUEST:
            return {
                ...state,
            };
        case FETCH_PLUGINS_SUCCESS:
            return {
                ...state,
                plugins: action.payload.plugins
            };
        case FETCH_PLUGINS_FAILURE:
            // todo handle
            return {
                ...state,
            };
        case ON_PLUGINS_LOADING:
            return {
                ...state,
                loaded: false
            };
        case ON_PLUGINS_LOADED:
            return {
                ...state,
                loaded: true
            };
    }
    return state;
};