import {
    ADD_DEV_PLUGIN,
    REMOVE_DEV_PLUGIN,
} from "./actionTypes";

export type DevFrontPlugins = {
    [devPluginUrl: string]: null
}

export type DevState = {
    frontPlugins: DevFrontPlugins;
}

export type AddDevFrontPluginPayload = {
    url: string;
}

export type RemoveDevFrontPluginPayload = {
    url: string;
}

export type AddDevFrontPlugin = {
    type: typeof ADD_DEV_PLUGIN;
    payload: AddDevFrontPluginPayload;
};

export type RemoveDevFrontPlugin = {
    type: typeof REMOVE_DEV_PLUGIN;
    payload: RemoveDevFrontPluginPayload;
};

export type DevActions =
    AddDevFrontPlugin |
    RemoveDevFrontPlugin;