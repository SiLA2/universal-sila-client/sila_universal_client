import {
    SET_SERVER_TABS_STORE_IN_MEMORY,
    SET_SIDEBAR_DEFAULT_EXPANDED,
    SET_THEME_MODE, SET_TIMEZONE_MODE, SET_CUSTOM_TIMEZONE_NAME, SET_ADVANCED_USER,
} from "./actionTypes";

export type AppTimezoneSettings = {
    useSystemTimezone: boolean;
    customTimezoneName: string;
}

export type AppSettings = {
    darkMode: boolean;
    sidebarExpandedByDefault: boolean;
    serverTabsStoreInMemory: boolean;
    isAdvancedUser: boolean;
    timezoneSettings: AppTimezoneSettings;
}

export type ThemeModePayload = {
    darkMode: boolean;
}

export type SidebarDefaultExpandedPayload = {
    sidebarExpandedByDefault: boolean;
}

export type ServerTabsStoreInMemoryPayload = {
    serverTabsStoreInMemory: boolean;
}

export type SetAdvancedUserPayload = {
    isAdvancedUser: boolean;
}

export type TimezoneModePayload = {
    useSystemTimezone: boolean;
}

export type TimezoneNamePayload = {
    customTimezoneName: string;
}

export type SetThemeMode = {
    type: typeof SET_THEME_MODE;
    payload: ThemeModePayload;
};

export type SetSidebarDefaultExpanded = {
    type: typeof SET_SIDEBAR_DEFAULT_EXPANDED;
    payload: SidebarDefaultExpandedPayload;
};

export type SetServerTabsStoreInMemory = {
    type: typeof SET_SERVER_TABS_STORE_IN_MEMORY;
    payload: ServerTabsStoreInMemoryPayload;
};

export type SetTimezoneMode = {
    type: typeof SET_TIMEZONE_MODE;
    payload: TimezoneModePayload;
};

export type SetCustomTimezoneName = {
    type: typeof SET_CUSTOM_TIMEZONE_NAME;
    payload: TimezoneNamePayload;
};

export type SetAdvancedUser = {
    type: typeof SET_ADVANCED_USER;
    payload: SetAdvancedUserPayload;
};

export type AppSettingActions =
    SetThemeMode |
    SetSidebarDefaultExpanded |
    SetServerTabsStoreInMemory |
    SetTimezoneMode |
    SetCustomTimezoneName |
    SetAdvancedUser;