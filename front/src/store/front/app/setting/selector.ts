import { AppState } from "../../../rootReducer";

export const getAppSettings = (state: AppState) => state.appSettings;
export const getAppTimezoneSettings = (state: AppState) => state.appSettings.timezoneSettings;
export const getAppIsAdvancedUser = (state: AppState) => state.appSettings.isAdvancedUser;

