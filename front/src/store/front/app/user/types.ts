import {
    REMOVE_USER_ACCESS_TOKEN, REMOVE_USER_LOCK_TOKEN, SET_EXPIRED_USER_ACCESS_TOKEN, SET_EXPIRED_USER_LOCK_TOKEN,
    SET_USER_ACCESS_TOKEN, SET_USER_LOCK_TOKEN,
} from "./actionTypes";
import {DateTime} from "luxon";

export type AppUserState = {
    servers: UserServers;
}

export type UserServerAccessToken = {
    token: string;
    authorizedByServerUuid: string;
    expirationUTC?: string | null; // undefined = expired
    fullyQualifiedFeatureId: string;
}

export type UserServerLockToken = {
    token: string;
    expirationUTC?: string | null; // undefined = expired
    fullyQualifiedFeatureId: string;
}

export type UserServer = {
    accessToken?: UserServerAccessToken;
    lockToken?: UserServerLockToken;
}

export type UserServers = {
    [uuid:string]: UserServer;
}

export type SetUserAccessTokenPayload = {
    serverUuid: string;
    authorizedByServerUuid: string;
    token: string;
    expirationUTC: DateTime;
    fullyQualifiedFeatureId: string;
}

export type SetUserAccessToken = {
    type: typeof SET_USER_ACCESS_TOKEN;
    payload: SetUserAccessTokenPayload;
};

export type SetExpiredUserAccessTokenPayload = {
    serverUuid: string;
    token: string;
    fullyQualifiedFeatureId: string;
}

export type SetExpiredUserAccessToken = {
    type: typeof SET_EXPIRED_USER_ACCESS_TOKEN;
    payload: SetExpiredUserAccessTokenPayload;
};


export type RemoveUserAccessTokenPayload = {
    serverUuid: string;
}

export type RemoveUserAccessToken = {
    type: typeof REMOVE_USER_ACCESS_TOKEN;
    payload: RemoveUserAccessTokenPayload;
};

export type SetUserLockTokenPayload = {
    serverUuid: string;
    token: string;
    expirationUTC: DateTime;
    fullyQualifiedFeatureId: string;
}

export type SetUserLockToken = {
    type: typeof SET_USER_LOCK_TOKEN;
    payload: SetUserLockTokenPayload;
};

export type SetExpiredUserLockTokenPayload = {
    serverUuid: string;
    token: string;
    fullyQualifiedFeatureId: string;
}

export type SetExpiredUserLockToken = {
    type: typeof SET_EXPIRED_USER_LOCK_TOKEN;
    payload: SetExpiredUserLockTokenPayload;
};


export type RemoveUserLockTokenPayload = {
    serverUuid: string;
}

export type RemoveUserLockToken = {
    type: typeof REMOVE_USER_LOCK_TOKEN;
    payload: RemoveUserLockTokenPayload;
};

export type AppSettingActions =
    SetUserAccessToken |
    SetExpiredUserAccessToken |
    RemoveUserAccessToken |
    SetUserLockToken |
    SetExpiredUserLockToken |
    RemoveUserLockToken;
