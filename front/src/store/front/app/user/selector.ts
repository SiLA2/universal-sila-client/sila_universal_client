import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {UserServer, UserServerAccessToken, UserServerLockToken} from "./types";

export const getAppUser = (state: AppState) => state.appUser;
export const getAppUserForServerId = (state: AppState, uuid: string) => uuid;

export const getAppUserForServerIdSelector: Selector<AppState, UserServer | undefined> = createSelector(
    [getAppUser, getAppUserForServerId],
    (userApp, serverUuid) => userApp.servers[serverUuid]
);

export const getAppUserAccessTokenForServerIdSelector: Selector<AppState, UserServerAccessToken | undefined> = createSelector(
    [getAppUser, getAppUserForServerId],
    (userApp, serverUuid) => userApp.servers[serverUuid]?.accessToken
);

export const getAppUserLockTokenForServerIdSelector: Selector<AppState, UserServerLockToken | undefined> = createSelector(
    [getAppUser, getAppUserForServerId],
    (userApp, serverUuid) => userApp.servers[serverUuid]?.lockToken
);