import {AppState} from "../../rootReducer";

export const getIsConnected = (state: AppState) => state.ws.isConnected;
export const getLostConnection = (state: AppState) => state.ws.lostConnection;
export const getClient = (state: AppState) => state.ws.client;

