import {call, put, take} from 'redux-saga/effects'
import {eventChannel} from 'redux-saga'
import {Client, Message} from "@stomp/stompjs";
import {
    WS_CALL_COMPLETE, WS_CALL_ERROR, WS_CALL_EXEC_INFO, WS_CALL_INIT, WS_CALL_INTERMEDIATE,
    WS_CALL_PROGRESS,
    WS_CONNECTED,
    WS_DISCONNECTED, WS_PROPERTY_UPDATE,
    WS_SERVER_ADDITION,
    WS_SERVER_DELETE, WS_SERVER_FAIL,
    WS_SERVER_UPDATE
} from "./actionTypes";
import WebSocketEvent from "../../../websocket/WebSocketEvent";
import Websocket from "../../../websocket/Websocket";
import NotificationService from '../../../services/NotificationService';

// todo refactor those 3 similar functions

function subscribeToCallProgress() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onCallProgress = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/call/progress', onCallProgress);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToCallComplete() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onCallComplete = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/call/complete', onCallComplete);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}


function subscribeToCallError() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onCallError = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/call/error', onCallError);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToCallInit() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onCallInit = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/call/init', onCallInit);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToCallExecInfo() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onCallExecInfo = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/call/exec-info', onCallExecInfo);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToCallIntermediate() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onCallIntermediate = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/call/intermediate', onCallIntermediate);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToObservablePropertyUpdate() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onObservablePropertyUpdate = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/call/propertyUpdate', onObservablePropertyUpdate);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToServerAddition() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onServerAddition = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/server/add', onServerAddition);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToServerUpdate() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onServerUpdate = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/server/update', onServerUpdate);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToServerDelete() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onServerRemove = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/server/remove', onServerRemove);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

function subscribeToServerFail() {
    return eventChannel(emit => {
        const subscribeAndListen = (socket: Client | null) => {
            if (socket === null) {
                return
            }
            const onServerFail = (message:Message) => {
                emit(message.body)
            }

            socket.subscribe('/event/server/fail', onServerFail);
        }
        Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, subscribeAndListen)
        if (Websocket.client?.connected) {
            subscribeAndListen(Websocket.client)
        }
        return () => {}
    })
}

export function* serverFail(): any {
    const serverFail = yield call(subscribeToServerFail);
    while (true) {
        try {
            const payload = yield take(serverFail)
            const parsedPayload = JSON.parse(payload);
            yield put({ type: WS_SERVER_FAIL, payload: parsedPayload })

            NotificationService.showNotification(
                'error',
                'Server failed to be added',
                {
                    host: parsedPayload.host,
                    port: parsedPayload.port,
                    certificate: parsedPayload.certificate
                }
            );
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

function subscribeToWsConnection() {
    return eventChannel(emit => {
        const unsubscribe = Websocket.eventEmitter.on(WebSocketEvent.ON_CONNECTION, (client, frame) => {
            emit(client)
        });
        return () => {
            unsubscribe()
        }
    })
}

function subscribeToWsDisconnection() {
    return eventChannel(emit => {
        const unsubscribe = Websocket.eventEmitter.on(WebSocketEvent.ON_ERROR, (error) => {
            emit(error)
        });
        return () => {
            unsubscribe()
        }
    })
}

export function* callProgress(): any {
    const callProgress = yield call(subscribeToCallProgress);
    while (true) {
        try {
            const payload = yield take(callProgress)
            yield put({ type: WS_CALL_PROGRESS, payload: {...JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* callComplete(): any {
    const callComplete = yield call(subscribeToCallComplete);
    while (true) {
        try {
            const payload = yield take(callComplete)
            yield put({ type: WS_CALL_COMPLETE, payload: {...JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* callError(): any {
    const callError = yield call(subscribeToCallError);
    while (true) {
        try {
            const payload = yield take(callError)
            yield put({ type: WS_CALL_ERROR, payload: {...JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* callInit(): any {
    const callInit = yield call(subscribeToCallInit);
    while (true) {
        try {
            const payload = yield take(callInit)
            yield put({ type: WS_CALL_INIT, payload: {...JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* callExecInfo(): any {
    const callExecInfo = yield call(subscribeToCallExecInfo);
    while (true) {
        try {
            const payload = yield take(callExecInfo)
            yield put({ type: WS_CALL_EXEC_INFO , payload: {...JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* callIntermediate(): any {
    const callIntermediate = yield call(subscribeToCallIntermediate);
    while (true) {
        try {
            const payload = yield take(callIntermediate)
            yield put({ type: WS_CALL_INTERMEDIATE, payload: {...JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* observablePropertyUpdate(): any {
    const propertyUpdate = yield call(subscribeToObservablePropertyUpdate);
    while (true) {
        try {
            const payload = yield take(propertyUpdate)
            const json = JSON.parse(payload);
            yield put({ type: WS_PROPERTY_UPDATE, payload: {
                ...json,
                value: JSON.parse(json.value)
            }});
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* serverAddition(): any {
    const serverAddition = yield call(subscribeToServerAddition);
    while (true) {
        try {
            const payload = yield take(serverAddition)
            yield put({ type: WS_SERVER_ADDITION, payload: {server: JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* serverUpdate(): any {
    const serverUpdate = yield call(subscribeToServerUpdate);
    while (true) {
        try {
            const payload = yield take(serverUpdate)
            yield put({ type: WS_SERVER_UPDATE, payload: {server: JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* serverDelete(): any {
    const serverDelete = yield call(subscribeToServerDelete);
    while (true) {
        try {
            const payload = yield take(serverDelete)
            yield put({ type: WS_SERVER_DELETE, payload: {uuid: JSON.parse(payload)} })
        } catch(err) {
            console.error('socket error:', err)
        }
    }
}

export function* wsConnection(): any {
    const wsConnection = yield call(subscribeToWsConnection);
    while (true) {
        const payload = yield take(wsConnection)
        yield put({ type: WS_CONNECTED, payload: { client: payload }});
    }
}

export function* wsDisconnection(): any {
    const wsDisconnection = yield call(subscribeToWsDisconnection);
    while (true) {
        const payload = yield take(wsDisconnection)
        yield put({ type: WS_DISCONNECTED, payload: { error: payload.error }});
    }
}

