import {
    WS_CALL_COMPLETE, WS_CALL_ERROR, WS_CALL_EXEC_INFO, WS_CALL_INIT, WS_CALL_INTERMEDIATE,
    WS_CALL_PROGRESS,
    WS_CONNECTED, WS_DISCONNECTED, WS_PROPERTY_UPDATE,
    WS_SERVER_ADDITION, WS_SERVER_DELETE, WS_SERVER_FAIL, WS_SERVER_UPDATE,
} from "./actionTypes";
import {
    WsCallComplete, WsCallError, WsCallExecInfo, WsCallInit, WsCallIntermediate,
    WsCallProgress,
    WsConnected,
    WsConnectedPayload,
    WsDisconnected,
    WsDisconnectedPayload, WsPropertyUpdate,
    WsServerAddition,
    WsServerAdditionPayload,
    WsServerDelete,
    WsServerDeletePayload, WsServerFail, WsServerFailPayload,
    WsServerUpdate,
    WsServerUpdatePayload
} from "./types";
import {
    ObservableCommandInit, ObservableExecutionInfo, ObservableIntermediateResponse, ObservablePropertyResponse,
    SiLAJavaCallCompleted,
    SiLAJavaCallErrored,
    SiLAJavaCallStarted
} from "../../../generated/silaModels";

export const wsCallProgress = (
    payload: SiLAJavaCallStarted
): WsCallProgress => ({
    type: WS_CALL_PROGRESS,
    payload,
});

export const wsCallComplete = (
    payload: SiLAJavaCallCompleted
): WsCallComplete => ({
    type: WS_CALL_COMPLETE,
    payload,
});

export const wsCallError = (
    payload: SiLAJavaCallErrored
): WsCallError => ({
    type: WS_CALL_ERROR,
    payload,
});

export const wsCallInit = (
    payload: ObservableCommandInit
): WsCallInit => ({
    type: WS_CALL_INIT,
    payload,
});

export const wsCallExecInfo = (
    payload: ObservableExecutionInfo
): WsCallExecInfo => ({
    type: WS_CALL_EXEC_INFO,
    payload,
});

export const wsCallIntermediate = (
    payload: ObservableIntermediateResponse
): WsCallIntermediate => ({
    type: WS_CALL_INTERMEDIATE,
    payload,
});

export const wsObservablePropertyUpdate = (
    payload: ObservablePropertyResponse
): WsPropertyUpdate => ({
    type: WS_PROPERTY_UPDATE,
    payload,
});

export const wsServerAddition = (
    payload: WsServerAdditionPayload
): WsServerAddition => ({
    type: WS_SERVER_ADDITION,
    payload,
});

export const wsServerUpdate = (
    payload: WsServerUpdatePayload
): WsServerUpdate => ({
    type: WS_SERVER_UPDATE,
    payload,
});

export const wsServerDelete = (
    payload: WsServerDeletePayload
): WsServerDelete => ({
    type: WS_SERVER_DELETE,
    payload,
});

export const wsServerFail = (
    payload: WsServerFailPayload
): WsServerFail => ({
    type: WS_SERVER_FAIL,
    payload,
});

export const wsConnected = (
    payload: WsConnectedPayload
): WsConnected => ({
    type: WS_CONNECTED,
    payload,
});

export const wsDisconnected = (
    payload: WsDisconnectedPayload
): WsDisconnected => ({
    type: WS_DISCONNECTED,
    payload,
});