import {FETCH_SERVERS_FAILURE, FETCH_SERVERS_REQUEST, FETCH_SERVERS_SUCCESS,} from "./actionTypes";

import {ServerActions, ServerState} from "./types";
import {WS_SERVER_ADDITION, WS_SERVER_DELETE, WS_SERVER_UPDATE} from "../../websocket/actionTypes";

const initialState: ServerState = {
    servers: {},
    pending: true,
    error: null
};

export default function Reducer(state = initialState, action: ServerActions): ServerState {
    switch (action.type) {
        case FETCH_SERVERS_REQUEST:
            return {
                ...state,
                pending: true,
            };
        case FETCH_SERVERS_SUCCESS:
            return {
                ...state,
                pending: false,
                servers: {
                    // override the previous servers state
                    ...action.payload.servers.reduce((acc,server) => (
                        {...acc,[server.serverUuid]: server}
                    ),{})
                },
                error: null,
            };
        case FETCH_SERVERS_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.payload.error,
            };
        case WS_SERVER_ADDITION:
            return {
                ...state,
                servers: {
                    ...state.servers,
                    ...{[action.payload.server.serverUuid]: action.payload.server}
                },
            };
        case WS_SERVER_UPDATE:
            return {
                ...state,
                servers: {
                    ...state.servers,
                    ...{[action.payload.server.serverUuid]: action.payload.server}
                },
            };
        case WS_SERVER_DELETE:
            const { [action.payload.uuid]: remove, ...rest } = state.servers
            return {
                ...state,
                servers: rest,
            };
    }
    return state;
};