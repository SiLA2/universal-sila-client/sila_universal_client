import {createSelector, Selector} from "reselect";

import { AppState } from "../../../rootReducer";
import {UscFeature, UscServer} from "../../../../generated/silaModels";

export const getPending = (state: AppState) => state.server.pending;

export const getError = (state: AppState) => state.server.error;

export const getServersState = (state: AppState) => state.server;
export const getServers = (state: AppState) => state.server.servers;
export const getServerById = (state: AppState, uuid: string) => uuid;

export const getServerByIdSelector: Selector<AppState, UscServer> = createSelector(
    [getServers, getServerById],
    (servers, serverUuid) => servers[serverUuid]
);

export const getServerFeaturesByIdSelector: Selector<AppState, UscFeature[]> = createSelector(
    [getServers, getServerById],
    (servers, serverUuid) => (servers[serverUuid]?.features || [])
);
