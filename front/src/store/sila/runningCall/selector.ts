import { AppState } from "../../rootReducer";

export const getRunningCall = (state: AppState) => state.runningCall;

