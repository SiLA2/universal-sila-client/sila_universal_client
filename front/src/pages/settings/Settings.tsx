import React, {useEffect, useState} from 'react';
import {
    Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Switch, Typography, Container
} from '@mui/material/';
import {PluginModel} from "../../generated/silaModels";
import Plugin from '../../components/Plugin';
import AddIcon from "@mui/icons-material/Add";
import {removePersistedUscState} from "../../store";
import {useDispatch, useSelector} from "react-redux";
import {getAppSettings} from "../../store/front/app/setting/selector";
import {
    setAdvancedUser,
    setCustomTimezoneName,
    setServerTabsStoreInMemory,
    setSidebarDefaultExpanded,
    setTimezoneMode
} from "../../store/front/app/setting/actions";
import { getBackEndpoint } from '../../utils/endpoint';
import {getBackUrl, setBackUrl} from "../../utils/localstorage";
import {DialogContentText, TextField, Autocomplete, SxProps, Grid} from "@mui/material";
import {getSystemDefaultTimezoneName, getTimezones} from "../../utils/timezone";
import Divider from "@mui/material/Divider";

export default function Settings(): JSX.Element {
    const [plugins, setPlugins] = useState<PluginModel[]>([]);
    const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
    const [inputBackUrl, setInputBackUrl] = useState<string>(getBackUrl() || window.location.origin || '');
    const { sidebarExpandedByDefault, serverTabsStoreInMemory, timezoneSettings, isAdvancedUser } = useSelector(getAppSettings);
    const timezones = getTimezones();
    const dispatch = useDispatch();
    useEffect(() => {
        fetch(getBackEndpoint('/api/plugin'), {
            method: 'GET', // or 'PUT'
        })
            .then(response => response.json())
            .then((data) => {
                setPlugins(data);
            })
            .catch((error) => {
                console.error(error);
                setPlugins([]);
            })
    }, [])

    return (
        <Container maxWidth={'lg'}>
            <Box>
                <Typography variant="h1" component="h2" gutterBottom>
                    Settings
                </Typography>
                <Typography variant="h2" component="h2" gutterBottom>
                    Timezone
                </Typography>
                <Box>
                    <Typography>Use a custom timezone instead of system default ({getSystemDefaultTimezoneName()})</Typography>
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>No</Grid>
                        <Grid item>
                            <Switch checked={!timezoneSettings.useSystemTimezone} onClick={() => {
                                dispatch(setTimezoneMode({useSystemTimezone: !timezoneSettings.useSystemTimezone}))
                            }}/>
                        </Grid>
                        <Grid item>Yes</Grid>
                    </Grid>
                    {
                        !timezoneSettings.useSystemTimezone && (
                            <Autocomplete
                                disablePortal
                                options={timezones}
                                sx={{ width: 300 }}
                                disabled={timezoneSettings.useSystemTimezone}
                                value={timezoneSettings.customTimezoneName}
                                onChange={(event, newValue) => {
                                    if (newValue != null && newValue.length > 0) {
                                        dispatch(setCustomTimezoneName({customTimezoneName: newValue as string}))
                                    }
                                }}
                                renderInput={(params) => <TextField {...params} label="Timezone" />}
                            />
                        )
                    }
                </Box>
                <Divider sx={DividerStyle}/>
                <Typography variant="h2" component="h2" gutterBottom>
                    Cache / Cookies
                </Typography>
                <Typography>
                    Clear the local cache / cookies / local settings from your browser.
                </Typography>
                <Typography>
                    Useful if the application is becoming slow / unstable or to reset the browser application with default settings.
                </Typography>
                <Button variant="contained" color="primary" onClick={() => {
                    removePersistedUscState();
                    window.location.reload();
                }}>
                    Clear local cache & Reload page
                </Button>
                <Divider sx={DividerStyle}/>
                <Typography variant="h2" component="h2" gutterBottom>
                    Sidebar
                </Typography>
                <Box>
                    <Typography>Expand sidebar by default</Typography>
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>No</Grid>
                        <Grid item>
                            <Switch checked={sidebarExpandedByDefault} onClick={
                                () => dispatch(setSidebarDefaultExpanded({sidebarExpandedByDefault: !sidebarExpandedByDefault}))
                            }/>
                        </Grid>
                        <Grid item>Yes</Grid>
                    </Grid>
                </Box>
                <Divider sx={DividerStyle}/>
                <Typography variant="h2" component="h2" gutterBottom>
                    Advanced user
                </Typography>
                <Box>
                    <Typography>Enable advanced user mode to display functionalities that are either planned or not finished and developer utilities.</Typography>
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>No</Grid>
                        <Grid item>
                            <Switch checked={isAdvancedUser} onClick={
                                () => dispatch(setAdvancedUser({isAdvancedUser: !isAdvancedUser}))
                            }/>
                        </Grid>
                        <Grid item>Yes</Grid>
                    </Grid>
                </Box>
                <Divider sx={DividerStyle}/>
                <Typography variant="h2" component="h2" gutterBottom>
                    Performance
                </Typography>
                <Box>
                    <Typography>keep server tabs in memory.</Typography>
                    <Typography>
                        If enable, servers open as tab will be kept in memory to prevent them for re-rendering everytime another page or sever is selected.
                        It is recommended to disable this option when using a device with very limited resources or when there are many servers open and experiencing application latency/freeze.
                    </Typography>
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>No</Grid>
                        <Grid item>
                            <Switch checked={serverTabsStoreInMemory} onClick={
                                () => dispatch(setServerTabsStoreInMemory({serverTabsStoreInMemory: !serverTabsStoreInMemory}))
                            }/>
                        </Grid>
                        <Grid item>Yes</Grid>
                    </Grid>
                </Box>
                <Divider sx={DividerStyle}/>
                <Typography variant="h2" component="h2" gutterBottom>
                    Universal SiLA Client Server
                </Typography>
                <Typography>
                    This option allows to use a custom Universal SiLA Client Server (Back-End API) instead of the default one.
                </Typography>
                <Typography>
                    It is especially useful when using the desktop application (which is serverless) or for development debugging.
                </Typography>
                <Grid container display={"flex"} marginTop={2} rowSpacing={2} columnSpacing={2} maxWidth={'1024px'}>
                    <Grid item xs={12} sm={8}>
                        <TextField
                            label="Universal SiLA Client Server URL"
                            value={inputBackUrl}
                            fullWidth
                            onChange={(e) => setInputBackUrl(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4} display={"flex"}>
                        <Button variant="contained" color="primary" onClick={() => {
                            setBackUrl(inputBackUrl);
                            window.location.reload();
                        }}>
                            Apply & Reload page
                        </Button>
                    </Grid>
                </Grid>
                <Divider sx={DividerStyle}/>
                <Typography variant="h2" component="h2" gutterBottom>
                    Plugins
                </Typography>
                <Dialog
                    open={isDialogOpen}
                    onClose={() => setIsDialogOpen(false)}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Plugin warning"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Uploading a plugin can prevent the application from working.
                            After the upload is finished, the application will restart itself automatically and will
                            cancel all tasks in progress for all users using this application.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button  variant={"outlined"} onClick={() => setIsDialogOpen(false)} autoFocus>Cancel</Button>
                        <Button variant="outlined" color="warning" component="label">
                            Proceed
                            <input
                                type="file"
                                hidden
                                onChange={(e) => {
                                    setIsDialogOpen(false)
                                    const files = e.target.files;
                                    if (files && files.length) {
                                        const formData = new FormData();
                                        formData.append('file', files[0]);
                                        fetch(getBackEndpoint('/api/plugin/add'), {method: 'POST', body: formData})
                                            .then((data) => {
                                                console.log(data);
                                                console.log("plugin add response");
                                            })
                                            .catch((error) => {
                                                console.error(error);
                                            })
                                    }
                                }}
                            />
                        </Button>
                    </DialogActions>
                </Dialog>
                <Typography>
                    Plugins are application extensions made by the community which extends or changes the Universal SiLA Client functionalities.
                </Typography>
                <Typography>
                    Plugins may not be approved/verified and can also be the cause of instabilities/latency/crash.
                </Typography>
                <Button variant="contained" color="primary" component="label" onClick={() => setIsDialogOpen(true)}>
                    Upload Plugin
                    <AddIcon/>
                </Button>
                {
                    (plugins.length) ? (
                        plugins.map((p) => <Plugin key={p.name} plugin={p}/>)
                    ) : (
                        <Typography>No loaded plugins</Typography>
                    )
                }
            </Box>
        </Container>
    );
}

const DividerStyle: SxProps = {
    marginY: '16px'
}