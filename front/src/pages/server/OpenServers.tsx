import React from "react";
import {ServerOpenKeyValueState} from "../../store/front/server/open/types";
import {useSelector} from "react-redux";
import {getServerOpen} from "../../store/front/server/open/selector";
import {AppSettings} from "../../store/front/app/setting/types";
import {getAppSettings} from "../../store/front/app/setting/selector";
import {getServers} from "../../store/sila/manager/server/selector";
import ServerConfigurationContainer from "./config/ServerConfig";
import ServerProperty from "./property/ServerProperty";
import ServerCommand from "./command/ServerCommand";
import PluginsValue from "../../plugin/PluginsValue";

const OpenServers = React.memo((): JSX.Element => {
    const serverOpen: ServerOpenKeyValueState = useSelector(getServerOpen);
    const settings: AppSettings = useSelector(getAppSettings);
    const servers = useSelector(getServers); // todo select only those open
    const pluginsServerRootRoutes = (PluginsValue('PluginRootServerRoute') || [])
        .flat()
        .map(v => v.routes)
        .flat();
    const getVisibilityStyle = (hiddenCondition: boolean): any => {
        if (!hiddenCondition) {
            return {
                display: 'none',
                height: 0,
            };
        }
        return {
            height: '100%'
        };
    };

    const elements = Object.keys(serverOpen.serversTabs)
        .filter((serverId) => (settings.serverTabsStoreInMemory) ? true : serverOpen.serverUuidOpen === serverId)
        .map((serverId) => {
            const server = servers[serverId];
            if (!server) {
                return null;
            }
            const isCurrentServerOpen = serverOpen.serverUuidOpen === serverId;
            const currentServerTab = serverOpen.serversTabs[serverId];
            return (
                <React.Fragment key={serverId}>
                    <div key={'info'} style={getVisibilityStyle(isCurrentServerOpen && currentServerTab === 'info')}>
                        <ServerConfigurationContainer server={server}/>
                    </div>
                    <div key={'property'}
                         style={getVisibilityStyle(isCurrentServerOpen && currentServerTab === 'property')}>
                        <ServerProperty server={server}/>
                    </div>
                    <div key={'command'}
                         style={getVisibilityStyle(isCurrentServerOpen && currentServerTab === 'command')}>
                        <ServerCommand server={server}/>
                    </div>
                    {
                        pluginsServerRootRoutes.map(r => (
                            <div key={r.path}
                                 style={getVisibilityStyle(isCurrentServerOpen && currentServerTab === r.path)}>
                                {r.element}
                            </div>
                        ))
                    }
                </React.Fragment>
            )
        });
    return (
        <>
            {elements}
        </>
    );
});

export default OpenServers;