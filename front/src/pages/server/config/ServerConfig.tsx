import React, {useEffect} from 'react';
import {ServerHostPortCertificate, SiLACallModel, UscCall, UscServer} from '../../../generated/silaModels';
import {
    Accordion, AccordionDetails,
    AccordionSummary, Alert,
    Box, Button, Checkbox, Chip, Divider,
    FormControlLabel, Grid, InputAdornment, Link, Modal, Stack, TextField, Tooltip, TooltipProps,
    Typography,
} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
    serverFeatureSelection, serverFeatureSelections,
} from "../../../store/front/server/feature/actions";
import {getServerFeatures} from "../../../store/front/server/feature/selector";
import {ServerFeatureSelectedKeyValue} from "../../../store/front/server/feature/types";
import SplitScreen from "../../../components/common/SplitScreen";
import HttpsOutlinedIcon from '@mui/icons-material/HttpsOutlined';
import WarningAmberOutlinedIcon from '@mui/icons-material/WarningAmberOutlined';
import CloudOutlinedIcon from '@mui/icons-material/CloudOutlined';
import CloudOffOutlinedIcon from '@mui/icons-material/CloudOffOutlined';
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import {urlToHREF} from "../../../utils/url";
import {getBackEndpoint} from "../../../utils/endpoint";
import {fetchResponseFromCacheRequest} from "../../../store/front/server/response/actions";
import AccessToken from "./AccessToken";
import ConnectionConfigurationModal from "./modal/ConnectionConfiguration";
import {AppState} from "../../../store/rootReducer";
import {
    getServerMetaRecoverableErrorsByIdSelector
} from "../../../store/front/server/meta/selector";
import LockToken from "./LockToken";
import IconButton from "@mui/material/IconButton";
import EditIcon from '@mui/icons-material/Edit';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';
import {
    fetchCallAffectedByMetadataRequest
} from "../../../store/front/server/meta/actions";
import ErrorRecoveryModal from "./modal/ErrorRecovery";
import SimulationControllerModal from "./modal/SimulationController";
import {
    ProtoSubscribe_ParametersConstraints_Parameters
} from "../../../generated/sila2/org/silastandard/core/commands/parameterconstraintsprovider/v1/ParameterConstraintsProvider";
import {
    ProtoSubscribe_PausedCommands_Parameters
} from "../../../generated/sila2/org/silastandard/core/commands/pausecontroller/v2/PauseController";
import {
    ProtoGet_AuthorizationProvider_Parameters
} from "../../../generated/sila2/org/silastandard/core/authorizationconfigurationservice/v1/AuthorizationConfigurationService";
import {
    ProtoGet_ConfiguredSiLAClients_Parameters
} from "../../../generated/sila2/org/silastandard/core/connectionconfigurationservice/v1/ConnectionConfigurationService";
import {
    ProtoSubscribe_RecoverableErrors_Parameters
} from "../../../generated/sila2/org/silastandard/core/errorrecoveryservice/v1/ErrorRecoveryService";
import {
    ProtoSubscribe_IsLocked_Parameters
} from "../../../generated/sila2/org/silastandard/core/lockcontroller/v2/LockController";
import {
    ProtoCancelAll_Parameters
} from "../../../generated/sila2/org/silastandard/core/commands/cancelcontroller/v1/CancelController";
import {
    ProtoSetServerName_Parameters
} from "../../../generated/sila2/org/silastandard/core/silaservice/v1/SiLAService";
import {findHighestCoreFeatureVersion} from "../../../utils/feature";
import {DateTime} from "luxon";

export interface ServerConfigurationContainerProps {
    server: UscServer;
}

const ServerConfigurationContainer = React.memo((props: ServerConfigurationContainerProps): JSX.Element => {
    const selectedServerFeatures = useSelector(getServerFeatures);
    const recoverableErrorsMeta = useSelector((state: AppState) => getServerMetaRecoverableErrorsByIdSelector(state, props.server.serverUuid));
    const dispatch = useDispatch();
    const [openConnectionConfigurationModal, setOpenConnectionConfigurationModal] = React.useState<boolean>(false);
    const [openErrorRecoveryModal, setOpenErrorRecoveryModal] = React.useState<boolean>(false);
    const [openSimulationControllerModal, setOpenSimulationControllerModal] = React.useState<boolean>(false);
    const parameterConstraintProviderFeature = findHighestCoreFeatureVersion(props.server.features, "ParameterConstraintsProvider", ['1.0']);
    const commandCancelFeature = findHighestCoreFeatureVersion(props.server.features, "CancelController", ['1.0']);
    const lockControllerFeature = findHighestCoreFeatureVersion(props.server.features, "LockController", ['1.0', '2.0']);
    const pauseControllerFeature = findHighestCoreFeatureVersion(props.server.features, "PauseController", ['2.0']);
    const connectionConfigurationFeature = findHighestCoreFeatureVersion(props.server.features, "ConnectionConfigurationService", ['1.0']);
    const errorRecoveryServiceFeature = findHighestCoreFeatureVersion(props.server.features, "ErrorRecoveryService", ['1.0']);
    const simulationControllerFeature = findHighestCoreFeatureVersion(props.server.features, "SimulationController", ['1.0']);
    const authorizationConfigurationServiceFeature = findHighestCoreFeatureVersion(props.server.features, "AuthorizationConfigurationService", ['1.0']);
    const isServerOnline = props.server.status === "ONLINE";

    useEffect(() => {
        dispatch(fetchCallAffectedByMetadataRequest({
            serverUuid: props.server.serverUuid
        }))
    }, [dispatch, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && parameterConstraintProviderFeature) {
            fetch(getBackEndpoint(`/api/servers/${props.server.serverUuid}/features/${parameterConstraintProviderFeature.fullyQualifiedIdentifier}/calls/ParametersConstraints/running`))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_ParametersConstraints_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: parameterConstraintProviderFeature.fullyQualifiedIdentifier,
                                callId: 'ParametersConstraints',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, parameterConstraintProviderFeature, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && pauseControllerFeature) {
            fetch(getBackEndpoint(`/api/servers/${props.server.serverUuid}/features/${pauseControllerFeature.fullyQualifiedIdentifier}/calls/PausedCommands/running`))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_PausedCommands_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: pauseControllerFeature.fullyQualifiedIdentifier,
                                callId: 'PausedCommands',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({}),
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, pauseControllerFeature, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && authorizationConfigurationServiceFeature) {
            fetch(getBackEndpoint(`/api/servers/${props.server.serverUuid}/features/${authorizationConfigurationServiceFeature.fullyQualifiedIdentifier}/calls/AuthorizationProvider/running`))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoGet_AuthorizationProvider_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: authorizationConfigurationServiceFeature.fullyQualifiedIdentifier,
                                callId: 'AuthorizationProvider',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, authorizationConfigurationServiceFeature, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && connectionConfigurationFeature) {
            fetch(getBackEndpoint(`/api/servers/${props.server.serverUuid}/features/${connectionConfigurationFeature.fullyQualifiedIdentifier}/calls/ConfiguredSiLAClients/running`))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoGet_ConfiguredSiLAClients_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: connectionConfigurationFeature.fullyQualifiedIdentifier,
                                callId: 'ConfiguredSiLAClients',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, connectionConfigurationFeature, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && errorRecoveryServiceFeature) {
            fetch(getBackEndpoint(`/api/servers/${props.server.serverUuid}/features/${errorRecoveryServiceFeature.fullyQualifiedIdentifier}/calls/RecoverableErrors/running`))
                .then(response => response.json())
                .then((data: string[]) => {
                    if (data.length) {
                        dispatch(fetchResponseFromCacheRequest({
                            callId: data[0]
                        }))
                    } else {
                        const parameters: ProtoSubscribe_RecoverableErrors_Parameters = {}
                        fetch(getBackEndpoint('/api/calls/execute'), {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                serverId: props.server.serverUuid,
                                fullyQualifiedFeatureId: errorRecoveryServiceFeature.fullyQualifiedIdentifier,
                                callId: 'RecoverableErrors',
                                type: 'OBSERVABLE_PROPERTY',
                                parameters: JSON.stringify(parameters),
                                metadatas: JSON.stringify({})
                            } as SiLACallModel)
                        })
                        // todo handle fail
                    }
                })
                .catch((error) => {
                    // todo handle
                });
        }
    }, [dispatch, errorRecoveryServiceFeature, props.server.features, props.server.status, props.server.serverUuid]);

    useEffect(() => {
        if (props.server.status === "ONLINE" && lockControllerFeature) {
            if (lockControllerFeature.featureVersion === '1.0') {
                // todo fetch periodically ?
                /*fetch(getBackEndpoint('/api/calls/execute'), {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        serverId: props.server.serverUuid,
                        fullyQualifiedFeatureId: lockControllerFeature.fullyQualifiedIdentifier,
                        callId: 'IsLocked',
                        type: 'UNOBSERVABLE_PROPERTY',
                        parameters: JSON.stringify({}),
                        metadatas: JSON.stringify({}),
                        runAsync: true
                    } as SiLACallModel)
                })*/
            } else if (lockControllerFeature.featureVersion === '2.0') {
                fetch(getBackEndpoint(`/api/servers/${props.server.serverUuid}/features/${lockControllerFeature.fullyQualifiedIdentifier}/calls/IsLocked/running`))
                    .then(response => response.json())
                    .then((data: string[]) => {
                        if (data.length) {
                            dispatch(fetchResponseFromCacheRequest({
                                callId: data[0]
                            }))
                        } else {
                            const parameters: ProtoSubscribe_IsLocked_Parameters = {}
                            fetch(getBackEndpoint('/api/calls/execute'), {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    serverId: props.server.serverUuid,
                                    fullyQualifiedFeatureId: lockControllerFeature.fullyQualifiedIdentifier,
                                    callId: 'IsLocked',
                                    type: 'OBSERVABLE_PROPERTY',
                                    parameters: JSON.stringify(parameters),
                                    metadatas: JSON.stringify({})
                                } as SiLACallModel)
                            })
                            // todo handle fail
                        }
                    })
                    .catch((error) => {
                        // todo handle
                    });
            }
        }
    }, [dispatch, lockControllerFeature, props.server.features, props.server.status, props.server.serverUuid]);

    function isSelected(serverUuid: string, featureIdentifier: string): boolean {
        if (selectedServerFeatures && selectedServerFeatures[serverUuid] && selectedServerFeatures[serverUuid][featureIdentifier] !== undefined) {
            return selectedServerFeatures[serverUuid][featureIdentifier];
        }
        return true;
    }

    function setAllFeatureSelection(s: UscServer | undefined, selected: boolean) {
        if (s !== undefined) {
            const serverUuid = s.serverUuid;
            let featureSelections: ServerFeatureSelectedKeyValue = {};
            s.features.forEach((f) => {
                featureSelections = {
                    ...featureSelections,
                    [f.fullyQualifiedIdentifier]: selected
                };
            });
            dispatch(serverFeatureSelections({
                serverUuid: serverUuid,
                featuresSelection: featureSelections
            }));
        }
    }

    const renderServer = (s: UscServer) => {
        const informations = {
            'Status': <Chip size="small" label={s.status} color={s.status === "ONLINE" ? 'success': 'error'} />,
            'Type': <Chip size="small" label={s.serverType}/>,
            'Description': <Chip size="small" label={(s.description) ? s.description : 'No description'}/>,
            ...((s.connectionType === "CLIENT_INITIATED") ? ({'Host': <Chip size="small" label={s.serverHost}/>, 'Port': <Chip size="small" label={s.port}/>}) : ({})),
            'UUID': <Chip size="small" label={s.serverUuid}/>,
            'Connection security': ((s.negotiationType === 'TLS') ?
                <Chip size="small" label="Secured" icon={<HttpsOutlinedIcon color={'success'}/>}/> :
                <Chip size="small" label="Insecure" icon={<WarningAmberOutlinedIcon color={"warning"}/>}/>),
            'Connection type': ((s.connectionType === 'CLIENT_INITIATED') ?
                <Chip size="small" label="Client initiated" icon={<CloudOffOutlinedIcon color={'info'}/>}/> :
                <Chip size="small" label="Server initiated" icon={<CloudOutlinedIcon color={"success"}/>}/>),
            'Added on': <Chip size="small" label={DateTime.fromJSDate(s.joined).toLocaleString()}/>,
            'Vendor URL': <Link underline="hover" href={urlToHREF(s.vendorUrl)} target="_blank"
                                rel="noopener"><Chip size="small" clickable variant={'outlined'} label={s.vendorUrl}/></Link>,
            'Version': <Chip size="small" label={s.version}/>,
            'Certificate': (s.certificateAuthority?.length) ? (<TextField
                variant={'filled'}
                multiline
                maxRows={4}
                onFocus={event => {
                    event.target.select();
                }}
                fullWidth
                InputProps={{
                    readOnly: true,
                }}
                label={'Server Certificate'}
                value={s.certificateAuthority}
            />) : 'None'
        }

        return (
            <Box key={s.serverUuid}>
                <Box sx={{my: 2}} key={'server-name'}>
                    <EditableServerName name={s.name} serverUuid={s.serverUuid} isOnline={s.status === "ONLINE"}/>
                    <Divider/>
                </Box>
                <Box sx={{my: 2}} key={'server-configuration'}>
                    <Typography style={{flex: 1, marginBottom: 7}} variant={"h5"}>Configuration</Typography>
                    <Divider/>
                    <Grid container spacing={1} marginTop={1} marginBottom={4}>
                        {
                            ((props.server.status === "UNKNOWN" && props.server.connectionType !== "SERVER_INITIATED") && (
                                <Grid item xs={12}>
                                    <Alert severity={'warning'} sx={{maxWidth: '600px'}} action={
                                        <Button sx={{minWidth: '100px'}} color={"warning"} variant={'contained'} size="small" onClick={() => {
                                            fetch(getBackEndpoint('/api/servers'), {
                                                method: 'POST',
                                                headers: {
                                                    'Content-Type': 'application/json',
                                                },
                                                body: JSON.stringify({host: props.server.serverHost, port: props.server.port, certificate: props.server.certificateAuthority} as ServerHostPortCertificate)
                                            })
                                        }}>
                                            Retry connection
                                        </Button>
                                    }>
                                        Server is not reachable
                                    </Alert>
                                </Grid>
                            ))
                        }
                        <LockToken server={props.server}/>
                        <AccessToken server={props.server}/>
                        {
                            ((isServerOnline && errorRecoveryServiceFeature && recoverableErrorsMeta?.v1?.RecoverableErrors?.length) && (
                                <Grid item xs={12}>
                                    <Alert severity={'warning'} sx={{maxWidth: '600px'}} action={
                                        <Button sx={{minWidth: '100px'}} color={"warning"} variant={'contained'}
                                                size="small" onClick={() => {
                                            setOpenErrorRecoveryModal(true);
                                        }}>
                                            Recover
                                        </Button>
                                    }>
                                        {recoverableErrorsMeta?.v1?.RecoverableErrors?.length} command execution errors can be
                                        recovered!
                                    </Alert>
                                </Grid>
                            ))
                        }
                        {
                            ((isServerOnline && commandCancelFeature) && (
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={(e) => {
                                        const parameters: ProtoCancelAll_Parameters = {}
                                        // todo handle loading and error
                                        fetch(getBackEndpoint('/api/calls/execute'), {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json',
                                            },
                                            body: JSON.stringify({
                                                serverId: props.server.serverUuid,
                                                fullyQualifiedFeatureId: commandCancelFeature?.fullyQualifiedIdentifier,
                                                callId: 'CancelAll',
                                                type: 'UNOBSERVABLE_COMMAND',
                                                parameters: JSON.stringify(parameters),
                                                metadatas: JSON.stringify({}),
                                                runAsync: false
                                            } as SiLACallModel)
                                        })
                                    }}>
                                        Cancel all commands
                                    </Button>
                                </Grid>
                            ))
                        }
                        {
                            ((isServerOnline && connectionConfigurationFeature) && (
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={(e) => {
                                        setOpenConnectionConfigurationModal(true);
                                    }}>
                                        Configure connection
                                    </Button>
                                </Grid>
                            ))
                        }
                        {
                            ((isServerOnline && simulationControllerFeature) && (
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={(e) => {
                                        setOpenSimulationControllerModal(true);
                                    }}>
                                        Simulation controller
                                    </Button>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Box>
                <Box sx={{my: 2}} key={'server-information'}>
                    <Typography style={{flex: 1, marginBottom: 7}} variant={"h5"}>Information</Typography>
                    <Divider/>
                    <Grid container rowSpacing={{xs: 2, sm: 1}} alignItems={'baseline'} maxWidth={'1000px'}>
                        {
                            Object.entries(informations).map(([k, v]) => (
                                <React.Fragment key={k}>
                                    <Grid item xs={12} sm={4} sx={{'&.MuiGrid-item ': {maxWidth: '200px'}}}>
                                        <Typography variant={"h6"}>{k}</Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={8} paddingLeft={{xs: 0, sm: 1, md: 2, lg: 4}} sx={{'&.MuiGrid-item ': {paddingTop: '0px'}}}>
                                        <Box>{v}</Box>
                                    </Grid>
                                </React.Fragment>
                            ))
                        }
                    </Grid>
                </Box>
            </Box>
        );
    }

    return (
        <>
            <SplitScreen leftName={'Configuration'} rightName={'Feature'} leftComponent={
                props.server !== undefined ? renderServer(props.server) : (
                    <Typography>'No server to display'</Typography>)
            } rightComponent={
                <Box>
                    <Typography variant={"h5"}>Selected features</Typography>
                    <Grid container spacing={1} marginTop={1} marginBottom={4}>
                        <Grid item>
                            <Button variant="contained" color="primary" onClick={() => {
                                setAllFeatureSelection(props.server, true);
                            }}>
                                Select all
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="primary" onClick={() => {
                                setAllFeatureSelection(props.server, false);
                            }}>
                                Select none
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container direction={'column'} spacing={1}>
                        {
                            props.server?.features
                                .sort((a, b) => a.fullyQualifiedIdentifier.localeCompare(b.fullyQualifiedIdentifier))
                                .map((f) => {
                                    return (
                                        <Grid item key={f.fullyQualifiedIdentifier + '/checkbox'}>
                                            <FormControlLabel
                                                sx={styledFormControlLabel}
                                                control={
                                                    <Checkbox
                                                        color="primary"
                                                        inputProps={{'aria-label': 'secondary checkbox'}}
                                                        checked={isSelected(props.server?.serverUuid, f.fullyQualifiedIdentifier)}
                                                        onChange={(e) => {
                                                            dispatch(serverFeatureSelection({
                                                                serverUuid: props.server?.serverUuid,
                                                                featureIdentifier: f.fullyQualifiedIdentifier,
                                                                selected: e.target.checked
                                                            }))
                                                        }}
                                                    />
                                                }
                                                label={
                                                    <Accordion sx={{color: 'text.secondary'}} onClick={(e) => {
                                                        e.preventDefault();
                                                    }}>
                                                        <AccordionSummary expandIcon={<ExpandMoreIcon/>} sx={{
                                                            backgroundColor: "initial",
                                                            color: 'text.secondary',
                                                        }}>
                                                            <Typography>
                                                                {`${f.displayName}`}
                                                            </Typography>
                                                            <Stack direction="row" spacing={1}>
                                                                {
                                                                    (f.fullyQualifiedIdentifier.startsWith('org.silastandard/core') && (
                                                                        <Tooltip title="This feature belongs under the org.silastandard/core">
                                                                            <Chip size="small" label="Core" color="primary" />
                                                                        </Tooltip>
                                                                    ))
                                                                }
                                                                <Chip size="small" label={`v${f.featureVersion}`} />
                                                            </Stack>
                                                        </AccordionSummary>
                                                        <AccordionDetails sx={{color: 'text.secondary'}}>
                                                            <Typography variant={'h6'}>
                                                                Fully Qualified Feature Identifier
                                                            </Typography>
                                                            <Divider/>
                                                            <TextField
                                                                disabled
                                                                fullWidth
                                                                size={'small'}
                                                                defaultValue={f.fullyQualifiedIdentifier}
                                                                variant="outlined"
                                                                InputProps={{
                                                                    sx: {
                                                                        fontSize: '12px',
                                                                    },
                                                                }}
                                                                sx={{marginBottom: 2, marginTop: 2}}
                                                            />
                                                            <Typography variant={'h6'}>
                                                                Description
                                                            </Typography>
                                                            <Divider/>
                                                            <Typography variant={'body2'}>
                                                                {f.description}
                                                            </Typography>
                                                        </AccordionDetails>
                                                    </Accordion>
                                                }
                                            />
                                        </Grid>
                                    );
                                })
                        }
                    </Grid>
                </Box>
            }/>
            <Modal
                key={'connection-configuration-modal'}
                id={'connection-configuration-modal'}
                open={openConnectionConfigurationModal}
                onClose={() => setOpenConnectionConfigurationModal(false)}
            >
                <div>
                    {((connectionConfigurationFeature) && (
                        <ConnectionConfigurationModal
                            handleClose={() => setOpenConnectionConfigurationModal(false)}
                            server={props.server}
                            connectionConfigurationFeature={connectionConfigurationFeature}
                        />
                    ))}
                </div>
            </Modal>
            <Modal
                key={'error-recovery-modal'}
                id={'error-recovery-modal'}
                open={openErrorRecoveryModal}
                onClose={() => setOpenErrorRecoveryModal(false)}
            >
                <div>
                    {((errorRecoveryServiceFeature) && (
                        <ErrorRecoveryModal
                            handleClose={() => setOpenErrorRecoveryModal(false)}
                            server={props.server}
                            recoverableErrors={recoverableErrorsMeta?.v1}
                            errorRecoveryFeature={errorRecoveryServiceFeature}
                        />
                    ))}
                </div>
            </Modal>
            <Modal
                key={'simulation-controller-modal'}
                id={'simulation-controller-modal'}
                open={openSimulationControllerModal}
                onClose={() => setOpenSimulationControllerModal(false)}
            >
                <div>
                    {((simulationControllerFeature) && (
                        <SimulationControllerModal
                            handleClose={() => setOpenSimulationControllerModal(false)}
                            serverUuid={props.server.serverUuid}
                            isCurrentServerOnline={props.server.status === "ONLINE"}
                            simulationFeature={simulationControllerFeature}
                        />
                    ))}
                </div>
            </Modal>
        </>
    );
});

export default ServerConfigurationContainer;

const styledFormControlLabel: SxProps<Theme> = {
    '& .MuiTypography-root': {
        width: '100%',
    },
    width: '100%',
};

export interface EditableServerNameProps {
    serverUuid: string;
    name: string;
    isOnline: boolean;
}

export interface EditableServerNameRequest {
    loading: boolean;
    error?: string;
}

export function EditableServerName(props: EditableServerNameProps) {
    const [isEditing, setIsEditing] = React.useState<boolean>(false);
    const [newName, setNewName] = React.useState<string>(props.name);
    const [request, setRequest] = React.useState<EditableServerNameRequest>({loading: false});

    function changeName() {
        const parameters: ProtoSetServerName_Parameters = {
            ServerName: {
                value: newName
            }
        }
        fetch(getBackEndpoint('/api/calls/execute'), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                serverId: props.serverUuid,
                fullyQualifiedFeatureId: 'org.silastandard/core/SiLAService/v1',
                callId: 'SetServerName',
                type: 'UNOBSERVABLE_COMMAND',
                parameters: JSON.stringify(parameters),
                metadatas: JSON.stringify({}),
                runAsync: false
            } as SiLACallModel)
        }).then(response => response.json()).then((response: UscCall) => {
            if (!response.error) {
                setRequest({
                    loading: false,
                    error: undefined
                })
                setIsEditing(false)
            } else {
                setRequest({
                    loading: false,
                    error: response.result || response.error as any || 'Unknown error'
                })
            }
        }).catch((error) => {
            setRequest({
                loading: false,
                error: error
            })
        })
    }

    if (isEditing) {
        return (
            <>
                {((!request.loading && request.error) && (
                    <Alert severity={"warning"}>{request.error}</Alert>
                ))}
                <TextField
                    fullWidth={false}
                    variant={"filled"}
                    value={newName}
                    label={'Server name'}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <Tooltip {...tooltipProps} title={'Cancel'}>
                                    <IconButton
                                        sx={{margin: 1}}
                                        aria-label="Cancel"
                                        disabled={request.loading}
                                        onClick={() => {
                                            setIsEditing(false)
                                        }}
                                        onMouseDown={(event) => {
                                            event.preventDefault();
                                        }}
                                        edge="end"
                                    >
                                        <ClearIcon/>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip {...tooltipProps} title={'Validate'}>
                                    <IconButton
                                        aria-label="Validate"
                                        disabled={!props.isOnline || request.loading || newName.length < 1 || newName.length > 64}
                                        onClick={() => {
                                            changeName();
                                        }}
                                        onMouseDown={(event) => {
                                            event.preventDefault();
                                        }}
                                        edge="end"
                                    >
                                        <DoneIcon/>
                                    </IconButton>
                                </Tooltip>
                            </InputAdornment>
                        )
                    }}
                    onChange={(e) => setNewName(e.target.value)}
                />
            </>
        )
    }
    return (
        <Box display={'flex'}>
            <Typography variant={"h4"}>{props.name}</Typography>
            <Tooltip {...tooltipProps} title={'Edit server name'}>
                <IconButton
                    aria-label="Edit server name"
                    disabled={!props.isOnline}
                    onClick={() => {
                        setIsEditing(true)
                    }}
                    onMouseDown={(event) => {
                        event.preventDefault();
                    }}
                    edge="end"
                >
                    <EditIcon/>
                </IconButton>
            </Tooltip>
        </Box>
    )
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};