import React, {useEffect, useState} from 'react';
import {
    Box, Paper, Typography, FormGroup, FormControlLabel, Switch
} from '@mui/material/';
import {SiLAJavaFeature} from "../../generated/silaModels";
import FeatureParser from "../../utils/parser/featureParser";
import {store} from "../../components/feature/store/store";
import {Provider} from "react-redux";
import {SiLAJavaFeatureEditor} from "../../components/feature/SiLAJavaFeatureEditor";
import {DefaultFeature} from "../../components/feature/defaultValues";
import {Container} from "@mui/material";
import XMLUploader from "../../components/feature/XMLUploader";


export default function Tools(): JSX.Element {
    const [xml, setXml] = useState<string>('');
    const [feature, setFeature] = useState<SiLAJavaFeature | null>(DefaultFeature);
    const [edit, setEdit] = useState<boolean>(false);

    useEffect(() => {
        const parsedFeature = FeatureParser.parse(xml);
        if (parsedFeature?.featureVersion) {
            setFeature(parsedFeature);
        }
    }, [xml]);

    return (
        <Container maxWidth={'lg'}>
            <Box>
                <Typography variant="h1" component="h1" gutterBottom>
                    Tools
                </Typography>
                <Typography variant="h2" component="h2" gutterBottom>
                    Feature Viewer / Builder (beta)
                </Typography>
                <Paper>
                    <Box margin={2}>
                        <XMLUploader onXML={(xml) => setXml(xml)} />
                        <FormGroup>
                            <FormControlLabel control={
                                <Switch
                                    checked={edit}
                                    onChange={(e) => setEdit(e.target.checked)}
                                    size={'medium'}
                                />
                            } label="Enable Edit" />
                        </FormGroup>
                    </Box>
                    {(feature && (
                        <Provider store={store}>
                            <SiLAJavaFeatureEditor isEditing={edit} feature={feature}/>
                        </Provider>
                    ))}
                </Paper>
            </Box>
        </Container>
    );
}