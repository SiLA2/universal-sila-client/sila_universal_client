import React from 'react';
import './App.css';
import ThemeProviderComponent from "./components/ThemeProviderComponent";
import store from "./store";
import {LocalizationProvider} from "@mui/x-date-pickers";
import { AdapterLuxon } from '@mui/x-date-pickers/AdapterLuxon';
import {Provider} from "react-redux";
import PluginsProviderComponent from "./plugin/PluginsProvider";
import RootRoutes from "./routes/RootRoutes";
import TimezoneHandler from "./components/TimezoneHandler";
import NotificationHandler from './components/NotificationHandler';

export default function App() {
    return (
        <React.StrictMode>
            <Provider store={store}>
                <TimezoneHandler>
                    <LocalizationProvider dateAdapter={AdapterLuxon}>
                        <ThemeProviderComponent>
                            <>
                                <NotificationHandler />
                                <PluginsProviderComponent>
                                    <RootRoutes/>
                                </PluginsProviderComponent>
                            </>
                        </ThemeProviderComponent>
                    </LocalizationProvider>
                </TimezoneHandler>
            </Provider>
        </React.StrictMode>
    );
}