import React from 'react';
import {UscCommand, UscFeature, UscIntermediateResponse} from "../../../../generated/silaModels";
import {
    Accordion,
    AccordionDetails,
    Box, Typography
} from '@mui/material/';
import DataTypeForm from "../dataTypeForm/DataTypeFormRoot";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {getCommandResponseState} from "../dataTypeForm/silaToState";
import {CommandResponse} from "../../../../store/front/server/response/types";
import {styledAccordionSummary} from './CallResponses';
import {styledAccordion} from "../ServerCalls";
import {AccordionSummary, Tooltip, TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface CallResponseIntermediateResponsesProps {
    feature: UscFeature;
    command: UscCommand;
    response: CommandResponse;
}

export default function CallResponseIntermediateResponses(props: CallResponseIntermediateResponsesProps): JSX.Element {
    const fullyQualifiedCommandId = props.response.serverUuid + '/' + props.response.fullyQualifiedFeatureId + '/' + props.response.commandIdentifier;
    let lastIntermediateResponse: UscIntermediateResponse | undefined = props.response?.intermediateResponse.filter(i => i !== null).at(-1);
    return (
        <Box key={props.response.uuid + '/intermediate'}>
            <Accordion sx={styledAccordion}>
                <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                    <Typography>Intermediate</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    {
                        (lastIntermediateResponse) ? (
                            props.command?.intermediateResponses
                                .sort((a, b) => a.displayName.localeCompare(b.displayName))
                                .map(r => {
                                    const formParamValues = getCommandResponseState(props.feature.dataTypeDefinitions, [r], lastIntermediateResponse)
                                    return (
                                        <Box
                                            key={r?.relativeIdentifier + '/' + props.response.uuid + '/' + 0}>
                                            <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                                                <Typography>{r?.relativeIdentifier}</Typography>
                                                <Tooltip
                                                    {...tooltipProps}
                                                    title={r?.description || 'No description'}>
                                                    <IconButton>
                                                        <InfoIcon/>
                                                    </IconButton>
                                                </Tooltip>
                                            </Box>
                                            <DataTypeForm
                                                dataType={r.dataType}
                                                dataTypeDefinitions={props.feature.dataTypeDefinitions}
                                                stateId={r.relativeIdentifier}
                                                parameterId={r.relativeIdentifier}
                                                serverId={props.response.serverUuid}
                                                value={formParamValues}
                                                readonly={true}
                                                fullyQualifiedCommandId={fullyQualifiedCommandId}
                                                type={'PARAMETER'}
                                            />
                                        </Box>
                                    );
                                })
                        ) : (
                            <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                                <Typography>No Intermediate response to display</Typography>
                                <Tooltip
                                    componentsProps={{
                                        tooltip: {
                                            sx: {
                                                fontSize: '14px'
                                            }
                                        }
                                    }}
                                    title={'Intermediate response(s) are not persistent. ' +
                                        'Intermediate responses are lost once the web application is closed. ' +
                                        'Only the responses received while the web application is open are displayed.'}>
                                    <IconButton>
                                        <InfoIcon/>
                                    </IconButton>
                                </Tooltip>
                            </Box>
                        )
                    }
                </AccordionDetails>
            </Accordion>
            <PluginsComponentRenderer componentName={'PluginCallResponseIntermediateResponses'} forwardProps={props}/>
        </Box>
    );
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};