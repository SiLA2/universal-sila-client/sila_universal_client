import React from 'react';
import {UscFeature, UscResponse} from "../../../../generated/silaModels";
import {
    Accordion,
    AccordionDetails,
    Box, Typography
} from '@mui/material/';
import DataTypeForm from "../dataTypeForm/DataTypeFormRoot";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {getCommandResponseState} from "../dataTypeForm/silaToState";
import {CommandResponse} from "../../../../store/front/server/response/types";
import CallResponseError from "./CallResponseError";
import {styledAccordionSummary} from './CallResponses';
import {styledAccordion} from '../ServerCalls';
import {CallType} from "../../../../store/front/server/activeCall/types";
import {AccordionSummary, Tooltip, TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface CallResponseResponsesProps {
    feature: UscFeature;
    response: CommandResponse;
    callResponses: UscResponse[];
    type: CallType;
}

export default function CallResponseResponses(props: CallResponseResponsesProps): JSX.Element {
    const fullyQualifiedCommandId = props.response.serverUuid + '/' + props.response.fullyQualifiedFeatureId + '/' + props.response.commandIdentifier;

    return (
        <Box key={props.response.uuid + '/responses'}>
            {
                ((props.callResponses?.length && props.response.success && props.response.value) ? (
                    <Accordion sx={styledAccordion} defaultExpanded={true}>
                        <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                            <Typography>{props.type === 'command' ? 'Responses' : 'Value'}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {
                                props.callResponses
                                    .sort((a, b) => a.displayName.localeCompare(b.displayName))
                                    .map((r: UscResponse) => {
                                    const formValues = (typeof props.response.value === 'string' && props.response.success && props.response.value) ?
                                        getCommandResponseState(props.feature.dataTypeDefinitions, [r], JSON.parse(props.response.value)) :
                                        (props.response.value);
                                    return (
                                        <Box key={r.relativeIdentifier + '/' + props.response.uuid}>
                                            {
                                                (props.response.success && formValues) ? (
                                                    <Box key={r.relativeIdentifier + '/' + props.response.uuid}>
                                                        <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                                                        <Typography>{r.displayName || r.relativeIdentifier}</Typography>
                                                            <Tooltip
                                                                {...tooltipProps}
                                                                title={r.description || 'No description'}>
                                                                <IconButton>
                                                                    <InfoIcon/>
                                                                </IconButton>
                                                            </Tooltip>
                                                        </Box>
                                                        <DataTypeForm
                                                            dataType={r.dataType}
                                                            dataTypeDefinitions={props.feature.dataTypeDefinitions}
                                                            stateId={r.relativeIdentifier}
                                                            parameterId={r.relativeIdentifier}
                                                            serverId={props.response.serverUuid}
                                                            value={formValues}
                                                            readonly={true}
                                                            fullyQualifiedCommandId={fullyQualifiedCommandId}
                                                            type={'PARAMETER'}
                                                        />
                                                    </Box>
                                                ) : (
                                                    <Typography>{props.response.value}</Typography>
                                                )
                                            }
                                        </Box>
                                    )
                                })
                            }
                        </AccordionDetails>
                    </Accordion>
                ) : ((!props.response.success) && (
                    <CallResponseError responseValue={props.response.value} responseId={props.response.uuid}/>
                )))
            }
            <PluginsComponentRenderer componentName={'PluginCallResponseResponses'} forwardProps={props}/>
        </Box>
    )
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};