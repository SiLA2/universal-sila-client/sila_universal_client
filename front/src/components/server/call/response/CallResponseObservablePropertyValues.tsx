import React from 'react';
import {UscFeature, UscProperty} from "../../../../generated/silaModels";
import {
    Accordion,
    AccordionDetails,
    Box, Typography
} from '@mui/material/';
import DataTypeForm from "../dataTypeForm/DataTypeFormRoot";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {getCommandResponseState} from "../dataTypeForm/silaToState";
import {CommandResponse} from "../../../../store/front/server/response/types";
import {styledAccordionSummary} from './CallResponses';
import {styledAccordion} from "../ServerCalls";
import {AccordionSummary, Tooltip, TooltipProps} from "@mui/material";
import PluginsComponentRenderer from "../../../../plugin/PluginsComponentRenderer";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";

export interface CallResponseObservablePropertyValuesProps {
    feature: UscFeature;
    property: UscProperty;
    response: CommandResponse;
}

export default function CallResponseObservablePropertyValues(props: CallResponseObservablePropertyValuesProps): JSX.Element {
    const fullyQualifiedCommandId = props.response.serverUuid + '/' + props.response.fullyQualifiedFeatureId + '/' + props.response.commandIdentifier;

    return (
        <Box key={props.response.uuid + '/observable-values'}>
            <Accordion sx={styledAccordion} defaultExpanded={true}>
                <AccordionSummary sx={styledAccordionSummary} expandIcon={<ExpandMoreIcon/>}>
                    <Typography>Last Observable Value</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    {
                        (props.response.intermediateResponse?.length) ? (
                            props.response?.intermediateResponse
                                // only display the last value
                                .filter((i, idx) => idx === props.response.intermediateResponse?.length - 1)
                                .sort((a, b) => a.displayName.localeCompare(b.displayName))
                                .map((i, idx) => {
                                    const type = props.property.response;
                                    const formParamValues = getCommandResponseState(props.feature.dataTypeDefinitions, [type], i);
                                    return (
                                        <Box key={type?.relativeIdentifier + '/' + props.response.uuid + '/' + idx}>
                                            <Box sx={{display: 'inline-flex', alignItems: 'center'}}>
                                                <Typography>{type?.relativeIdentifier}</Typography>
                                                <Tooltip
                                                    {...tooltipProps}
                                                    title={type.description || 'No description'}>
                                                    <IconButton>
                                                        <InfoIcon/>
                                                    </IconButton>
                                                </Tooltip>
                                            </Box>
                                            <DataTypeForm
                                                dataType={type.dataType}
                                                dataTypeDefinitions={props.feature.dataTypeDefinitions}
                                                stateId={type?.relativeIdentifier}
                                                parameterId={type?.relativeIdentifier}
                                                serverId={props.response.serverUuid}
                                                value={formParamValues}
                                                readonly={true}
                                                fullyQualifiedCommandId={fullyQualifiedCommandId}
                                                type={'PARAMETER'}
                                            />
                                        </Box>
                                    );
                                })) : (<Typography>No observable values (Observable property values are not
                            persistent)</Typography>)
                    }
                </AccordionDetails>
            </Accordion>
            <PluginsComponentRenderer componentName={'PluginCallResponseObservablePropertyValues'}
                                      forwardProps={props}/>
        </Box>
    );
}

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};