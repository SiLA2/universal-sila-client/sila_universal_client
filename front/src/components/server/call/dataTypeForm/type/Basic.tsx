import React, {FunctionComponent} from "react";
import Typography from "@mui/material/Typography";
import String from "./basic/String";
import Integer from "./basic/Integer";
import Real from "./basic/Real";
import Boolean from "./basic/Boolean";
import DataTypeFormProps from "./dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../generated/silaModels";
import {callInputUpdate} from "../../../../../store/front/server/callInput/actions";
import Date from "./basic/Date";
import Time from "./basic/Time";
import Binary from "./basic/Binary";
import Timestamp from "./basic/Timestamp";
import Any from "./basic/Any";
import {Box} from "@mui/material";
import {ConstraintHelpText} from "./constraints/ConstraintHelpText";
import {
    ProtoAny,
    ProtoBinary,
    ProtoBoolean,
    ProtoDate,
    ProtoInteger,
    ProtoReal,
    ProtoString, ProtoTime,
    ProtoTimestamp
} from "../../../../../generated/SiLAFramework";
import PluginsComponentRenderer from "../../../../../plugin/PluginsComponentRenderer";

export type BasicValueType =
    ProtoString |
    ProtoInteger |
    ProtoReal |
    ProtoBoolean |
    ProtoDate |
    ProtoTime |
    ProtoTimestamp |
    ProtoBinary |
    ProtoAny;

export interface BasicProps extends DataTypeFormProps<SiLAJavaBasicType, BasicValueType> {

}

const Basic = React.memo((props: BasicProps): JSX.Element => {
    const basicType: {[type: string]: FunctionComponent<DataTypeFormProps<SiLAJavaBasicType, any>>} = {
        "STRING": String,
        "INTEGER": Integer,
        "REAL": Real,
        "BOOLEAN": Boolean,
        "DATE": Date,
        "TIME": Time,
        "TIMESTAMP": Timestamp,
        "BINARY": Binary,
        "ANY": Any
    }
    const BasicType = basicType[props.dataType.toUpperCase()];
    if (!BasicType) {
        return (
            <Typography>Unknown type {JSON.stringify(props.dataType)}</Typography>
        );
    }

    function onChange(newValue: BasicValueType) {
        if (props.dispatch) {
            props.dispatch(callInputUpdate({
                fullyQualifiedCommandId: props.fullyQualifiedCommandId,
                stateId: props.stateId,
                updatedValue: newValue,
                formType: props.type,
                serverId: props.serverId
            }));
        }
    }

    return (
        <>
            <BasicType
                {...props}
                onValueChange={(newValue: any) => onChange(newValue)}
            />
            <Box className={"MuiFormHelperText-root"}>
                <ConstraintHelpText constraints={props.constraints} runtimeConstraints={props.runtimeConstraints} d={props.dataType} value={props.value}/>
            </Box>
            <PluginsComponentRenderer componentName={'PluginBasicTypeForm'} forwardProps={props}/>
        </>
    );
});

export default Basic;