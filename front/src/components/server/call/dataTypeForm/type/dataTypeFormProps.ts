import {SiLAJavaConstraints, UscSiLAElement} from "../../../../../generated/silaModels";
import {FormInputType, SiLATypeValueState} from "../../../../../store/front/server/callInput/types";
import {BasicValueType} from "./Basic";
import {Dispatch} from "redux";

export default interface DataTypeFormProps<T, V> {
    dataType: T;
    value: V;
    rootValue: SiLATypeValueState | undefined;
    dataTypeDefinitions: UscSiLAElement[];
    stateId: string;
    depth: number;
    readonly: boolean;
    fullyQualifiedCommandId: string;
    serverId: string;
    parameterId: string;
    onValueChange?: (newValue: BasicValueType) => void;
    dispatch?: Dispatch;
    constraints?: SiLAJavaConstraints;
    runtimeConstraints?: SiLAJavaConstraints[];
    dataTypeIdentifier: string;
    type: FormInputType;
}
