import React, {useState} from "react";
import DataTypeFormProps from "../dataTypeFormProps";
import {BinaryDownload, BinaryUpload, MultipartFile, SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {Alert, Button, TextField} from "@mui/material";
import {ProtoBinary} from "../../../../../../generated/SiLAFramework";
import {getBackEndpoint} from "../../../../../../utils/endpoint";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";
import {DateTime} from "luxon";
import {metadatasStateToGrpcJson} from "../../stateToGrpcJson";
import {useSelector} from "react-redux";
import {AppState} from "../../../../../../store/rootReducer";
import {getCallInputMetadatasByIdSelector} from "../../../../../../store/front/server/callInput/selector";
import {
    getServerFeaturesByIdSelector
} from "../../../../../../store/sila/manager/server/selector";
import {getCallMetadatas} from "../../../../../../utils/call";
import CircularProgressWithLabel from "../../../../../common/CircularWithValueLabel";

export interface BinaryProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoBinary> {

}

export default function Binary(props: BinaryProps): JSX.Element {
    const features = useSelector((state: AppState) => getServerFeaturesByIdSelector(state, props.serverId));
    const callMetadatas = useSelector((state: AppState) => getCallInputMetadatasByIdSelector(state, props.fullyQualifiedCommandId))
    const [completion, setCompletion] = useState<number | undefined>(undefined);
    const [request, setRequest] = useState<XMLHttpRequest | AbortController | undefined>(undefined);
    const [error, setError] = useState<string | undefined>(undefined);
    // todo check what happen if the component is not rendered anymore
    // todo fixme nothing happens when trying to upload the same file two time in a row
    // todo retrieve proper large binary error?

    const MAX_CHUNK_SIZE = 2097152;
    let fileReader: FileReader;

    async function downloadFile() {
        let abortController = new AbortController();
        let downloadProgress = 0; // download completion %

        try {
            let response: Response | undefined;
            if (props.value.value) {
                response = await fetch(`data:image/jpeg;base64,${props.value.value}`, { signal: abortController.signal });
            } else if (props.value.binaryTransferUUID) {
                response = await fetch(getBackEndpoint('/api/calls/binary/download'), {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        serverId: props.serverId,
                        binaryTransferUuid: props.value.binaryTransferUUID
                    } as BinaryDownload),
                    signal: abortController.signal
                });
            }

            if (!response || !response.ok) {
                throw new Error('Network response was not ok');
            }

            setRequest(abortController);
            const reader = response?.body?.getReader();
            const contentLength: number =  +(response?.headers?.get?.('Content-Length') || 'NaN');
            let receivedLength = 0; // received that many bytes at the moment
            let chunks = []; // array of received binary chunks (comprises the body)

            if (!reader) {
                throw new Error('File is not readable');
            }

            if (Number.isNaN(contentLength)) {
                console.warn("length: " + response?.headers?.get?.('Content-Length'))
                throw new Error('Unknown file length');
            }

            while (true) {
                const { done, value } = await reader.read();

                if (done) {
                    break;
                }

                chunks.push(value);
                receivedLength += value.length;

                downloadProgress = (receivedLength / contentLength) * 100; // get the download completion %
                setCompletion(downloadProgress);
            }

            setCompletion(undefined);
            setRequest(undefined)
            setError(undefined);
            let blob = new Blob(chunks);
            // Create blob link to download
            const url = window.URL.createObjectURL(blob);
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', `usc-binary-${DateTime.now().toString()}.bin`);

            document.body.appendChild(link);

            link.click();
            link.parentNode?.removeChild(link);
        } catch (error) {
            setCompletion(undefined);
            setRequest(undefined)
            setError('Failed to download! ' + error);
        }
    }

    function handleFileRead() {
        if (typeof fileReader.result === 'string') {
            const content = btoa(unescape(encodeURIComponent(fileReader.result)));
            console.log('encoded base64 binary length ' + content.length);
            if (props.onValueChange) {
                props.onValueChange({value: content});
            }
        } else {
            // todo handle arraybuffer
        }
        fileReader = new FileReader();
    }

    function handleFileChosen(files: FileList | null) {
        if (!files || !files.length) {
            return;
        }
        if (files[0].size > MAX_CHUNK_SIZE) {
            const fullyQualifiedCommandID = props.fullyQualifiedCommandId.replace(props.serverId + '/', '');
            const metadatas = metadatasStateToGrpcJson(getCallMetadatas(features || []), callMetadatas || {});

            const binaryUpload: BinaryUpload = {
                file: files[0] as unknown as MultipartFile,
                serverId: props.serverId,
                metadatas: JSON.stringify(metadatas),
                parameterIdentifier: fullyQualifiedCommandID + '/Parameter/' + props.parameterId
            }

            const formData = new FormData();
            for (const [key, value] of Object.entries(binaryUpload)) {
                formData.append(key, value);
            }
            const xhr = new XMLHttpRequest();

            xhr.upload.addEventListener('progress', (event) => {
                if (event.lengthComputable) {
                    const percentCompleted = Math.round((event.loaded * 100) / event.total);
                    setCompletion(percentCompleted);
                }
            });
            xhr.onreadystatechange = function() {
                try {
                    if (xhr.status !== 0 && xhr.status !== 200) {
                        throw new Error('Invalid server response ' + xhr.status);
                    }
                    if (xhr.readyState !== 4) {
                        return; // upload still in progress
                    }
                    const data = xhr.responseText;
                    if (!data) {
                        throw new Error('Received invalid uuid');
                    }
                    if (props.onValueChange) {
                        props.onValueChange({
                            value: undefined,
                            binaryTransferUUID: data
                        });
                        setCompletion(undefined);
                        setRequest(undefined)
                        setError(undefined);
                    }
                } catch (e) {
                    setCompletion(undefined);
                    setRequest(undefined)
                    setError('Upload failed! ' + e);
                }
            };
            xhr.onerror = function (e) {
                setCompletion(undefined);
                setRequest(undefined)
                setError('Upload failed!');
            };
            xhr.open('POST', getBackEndpoint('/api/calls/binary/upload'));
            setRequest(xhr);
            xhr.send(formData);

            return;
        }
        fileReader = new FileReader();
        fileReader.onloadend = handleFileRead;
        fileReader.readAsText(files[0]);
    }

    return (
        <>
            {
                ((!request) && (
                        (!props.readonly) ? (
                        <Button
                            variant="contained"
                            component="label"
                            sx={{marginBottom: '8px'}}
                        >
                            Upload File
                            <input
                                type="file"
                                onChange={e => {
                                    handleFileChosen(e.target.files);
                                    // reset file to allow onChange to be triggered even if uploading the same file two time
                                    e.target.value = '';
                                }}
                                hidden
                            />
                        </Button>
                    ) : (
                        <Button
                            variant="contained"
                            component="label"
                            sx={{marginBottom: '8px'}}
                            onClick={downloadFile}
                        >
                            Download File
                        </Button>
                    )
                ))
            }
            {
                (error !== undefined ? (
                    <Alert sx={{my: 1}} severity="error" onClose={() => setError(undefined)}>{error}</Alert>
                ) : null)
            }
            {
                ((request !== undefined) && (
                    <Button
                        variant="contained"
                        color={"warning"}
                        component="label"
                        sx={{marginBottom: '8px'}}
                        onClick={() => {request.abort()}}
                    >
                        Abort {(request as XMLHttpRequest)?.status !== undefined ? 'Upload' : 'Download'}
                    </Button>
                ))
            }
            {
                ((completion !== undefined) && (
                    <CircularProgressWithLabel value={completion} />
                ))
            }
            {
                (props.value.value) ? (
                    <TextField
                        label="Base64 Binary"
                        multiline
                        rows={4}
                        fullWidth={true}
                        value={props.value.value}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            disabled: props.readonly,
                        }}
                />) : (
                    <TextField
                        label="Binary transfer UUID"
                        fullWidth={true}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            disabled: props.readonly,
                        }}
                        value={props.value.binaryTransferUUID}
                    />
                )
            }
            <PluginsComponentRenderer componentName={'PluginBinaryTypeForm'} forwardProps={props}/>
        </>
    );
};