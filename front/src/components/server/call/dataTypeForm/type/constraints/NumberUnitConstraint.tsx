import DataTypeFormProps from "../dataTypeFormProps";
import {SiLAJavaBasicType} from "../../../../../../generated/silaModels";
import {Box, Button} from "@mui/material";
import React from "react";
import {ProtoInteger, ProtoReal} from "../../../../../../generated/SiLAFramework";
import PluginsComponentRenderer from "../../../../../../plugin/PluginsComponentRenderer";

export interface NumberUnitConstraintProps extends DataTypeFormProps<SiLAJavaBasicType, ProtoInteger | ProtoReal> {
    acceptDecimal: boolean;
}

export default function NumberUnitConstraint(props: NumberUnitConstraintProps): JSX.Element {
    const unit = props.constraints?.unit;
    let displayConversion = false;
    let forwardConversionLabel = '';
    let backwardConversionLabel = '';

    if (unit) {
        const originalValue = props.value?.value || 0;
        const convertedValueForward = ((originalValue * unit.factor) + unit.offset);
        const convertedValueBackward = ((originalValue - unit.offset) / unit.factor);
        // Determine if we need rounding
        const roundedForward = props.acceptDecimal ? convertedValueForward : Math.round(convertedValueForward);
        const roundedBackward = props.acceptDecimal ? convertedValueBackward : Math.round(convertedValueBackward);

        // Generate labels including the converted values
        forwardConversionLabel = `${unit.label} (${originalValue}) → ${unit.unitComponent[0].siunit} (${roundedForward})`;
        backwardConversionLabel = `${unit.unitComponent[0].siunit} (${originalValue}) → ${unit.label} (${roundedBackward})`;

        // Check if conversion is meaningful (including label comparison to account for unit name changes)
        displayConversion = originalValue !== roundedForward && unit.label !== unit.unitComponent[0].siunit;
    }

    return (
        <>
            {displayConversion && (
                <>
                    <Box style={{marginTop: '0.5rem', gap: '6px'}} display={'flex'} alignItems={'center'}>
                        <div style={{width: '50%'}}>{forwardConversionLabel}</div>
                        <div style={{width: '50%'}}>{backwardConversionLabel}</div>
                    </Box>
                    <Box style={{marginTop: '0.5rem'}} display={'flex'} alignItems={'center'} flexWrap={'wrap'}>
                        <Button style={{marginRight: '0.5rem', marginBottom: '0.5rem'}} variant="contained" color="inherit"
                                onClick={() => {
                                    if (unit && props.onValueChange) {
                                        const convertedValue = ((props.value?.value || 0) * unit.factor) + unit.offset;
                                        props.onValueChange({value: (props.acceptDecimal) ? convertedValue : Math.round(convertedValue)});
                                    }
                                }}>
                            {props.constraints?.unit.label + ' → ' + props.constraints?.unit.unitComponent[0].siunit}
                        </Button>
                        <Button style={{marginRight: '0.5rem', marginBottom: '0.5rem'}} variant="contained" color="inherit"
                                onClick={() => {
                                    if (unit && props.onValueChange) {
                                        const convertedValue = ((props.value?.value || 0) - unit.offset) / unit.factor;
                                        props.onValueChange({value: (props.acceptDecimal) ? convertedValue : Math.round(convertedValue)});
                                    }
                                }}>
                            {props.constraints?.unit.unitComponent[0].siunit + ' → ' + props.constraints?.unit.label}
                        </Button>
                    </Box>
                </>
            )}
            <PluginsComponentRenderer componentName={'PluginNumberUnitConstraint'} forwardProps={props}/>
        </>
    );
}