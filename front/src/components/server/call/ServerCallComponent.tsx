import {UscCommand, UscFeature, UscProperty, UscServer} from "../../../generated/silaModels";
import {useDispatch} from "react-redux";
import {Box} from "@mui/material";
import {serverActiveCall} from "../../../store/front/server/activeCall/actions";
import ServerCallRequest from "./request/ServerCallRequest";
import React from "react";
import {FullyQualifiedCallId, ServerActiveCalls} from "../../../store/front/server/activeCall/types";
import PluginsComponentRenderer from "../../../plugin/PluginsComponentRenderer";

function callMatch(call: FullyQualifiedCallId | undefined, s: UscServer, f: UscFeature, c: UscProperty | UscCommand): boolean {
    if (!call) {
        return false;
    }

    return (
        call.serverUuid === s.serverUuid &&
        call.fullyQualifiedFeatureId === f.fullyQualifiedIdentifier &&
        (call.callType === 'property' || (c as UscCommand).parameters?.length !== undefined) &&
        call.callId === c.relativeIdentifier
    )
}

function uscToKey(s: UscServer, f: UscFeature, c: UscProperty | UscCommand): string {
    return (
        s.serverUuid + '/' +
        f.fullyQualifiedIdentifier + '/' +
        (((c as UscCommand).parameters?.length !== undefined) ? ('command') : ('property')) + '/' +
        c.relativeIdentifier
    );
}

export interface ServerCallComponentProps {
    server: UscServer;
    type: "property" | "command";
    feature: UscFeature;
    call: UscProperty | UscCommand;
    activeCalls: ServerActiveCalls;
}

export default function ServerCallComponent(props: ServerCallComponentProps): JSX.Element {
    const dispatch = useDispatch();

    return (
        <Box onClick={() => {
            const activeCall = props.activeCalls[props.server.serverUuid];
            if (activeCall && activeCall.fullyQualifiedFeatureId === props.feature.fullyQualifiedIdentifier &&
                activeCall.callType === props.type &&
                activeCall.callId === props.call.relativeIdentifier
            ) {
                return;
            }
            dispatch(serverActiveCall({
                serverUuid: props.server.serverUuid,
                fullyQualifiedFeatureId: props.feature.fullyQualifiedIdentifier,
                callType: props.type,
                callId: props.call.relativeIdentifier
            }));
        }}>
            <ServerCallRequest
                key={uscToKey(props.server, props.feature, props.call)}
                server={props.server}
                feature={props.feature}
                call={props.call}
                active={callMatch(props.activeCalls[props.server.serverUuid], props.server, props.feature, props.call)}
            />
            <PluginsComponentRenderer componentName={'PluginServerCallComponent'} forwardProps={props}/>
        </Box>
    );
}