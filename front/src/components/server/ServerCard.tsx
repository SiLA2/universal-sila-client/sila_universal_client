import React from 'react';
import {
    Card, CardContent, Grid, Tooltip, Typography
} from '@mui/material/';
import IconButton from "@mui/material/IconButton";
import ThrashIcon from '@mui/icons-material/Delete';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import RefreshIcon from '@mui/icons-material/Refresh';
import {useDispatch, useSelector} from "react-redux";
import {serverBookmarkAddRequest, serverBookmarkDeleteRequest} from "../../store/front/server/bookmark/actions";
import {ServerBookmarkKeyValueState} from "../../store/front/server/bookmark/types";
import {getServerBookmarks} from "../../store/front/server/bookmark/selector";
import {getServerByIdSelector} from "../../store/sila/manager/server/selector";
import {ServersTabs} from "../../store/front/server/open/types";
import {getServerOpenTabs} from "../../store/front/server/open/selector";
import ServerLogoBadge from "../common/ServerLogoBadge";
import {getServerColor, getServerInitial} from "../../utils/server";
import {AppState} from "../../store/rootReducer";
import {getBackEndpoint} from "../../utils/endpoint";
import {SxProps} from "@mui/system";
import {Theme} from "@mui/material/styles";
import PluginsComponentRenderer from "../../plugin/PluginsComponentRenderer";
import {Chip, Stack, TooltipProps} from "@mui/material";
import {toast} from "react-toastify";
import {ServerHostPortCertificate} from "../../generated/silaModels";
import {HelpCircle} from "react-feather";

export interface ServerCardProps {
    serverId: string;
    onClick?: () => void;
}

export default function ServerCard(props: ServerCardProps): JSX.Element {
    const dispatch = useDispatch();
    const serverBookmark: ServerBookmarkKeyValueState = useSelector(getServerBookmarks);
    const serversTabs: ServersTabs = useSelector(getServerOpenTabs);
    const server = useSelector((state: AppState) => getServerByIdSelector(state, props.serverId));
    const bookmarked = serverBookmark[props.serverId];
    const serverInitials = getServerInitial(server?.name || 'NA');

    return (
        <Card sx={styledCard} className={((serversTabs[props.serverId]) ? ' active' : '')}>
            <CardContent onClick={(e) => {
                if (props.onClick) {
                    props.onClick();
                }
            }}>
                <Grid container direction="row" alignItems="center">
                    <Grid item>
                        <ServerLogoBadge status={server.status} serverInitials={serverInitials} serverColor={getServerColor(server.serverUuid)}/>
                    </Grid>
                    <Grid item xs style={{maxWidth: '100%', overflow: 'hidden'}}>
                        <Tooltip {...tooltipProps} title={server.name}>
                            <Typography
                                sx={{
                                    whiteSpace: 'nowrap',
                                    textOverflow: 'ellipsis',
                                    overflow: 'hidden',
                                    marginLeft: '8px',
                                }} color="textSecondary" gutterBottom>
                                {server.name}
                            </Typography>
                        </Tooltip>
                    </Grid>
                    <Grid item sx={{marginLeft: 'auto', display: 'inline-flex', alignItems: 'end'}}>
                        <Tooltip {...tooltipProps} title={server.description || 'This server does not have a description'}>
                            <IconButton disableFocusRipple style={{paddingTop: 0}} onClick={(e) => {
                                e.stopPropagation();
                            }}>
                                <HelpCircle/>
                            </IconButton>
                        </Tooltip>
                        {
                            ((server.status === "UNKNOWN" && server.connectionType !== "SERVER_INITIATED") && (
                                <Tooltip {...tooltipProps} title={'Retry connection'}>
                                    <IconButton style={{paddingTop: 0}} onClick={(e) => {
                                        e.stopPropagation();
                                        toast.promise(
                                            fetch(getBackEndpoint('/api/servers'), {
                                                method: 'POST',
                                                headers: {
                                                    'Content-Type': 'application/json',
                                                },
                                                body: JSON.stringify({host: server.serverHost, port: server.port, certificate: server.certificateAuthority} as ServerHostPortCertificate)
                                            }).then((r) => {
                                                if (!r.ok) {
                                                    throw new Error('Server response error')
                                                }
                                            }),
                                            {
                                                pending: 'Connecting to server...',
                                                success: 'Successfully reconnected to server',
                                                error: 'Failed to reconnect to server'
                                            },
                                            {position: 'bottom-center', theme: 'dark'}
                                        )
                                    }}>
                                        <RefreshIcon color={"warning"}/>
                                    </IconButton>
                                </Tooltip>
                            ))
                        }
                        <IconButton style={{paddingTop: 0}} onClick={(e) => {
                            e.stopPropagation();
                            if (bookmarked) {
                                dispatch(serverBookmarkDeleteRequest({serverUuid: server.serverUuid}));
                            } else {
                                dispatch(serverBookmarkAddRequest({serverUuid: server.serverUuid}));
                            }
                        }}>
                            {
                                (bookmarked) ? (<BookmarkIcon/>) : (<BookmarkBorderIcon/>)
                            }
                        </IconButton>
                        <IconButton style={{paddingTop: 0}} onClick={(e) => {
                            e.stopPropagation();
                            fetch(getBackEndpoint('/api/servers/' + server.serverUuid), {
                                method: 'DELETE'
                            })
                                .catch((error) => {
                                    console.error(error);
                                    //setAlertMessage(JSON.stringify(error));
                                    //setAlertSeverity('error');
                                    //setCloseAlert(false);
                                })
                        }}>
                            <ThrashIcon/>
                        </IconButton>
                    </Grid>
                </Grid>
                <Stack py={1} direction="row" marginLeft={'auto'} spacing={1}>
                    <Chip size="small" label={`${server.serverType}`} />
                    <Chip size="small" label={`v${server.version}`} />
                </Stack>
                <PluginsComponentRenderer componentName={'PluginServerCardBody'} forwardProps={{
                    ...props,
                    bookmarked: bookmarked,
                    serverInitials: serverInitials
                }}/>
            </CardContent>
        </Card>
    );
}

const styledCard: SxProps<Theme> = (theme) => ({
    height: '100%',
    cursor: 'pointer',
    border: '3px solid',
    '&:hover': {
        borderColor: theme.palette.primary.light,
    },
    '&.active': {
        borderColor: theme.palette.primary.main,
    },
    borderColor: 'transparent',
});

const tooltipProps: Partial<TooltipProps> = {
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};