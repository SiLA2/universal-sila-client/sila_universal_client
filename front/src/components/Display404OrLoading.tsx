import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import {getAppPluginsLoaded} from "../store/front/app/plugin/selector";
import {Button, CircularProgress, Typography} from "@mui/material";
import React from "react";

export function Display404OrLoading() {
    const navigate = useNavigate();
    const pluginsLoaded = useSelector(getAppPluginsLoaded);
    if (!pluginsLoaded) {
        return (
            <div>
                <Typography variant="h2">Loading</Typography>
                <CircularProgress></CircularProgress>
            </div>
        );
    }
    return (
        <div>
            <Typography variant="h2">This page does not exist</Typography>
            <Button variant="contained" color="primary" onClick={() => {
                navigate('/')
            }}>
                Go back to dashboard
            </Button>
        </div>
    );
}