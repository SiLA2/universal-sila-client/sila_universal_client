import Git from "../generated/git.json";
import {Button, Grid, TextField, Typography} from "@mui/material";
import {removePersistedUscState} from "../store";
import Package from "../../package.json";
import React, {useEffect, useState} from "react";
import {getBackEndpoint} from "../utils/endpoint";
import {UscVersion} from "../generated/silaModels";
import {useNavigate} from "react-router-dom";

export interface ReportErrorProps {
    errorName?: string;
    errorMessage?: string;
    errorStack?: string;
}

export default function ReportError(props: ReportErrorProps) {
    const navigate = useNavigate();
    const [backVersion, setBackVersion] = useState<UscVersion | undefined>();

    useEffect(() => {
        window?.fetch?.(getBackEndpoint('/api/version'))
            .then(response => response.json())
            .then((version) => {
                setBackVersion(version);
            })
            .catch((error) => {
                setBackVersion(undefined);
            })
    }, []);

    const errorLocation = window.location.hash;
    const params: { [param: string]: string } = {
        'issue[title]': `[Front report][${props.errorName || 'unknown'}] ${props.errorMessage || 'unknown'}`.substring(0, 128),
        'issue[description]':
            `Error location \n` +
            `\`\`\`\n` +
            `${errorLocation}\n` +
            `\`\`\`\n` +
            `\n` +
            `Error name \n` +
            `\`\`\`\n` +
            `${props.errorName || 'unknown'}\n` +
            `\`\`\`\n` +
            `\n` +
            `Error message \n` +
            `\`\`\`\n` +
            `${props.errorMessage || 'unknown'}\n` +
            `\`\`\`\n` +
            `\n` +
            `Error stack \n` +
            `\`\`\`\n` +
            `${props.errorStack || 'unknown'}\n` +
            `\`\`\`\n` +
            `\n` +
            `Front version \n` +
            `\`\`\`\n` +
            `${JSON.stringify(Git)}\n` +
            `\`\`\`\n` +
            `\n` +
            `Back version \n` +
            `\`\`\`\n` +
            `${JSON.stringify(backVersion || 'unknown')}\n\`` +
            `\`\`\`\n` +
            `\n`
    };
    const encodedParams = Object.entries(params).map(kv => kv.map(encodeURIComponent).join("=")).join("&");
    return (
        <Grid container spacing={1} paddingX={4} rowSpacing={2} marginTop={1} maxWidth={'1024px'} color={"#97999e"}>
            <Grid item xs={12}>
                <Typography variant={'h1'}>Oops!</Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography variant={'h2'}>There's an error</Typography>
            </Grid>
            <Grid item xs={12}>
                <Button variant="contained" color="primary" onClick={() => {
                    window.open('https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/issues/new?' + encodedParams)
                    //window.open('https://gitlab.com/SiLA2/universal-sila-client/sila_universal_client/-/issues/new?issue[title]=Validate%20new%20concept&issue[description]=Research%20idea')
                }}>
                    Report error
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Button variant="contained" color="primary" onClick={() => {
                    window.location.reload();
                }}>
                    Reload page
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Button variant="contained" color="primary" onClick={() => {
                    navigate('/')
                }}>
                    Go back to dashboard
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Typography>You can try to clear the local cache in case the error always occurs</Typography>
                <Button variant="contained" color="primary" onClick={() => {
                    removePersistedUscState();
                    window.location.reload();
                }}>
                    Clear local cache & Reload page
                </Button>
            </Grid>
            <Grid item xs={12}>
                <TextField
                    label="Error location"
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={errorLocation}
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    label="Error name"
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={props.errorName || 'unknown'}
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    label="Error message"
                    multiline
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={props.errorMessage || 'unknown'}
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    label="Error stack"
                    multiline
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={props.errorStack || 'unknown'}
                />
            </Grid>
            <Grid item xs={12} md={1.25}>
                <TextField
                    label="Front version"
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={Package.version}
                />
            </Grid>
            <Grid item xs={12} md={3}>
                <TextField
                    label="Front Git branch"
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={Git["git.branch"]}
                />
            </Grid>
            <Grid item xs={12} md={5.5}>
                <TextField
                    label="Front Git commit"
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={Git["git.commit.id"]}
                />
            </Grid>
            <Grid item xs={12} md={1.25}>
                <TextField
                    label="Front Git closest tag"
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={Git["git.closest.tag.name"]}
                />
            </Grid>
            <Grid item xs={12} md={1}>
                <TextField
                    label="Front Git dirty"
                    fullWidth
                    color="primary"
                    focused
                    InputProps={{
                        sx: {color: 'inherit'},
                        readOnly: true
                    }}
                    value={Git["git.dirty"]}
                />
            </Grid>
            {
                (backVersion) ? (
                    <>
                        <Grid item xs={12} md={1.25}>
                            <TextField
                                label="Back version"
                                fullWidth
                                color="primary"
                                focused
                                InputProps={{
                                    sx: {color: 'inherit'},
                                    readOnly: true
                                }}
                                value={backVersion.buildVersion}
                            />
                        </Grid>
                        <Grid item xs={12} md={3}>
                            <TextField
                                label="Back Git branch"
                                fullWidth
                                color="primary"
                                focused
                                InputProps={{
                                    sx: {color: 'inherit'},
                                    readOnly: true
                                }}
                                value={backVersion.branch}
                            />
                        </Grid>
                        <Grid item xs={12} md={5.5}>
                            <TextField
                                label="Back Git commit"
                                fullWidth
                                color="primary"
                                focused
                                InputProps={{
                                    sx: {color: 'inherit'},
                                    readOnly: true
                                }}
                                value={backVersion.commitId}
                            />
                        </Grid>
                        <Grid item xs={12} md={1.25}>
                            <TextField
                                label="Back Git closest tag"
                                fullWidth
                                color="primary"
                                focused
                                InputProps={{
                                    sx: {color: 'inherit'},
                                    readOnly: true
                                }}
                                value={backVersion.closestTag}
                            />
                        </Grid>
                        <Grid item xs={12} md={1}>
                            <TextField
                                label="Back Git dirty"
                                fullWidth
                                color="primary"
                                focused
                                InputProps={{
                                    sx: {color: 'inherit'},
                                    readOnly: true
                                }}
                                value={backVersion.dirty}
                            />
                        </Grid>
                    </>
                ) : (
                    <Grid item xs={12}>
                        <TextField
                            label="Unknown back information"
                            fullWidth
                            color="primary"
                            focused
                            InputProps={{
                                sx: {color: 'inherit'},
                                readOnly: true
                            }}
                            value={"Unknown back information"}
                        />
                    </Grid>
                )
            }
        </Grid>
    );
}