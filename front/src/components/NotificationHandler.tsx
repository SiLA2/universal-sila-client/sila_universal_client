import React, { useEffect, useState } from 'react';
import { Snackbar, Alert, Container, Accordion, AccordionSummary, AccordionDetails, Typography } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import NotificationService from '../services/NotificationService';

// todo support stacking of alerts?

interface NotificationState {
    open: boolean;
    severity: 'error' | 'warning' | 'info' | 'success';
    title: string;
    serverInfo?: {
        host: string;
        port: number;
        certificate: string | null;
    };
}

export default function NotificationHandler() {
    const [notification, setNotification] = useState<NotificationState>({
        open: false,
        severity: 'info',
        title: '',
    });

    useEffect(() => {
        const listener = (newNotification: { type: 'error' | 'warning' | 'info' | 'success', message: string, serverInfo?: { host: string; port: number; certificate: string | null; } }) => {
            setNotification({
                open: true,
                severity: newNotification.type,
                title: newNotification.message,
                serverInfo: newNotification.serverInfo,
            });
        };
        NotificationService.addListener(listener);
        return () => NotificationService.removeListener(listener);
    }, []);

    const handleClose = () => {
        setNotification(prev => ({ ...prev, open: false }));
    };

    const renderServerInfo = () => {
        if (!notification.serverInfo) return null;

        const { host, port, certificate } = notification.serverInfo;
        const certificateStatus = certificate ? "Valid certificate" : "Missing or invalid certificate";

        return (
            <Container sx={{marginTop: '1rem', padding: 0}}>
                <Typography>Server Information:</Typography>
                <Typography>Host: {host}</Typography>
                <Typography>Port: {port}</Typography>
                <Typography>Certificate: {certificateStatus}</Typography>
            </Container>
        );
    };

    return (
        <Snackbar
            open={notification.open}
            autoHideDuration={12000}
            onClose={handleClose}
        >
            <Container>
                <Alert
                    onClose={handleClose}
                    severity={notification.severity}
                    sx={{
                        maxWidth: 'sm',
                        '@media (max-width: 700px)': {
                            '& > *': {
                                paddingRight: '0px',
                                paddingLeft: '0px'
                            }
                        }
                    }}
                >
                    <Container sx={{padding: 0}}>
                        {notification.title}
                    </Container>
                    {notification.serverInfo && (
                        <Container sx={{marginTop: '1rem', padding: 0}}>
                            <Accordion sx={{
                                color: 'text.secondary',
                                backgroundColor: "transparent",
                            }} onClick={(e) => e.preventDefault()}>
                                <AccordionSummary expandIcon={<ExpandMoreIcon/>} sx={{
                                    backgroundColor: "initial",
                                    color: 'text.secondary',
                                }}>
                                    <Typography fontSize={'inherit'}>Server Details</Typography>
                                </AccordionSummary>
                                <AccordionDetails sx={{color: 'text.secondary', overflow: 'scroll', maxHeight: '300px', whiteSpace: 'pre-line'}}>
                                    {renderServerInfo()}
                                </AccordionDetails>
                            </Accordion>
                        </Container>
                    )}
                </Alert>
            </Container>
        </Snackbar>
    );
}