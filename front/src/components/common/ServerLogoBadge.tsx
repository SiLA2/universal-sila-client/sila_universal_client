import {UscServerStatus} from "../../generated/silaModels";
import Avatar from "@mui/material/Avatar";
import React from "react";
import {Theme} from "@mui/material/styles";
import Badge from "@mui/material/Badge";
import {SxProps} from "@mui/system";

export interface ServerLogoBadgeProps {
    status: UscServerStatus;
    serverInitials: string;
    serverColor: string;
    sx?: SxProps<Theme>;
}

const ServerLogoBadge = React.memo((props: ServerLogoBadgeProps): JSX.Element => {
    const s = styledBadge(props.status);
    return (
        <Badge
            sx={[
                (props.sx || {}),
                ...(Array.isArray(s) ? s : [s]),
            ]}
            overlap="circular"
            anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
            variant="dot"
        >
            <Avatar sx={(theme: any) => ({color: 'white', backgroundColor: props.serverColor})}>{props.serverInitials}</Avatar>
        </Badge>
    );
});

export default ServerLogoBadge;

export const styledBadge: (status: UscServerStatus) => SxProps<Theme> = (status) => ((theme) => ({
    '& .MuiBadge-badge': {
        minWidth: '10px',
        minHeight: '10px',
        backgroundColor: (status === "ONLINE") ? '#3cc453' : ((status === "OFFLINE") ? '#c43c3c' : '#5b5b5b'),
        color: (status === "ONLINE") ? '#3cc453' : ((status === "OFFLINE") ? '#c43c3c' : '#5b5b5b'),
        //boxShadow: (online) ? (`0 0 0 1px ${theme.palette.background.paper}`) : null,
        boxShadow: (status === "ONLINE") ? (`0 0 0 1px #676767`) : null,
        '&::after': {
            position: 'absolute',
            zIndex: 10,
            top: '0px',
            left: '0px',
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: 'ripple 1.2s infinite ease-in-out',
            content: '""',
            border: (status === "ONLINE") ? ('1px solid currentColor') : null,
        },
    },
    ...(status === "ONLINE") ? ({
        '@keyframes ripple': {
            '0%': {
                transform: 'scale(.8)',
                opacity: 1,
            },
            '100%': {
                transform: 'scale(2.4)',
                opacity: 0,
            },
        }
    }) : ({}),
}));