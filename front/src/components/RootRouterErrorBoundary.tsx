import {useRouteError} from "react-router-dom";
import ReportError from "./ReportError";

export interface RootRouterErrorBoundaryProps {

}

export default function RootRouterErrorBoundary(props: RootRouterErrorBoundaryProps) {
    const error = useRouteError() as Error;
    return (
        <ReportError
            errorName={error.name}
            errorMessage={error.message || JSON.stringify(error)}
            errorStack={error.stack}
        />
    )
}
