import React, {Component, ErrorInfo, ReactNode} from "react";
import {UscVersion} from "../generated/silaModels";
import {getBackEndpoint} from "../utils/endpoint";
import ReportError from "./ReportError";

interface Props {
    children: ReactNode;
}

interface State {
    hasError: boolean;
    error?: Error;
    errorInfo?: ErrorInfo;
    backVersion?: UscVersion | null;
}

class RootErrorBoundary extends Component<Props, State> {

    public state: State = {
        hasError: false
    };

    componentDidMount() {
        window?.fetch?.(getBackEndpoint('/api/version'))
            .then(response => response.json())
            .then((version) => {
                this.setState((prev) => ({...prev, backVersion: version}));
            })
            .catch((error) => {
            })
    }

    public static getDerivedStateFromError(error: Error): State {
        // Update state so the next render will show the fallback UI.
        return {hasError: true, error};
    }

    public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.error("Uncaught error:", error, errorInfo);
        //return {hasError: true, error, errorInfo};
    }

    public render() {
        if (this.state.hasError) {
            return (
                <ReportError
                    errorMessage={this.state.error?.message || JSON.stringify(this.state.error?.message)}
                    errorName={this.state.error?.name}
                    errorStack={this.state.error?.stack}
                />
            );
        }
        return this.props.children;
    }
}

export default RootErrorBoundary;
