import React from 'react';
import {
    Accordion, AccordionDetails, AccordionSummary,
    Box, CardContent, Typography
} from '@mui/material/';
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {PluginModel, PluginRemove} from "../generated/silaModels";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import {getBackEndpoint} from "../utils/endpoint";
import {Card, Tooltip, TooltipProps} from "@mui/material";

export interface PluginProps {
    plugin: PluginModel;
}

export default function Plugin(props: PluginProps): JSX.Element {

    return (
        <Card key={props.plugin.name} sx={{ maxWidth: '500px' }}>
            <CardContent>
                <Box display={"inline-flex"} width={'100%'} justifyContent={"space-between"} alignItems={"center"}>
                    <Typography variant={"h5"}>{props.plugin.name} v{props.plugin.version}</Typography>
                    <Tooltip {...tooltipProps} title={'Remove plugin'}>
                        <IconButton onClick={() => {
                            fetch(getBackEndpoint('/api/plugin/remove'), {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    name: props.plugin.name,
                                } as PluginRemove)
                            })
                                .then((data) => {
                                    console.log("plugin remove response");
                                })
                                .catch((error) => {
                                    console.error(error);
                                })
                        }}>
                            <CloseIcon color={"warning"}/>
                        </IconButton>
                    </Tooltip>

                </Box>
                <Typography>{(props.plugin.description) ? props.plugin.description : 'No description'}</Typography>
                <Accordion>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography>More details</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>Author: {props.plugin.author}</Typography>
                        <Typography>Website: {props.plugin.website}</Typography>
                        <Typography>EntryPoint: {props.plugin.jsEntryPoint}</Typography>
                        <Typography>Back API Version: {props.plugin.backApiVersion}</Typography>
                        <Typography>Front API Version: {props.plugin.frontApiVersion}</Typography>
                        <Typography>Hard dependencies: {JSON.stringify(props.plugin.hardDependencies)}</Typography>
                        <Typography>Soft dependencies: {JSON.stringify(props.plugin.softDependencies)}</Typography>
                        <Typography>Load before: {JSON.stringify(props.plugin.loadBefore)}</Typography>
                    </AccordionDetails>
                </Accordion>
            </CardContent>
        </Card>
    );
}

const tooltipProps: Partial<TooltipProps> = {
    placement: "right",
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};