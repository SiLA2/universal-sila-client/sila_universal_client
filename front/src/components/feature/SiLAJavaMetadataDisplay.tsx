import React from 'react';
import {
    Card,
    CardContent,
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Button,
    Grid, alpha, useTheme
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {SiLAJavaDefinedExecutionError, SiLAJavaMetadata, SiLAJavaSiLAElement} from "../../generated/silaModels";
import SiLAJavaDefinedExecutionErrorDisplay from "./SiLAJavaDefinedExecutionErrorDisplay";
import SiLAJavaDataTypeTypeDisplay from "./SiLAJavaDataTypeTypeDisplay";
import EditableListItemText from "./EditableListItemText";
import {useDispatch} from "react-redux";
import {appendToArrayOrAdd, removeFromArray, update} from "./store/dynamicSlice";
import List from "@mui/material/List";

interface SiLAJavaMetadataDisplayProps {
    metadata: SiLAJavaMetadata;
    dataTypeDefinition: SiLAJavaSiLAElement[];
    definedExecutionErrors: SiLAJavaDefinedExecutionError[];
    path: string;
    isEditing?: boolean;
    onRemove?: () => void;
}

const SiLAJavaMetadataDisplay: React.FC<SiLAJavaMetadataDisplayProps> = ({ metadata, definedExecutionErrors, dataTypeDefinition, path, onRemove, isEditing }) => {
    const theme = useTheme();
    const dispatch = useDispatch();

    function onTextChange(fieldPath: string, value: string) {
        dispatch(update({ path: [path, fieldPath].join('.'), value: value }))
    }

    function onAddCommandExecutionError() {
        if (definedExecutionErrors?.length) {
            dispatch(appendToArrayOrAdd({
                path: [path, 'definedExecutionErrors.identifier'].join('.'),
                value: definedExecutionErrors[0].identifier})
            );
        }
    }

    function onRemoveElement(fieldPath: string, index: number) {
        dispatch(removeFromArray({ path: [path, fieldPath].join('.'), index: index}));
    }

    return (
        <Card variant="outlined">
            <CardContent>
                <Grid container spacing={3} mb={2}>
                    {[
                        { primary: "Name", secondaryKey: "displayName", multiline: false, main: true },
                        { primary: "Identifier", secondaryKey: "identifier", multiline: false },
                        { primary: "Description", secondaryKey: "description", multiline: true, xs: 12, sm: 12, md: 12 },
                    ].map((item, idx) => (
                        <Grid item xs={item.xs || 12} sm={item.sm || 6} md={item.md || 3} key={item.secondaryKey}>
                            <Card
                                elevation={3}
                                variant={'elevation'}
                                sx={{
                                    ...(!item.main ? {} : {
                                        backgroundColor: alpha(theme.palette.primary.main, 0.7),
                                        color: theme.palette.primary.contrastText,
                                    }),
                                    height: '100%'
                                }}>
                                <CardContent>
                                    <EditableListItemText
                                        isEditing={isEditing}
                                        primary={item.primary}
                                        secondary={metadata[item.secondaryKey as keyof SiLAJavaMetadata] as string}
                                        multiline={item.multiline}
                                        editType={'secondary'}
                                        onTextChange={(s) => onTextChange(item.secondaryKey, s)}
                                    />
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                <Accordion>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography>Data Type</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <SiLAJavaDataTypeTypeDisplay
                            path={[path, 'dataType'].join('.')}
                            isEditing={isEditing}
                            dataTypeDefinition={dataTypeDefinition}
                            dataType={metadata.dataType}
                        />
                    </AccordionDetails>
                </Accordion>
                {((!!metadata?.definedExecutionErrors?.identifier?.length || isEditing) && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Defined Execution Errors</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {<List>
                                {(metadata?.definedExecutionErrors?.identifier || []).map((errorIdentifier, index) => (
                                    <SiLAJavaDefinedExecutionErrorDisplay
                                        onRemove={() => onRemoveElement('definedExecutionErrors.identifier', index)}
                                        isEditing={isEditing}
                                        path={[path, `definedExecutionErrors.identifier[${index}]`].join('.')}
                                        definedExecutionErrors={definedExecutionErrors}
                                        key={errorIdentifier + index}
                                        errorIdentifier={errorIdentifier}
                                    />
                                ))}
                            </List>}
                            {(isEditing && (
                                <Button
                                    color={'primary'}
                                    disabled={!definedExecutionErrors?.length}
                                    onClick={() => onAddCommandExecutionError()}>
                                    Add Defined Execution Error
                                </Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(isEditing && (
                    <Button color={'warning'} onClick={onRemove}>Delete Metadata</Button>
                ))}
            </CardContent>
        </Card>
    );
};

export default SiLAJavaMetadataDisplay;