import {SiLAJavaFeature} from "../../generated/silaModels";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "./store/store";
import {set} from "./store/dynamicSlice";
import {SiLAJavaFeatureDisplay} from "./SiLAJavaFeatureDisplay";

export interface SiLAJavaFeatureDisplayProps {
    feature: SiLAJavaFeature;
    isEditing?: boolean;
}

export const SiLAJavaFeatureEditor: React.FC<SiLAJavaFeatureDisplayProps> = ({ feature, isEditing }) => {
    const dispatch = useDispatch();
    const dynamicState = useSelector((state: RootState) => state.dynamic.data);

    useEffect(() => {
        if (feature.featureVersion) {
            dispatch(set({value: feature}));
        }
    }, [feature, dispatch]);

    return (
        <div>
            <SiLAJavaFeatureDisplay path={'$'} feature={dynamicState} isEditing={isEditing}/>
        </div>
    );
};
