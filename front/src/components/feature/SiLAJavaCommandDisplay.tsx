import React from 'react';
import {
    Card,
    CardContent,
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Button,
    Grid, alpha
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
    SiLAJavaCommand,
    SiLAJavaDefinedExecutionError,
    SiLAJavaSiLAElement
} from "../../generated/silaModels";
import SiLAJavaSiLAElementDisplay from "./SiLAJavaSiLAElementDisplay";
import {appendToArrayOrAdd, removeFromArray, update} from "./store/dynamicSlice";
import {useDispatch} from "react-redux";
import {
    DefaultIntermediateResponse,
    DefaultParameter,
    DefaultResponse
} from "./defaultValues";
import EditableListItemText from "./EditableListItemText";
import {useTheme} from "@mui/material/";
import SiLAJavaDefinedExecutionErrorDisplay from "./SiLAJavaDefinedExecutionErrorDisplay";
import List from "@mui/material/List";
import EditableSelect from "./EditableSelect";

interface SiLAJavaCommandDisplayProps {
    command: SiLAJavaCommand;
    dataTypeDefinition: SiLAJavaSiLAElement[];
    definedExecutionErrors: SiLAJavaDefinedExecutionError[];
    path: string;
    isEditing?: boolean;
    onRemove?: () => void;
}

const SiLAJavaCommandDisplay: React.FC<SiLAJavaCommandDisplayProps> = ({ command, dataTypeDefinition, definedExecutionErrors, path, isEditing, onRemove }) => {
    const theme = useTheme();
    const dispatch = useDispatch();

    function onTextChange(fieldPath: string, value: string) {
        dispatch(update({ path: [path, fieldPath].join('.'), value: value }))
    }

    function onAddParameter() {
        dispatch(appendToArrayOrAdd({ path: [path, 'parameter'].join('.'), value: DefaultParameter}));
    }

    function onAddResponse() {
        dispatch(appendToArrayOrAdd({ path: [path, 'response'].join('.'), value: DefaultResponse}));
    }

    function onAddIntermediateResponse() {
        dispatch(appendToArrayOrAdd({ path: [path, 'intermediateResponse'].join('.'), value: DefaultIntermediateResponse}));
    }

    function onAddCommandExecutionError() {
        if (definedExecutionErrors?.length) {
            dispatch(appendToArrayOrAdd({
                path: [path, 'definedExecutionErrors.identifier'].join('.'),
                value: definedExecutionErrors[0].identifier})
            );
        }
    }

    function onRemoveElement(fieldPath: string, index: number) {
        dispatch(removeFromArray({ path: [path, fieldPath].join('.'), index: index}));
    }

    return (
        <Card variant="outlined">
            <CardContent>
                <Grid container spacing={3} mb={2}>
                    {[
                        { primary: "Name", secondaryKey: "displayName", multiline: false, main: true },
                        { primary: "Identifier", secondaryKey: "identifier", multiline: false },
                        { primary: "Observable", secondaryKey: "observable", multiline: false, select: ['Yes', 'No'] },
                        { primary: "Description", secondaryKey: "description", multiline: true, xs: 12, sm: 12, md: 12 },
                    ].map((item, idx) => (
                        <Grid item xs={item.xs || 12} sm={item.sm || 6} md={item.md || 3} key={item.secondaryKey}>
                            <Card
                                elevation={3}
                                variant={'elevation'}
                                sx={{
                                    ...(!item.main ? {} : {
                                        backgroundColor: alpha(theme.palette.primary.main, 0.7),
                                        color: theme.palette.primary.contrastText,
                                    }),
                                    height: '100%'
                                }}>
                                <CardContent>
                                    {
                                        ((item.select) ? (
                                            <EditableSelect
                                                label={item.primary}
                                                isEditing={isEditing}
                                                options={item.select || []}
                                                value={command[item.secondaryKey as keyof SiLAJavaCommand] as string}
                                                defaultValue={command[item.secondaryKey as keyof SiLAJavaCommand] as string}
                                                onTextChange={(s) => onTextChange(item.secondaryKey, s)}
                                            />
                                        ) : (
                                            <EditableListItemText
                                                isEditing={isEditing}
                                                primary={item.primary}
                                                secondary={command[item.secondaryKey as keyof SiLAJavaCommand] as string}
                                                multiline={item.multiline}
                                                editType={'secondary'}
                                                onTextChange={(s) => onTextChange(item.secondaryKey, s)}
                                            />
                                        ))
                                    }
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                {((!!command?.parameter?.length || isEditing) && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Parameters</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {(command.parameter || []).map((element, index) => (
                                <SiLAJavaSiLAElementDisplay
                                    isEditing={isEditing}
                                    onRemove={() => onRemoveElement('parameter', index)}
                                    path={[path, `parameter[${index}]`].join('.')}
                                    dataTypeDefinition={dataTypeDefinition}
                                    key={element.identifier + index}
                                    element={element}
                                />
                            ))}
                            {(isEditing && (
                                <Button color={'primary'} onClick={() => onAddParameter()}>Add Parameter</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}
                {((!!command?.response?.length || isEditing) && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Responses</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {command.response.map((element, index) => (
                                <SiLAJavaSiLAElementDisplay
                                    onRemove={() => onRemoveElement('response',index )}
                                    isEditing={isEditing}
                                    path={[path, `response[${index}]`].join('.')}
                                    dataTypeDefinition={dataTypeDefinition}
                                    key={element.identifier + index}
                                    element={element}
                                />
                            ))}
                            {(isEditing && (
                                <Button color={'primary'} onClick={() => onAddResponse()}>Add Response</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}
                {((!!command?.intermediateResponse?.length || isEditing) && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Intermediate Responses</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {command.intermediateResponse.map((element, index) => (
                                <SiLAJavaSiLAElementDisplay
                                    onRemove={() => onRemoveElement('intermediateResponse',index )}
                                    isEditing={isEditing}
                                    path={[path, `intermediateResponse[${index}]`].join('.')}
                                    dataTypeDefinition={dataTypeDefinition}
                                    key={element.identifier + index}
                                    element={element}
                                />
                            ))}
                            {(isEditing && (
                                <Button color={'primary'} onClick={() => onAddIntermediateResponse()}>Add Intermediate Response</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}
                {((!!command?.definedExecutionErrors?.identifier?.length || isEditing) && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Defined Execution Errors</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {<List>
                                {(command?.definedExecutionErrors?.identifier || []).map((errorIdentifier, index) => (
                                    <SiLAJavaDefinedExecutionErrorDisplay
                                        onRemove={() => onRemoveElement('definedExecutionErrors.identifier', index)}
                                        isEditing={isEditing}
                                        path={[path, `definedExecutionErrors.identifier[${index}]`].join('.')}
                                        definedExecutionErrors={definedExecutionErrors}
                                        key={errorIdentifier + index}
                                        errorIdentifier={errorIdentifier}
                                    />
                                ))}
                            </List>}
                            {(isEditing && (
                                <Button
                                    color={'primary'}
                                    disabled={!definedExecutionErrors?.length}
                                    onClick={() => onAddCommandExecutionError()}>
                                    Add Defined Execution Error
                                </Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(isEditing && (
                    <Button color={'warning'} onClick={onRemove}>Remove Command</Button>
                ))}
            </CardContent>
        </Card>
    );
};

export default SiLAJavaCommandDisplay;
