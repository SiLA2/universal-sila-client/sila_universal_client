import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import jsonpath from 'jsonpath';

export interface DynamicState {
    data: any;
}

const initialState: DynamicState = {
    data: {}
};

export type ParsedPart = {
    expression: {
        type: string;
        value: string;
    };
    scope?: string;
};


const dynamicSlice = createSlice({
    name: 'dynamic',
    initialState,
    reducers: {
        set: (state, action: PayloadAction<{ value: any }>) => {
            state.data = action.payload.value;
        },
        appendToArrayOrAdd: (state, action: PayloadAction<{ path: string; value: any }>) => {
            const targetArray = jsonpath.value(state.data, action.payload.path);

            if (Array.isArray(targetArray)) {
                // Append to existing array
                targetArray.push(action.payload.value);
            } else {
                // Create a new array with the provided value
                jsonpath.value(state.data, action.payload.path, [action.payload.value]);
            }
        },
        removeFromArray: (state, action: PayloadAction<{ path: string; index: number }>) => {
            const targetArray = jsonpath.value(state.data, action.payload.path);

            if (Array.isArray(targetArray)) {
                // Remove the element at the provided index
                targetArray.splice(action.payload.index, 1);
            }
        },
        add: (state, action: PayloadAction<{ path: string; value: any }>) => {
            const path = action.payload.path;

            // If it's the root level
            if (!path) {
                Object.assign(state.data, action.payload.value);
                return;
            }

            const parentPath = jsonpath.parse(path).slice(0, -1).map((part: ParsedPart) => part.expression.value).join('.');
            const lastSegment = jsonpath.parse(path).slice(-1)[0].expression.value;

            // Access the parent object or array
            const parentObj = parentPath ? jsonpath.value(state.data, parentPath) : state.data;

            // Check and set the value
            if (typeof parentObj === "object") {
                if (Array.isArray(parentObj)) {
                    if (typeof lastSegment === "number") {
                        parentObj[lastSegment] = action.payload.value;
                    } else {
                        throw new Error("Trying to set a non-numeric key on an array.");
                    }
                } else {
                    parentObj[lastSegment] = action.payload.value;
                }
            }
        },
        remove: (state, action: PayloadAction<string>) => {
            const path = action.payload;

            if (!path) {  // if path is empty or undefined
                state.data = {};  // reset the root state
                return;
            }

            const parsedPath = jsonpath.nodes(state.data, path);
            if (parsedPath.length === 0) return;
            const parsedParts: ParsedPart[] = jsonpath.parse(action.payload);
            const parentPath = parsedParts.slice(0, -1).map((part: ParsedPart) => part.expression.value).join('.');
            const lastElement = jsonpath.parse(action.payload).slice(-1)[0];
            if (lastElement.scope === 'child') {
                delete jsonpath.value(state.data, parentPath)[lastElement.expression.value];
            } else if (lastElement.scope === 'index') {
                const arr = jsonpath.value(state.data, parentPath) as any[];
                arr.splice(Number(lastElement.expression.value), 1);
            }
        },
        update: (state, action: PayloadAction<{ path: string; value: any }>) => {
            const path = action.payload.path;

            if (!path) {  // if path is empty or undefined
                state.data = action.payload.value;
                return;
            }

            const parsedPath = jsonpath.nodes(state.data, path);
            if (parsedPath.length === 0) return;

            jsonpath.value(state.data, action.payload.path, action.payload.value);
        }
    }
});

export const { set, appendToArrayOrAdd, removeFromArray, add, remove, update } = dynamicSlice.actions;
export default dynamicSlice.reducer;