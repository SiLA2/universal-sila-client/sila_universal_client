import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { add, update, remove } from './dynamicSlice';
import {Button, TextField, Typography, MenuItem, Select, CardActions, Card} from '@mui/material';
import {CardContent} from "@mui/material/";

export interface StateItemProps {
    path: string;
    value: any;
}

const StateItem: React.FC<StateItemProps> = ({ path, value }) => {
    const dispatch = useDispatch();
    const [isAdding, setIsAdding] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [keyToAdd, setKeyToAdd] = useState('');
    const [valueToAdd, setValueToAdd] = useState('');
    const [typeToAdd, setTypeToAdd] = useState('property'); // property | object | array

    const handleAdd = () => {
        const newPath = path ? `${path}.${keyToAdd}` : keyToAdd;
        let newValue;
        switch(typeToAdd) {
            case 'property': newValue = valueToAdd || 'New Property'; break;
            case 'object': newValue = {}; break;
            case 'array': newValue = []; break;
            default: throw new Error('Invalid type to add');
        }
        dispatch(add({ path: newPath, value: newValue }));
        setIsAdding(false);
        setKeyToAdd('');
        setValueToAdd('');
    };

    const handleUpdate = () => {
        dispatch(update({ path, value: valueToAdd }));
        setIsEditing(false);
        setValueToAdd('');
    };

    const handleRemove = () => {
        dispatch(remove(path));
    };

    const renderType = (val: any) => {
        if (Array.isArray(val)) return 'array';
        if (typeof val === 'object') return 'object';
        return 'property';
    };

    const renderValue = (val: any, pth: string) => {
        const type = renderType(val);
        if (type === 'array') {
            return (
                <div>
                    {(val || []).map((item: any, index: number) => (
                        <StateItem key={index} path={`${pth}[${index}]`} value={item} />
                    ))}
                </div>
            );
        } else if (type === 'object') {
            return (
                <div>
                    {Object.entries(val || {}).map(([key, v]) => (
                        <StateItem key={key} path={`${pth}.${key}`} value={v} />
                    ))}
                </div>
            );
        } else {
            return isEditing ? (
                <TextField
                    value={valueToAdd || ''}
                    onChange={(e) => setValueToAdd(e.target.value)}
                    onBlur={handleUpdate}
                />
            ) : (
                <span>{val || ''}</span>
            );
        }
    };

    return (
        <Card variant="outlined" style={{ marginBottom: 8, marginLeft: path ? 20 : 0 }}>
            <CardContent>
                <Typography variant="body2" color="textSecondary" gutterBottom>
                    Name: {path.split('.').slice(-1)[0]}
                </Typography>
                <Typography variant="body2" color="textSecondary" gutterBottom>
                    Type: {renderType(value)}
                </Typography>
                {renderValue(value, path)}
            </CardContent>
            <CardActions>
                <Button onClick={() => setIsEditing(!isEditing)} size="small">Edit</Button>
                <Button onClick={handleRemove} size="small">Remove</Button>
                <Button onClick={() => setIsAdding(!isAdding)} size="small">Add</Button>

                {isAdding && (
                    <>
                        <TextField
                            size="small"
                            label={Array.isArray(value) ? 'Index' : 'Key'}
                            type={Array.isArray(value) ? 'number' : 'text'}
                            value={keyToAdd}
                            onChange={(e) => setKeyToAdd(e.target.value)}
                        />
                        <Select
                            size="small"
                            value={typeToAdd}
                            onChange={(e) => setTypeToAdd(e.target.value as string)}
                        >
                            <MenuItem value="property">Property</MenuItem>
                            <MenuItem value="object">Object</MenuItem>
                            <MenuItem value="array">Array</MenuItem>
                        </Select>
                        <Button onClick={handleAdd} size="small">Confirm Add</Button>
                    </>
                )}
            </CardActions>
        </Card>
    );
};

export default StateItem;