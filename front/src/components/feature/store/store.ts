import { configureStore } from '@reduxjs/toolkit';
import dynamicReducer from './dynamicSlice';

export const store = configureStore({
    reducer: {
        dynamic: dynamicReducer
    }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;