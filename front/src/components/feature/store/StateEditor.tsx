import React from 'react';
import { useSelector } from 'react-redux';
import StateItem from './StateItem';
import {RootState} from "./store";

const StateEditor: React.FC = () => {
    const dynamicState = useSelector((state: RootState) => state.dynamic.data);

    return (
        <div>
            <StateItem path="$" value={dynamicState} />
        </div>
    );
};

export default StateEditor;