import React, {useState} from 'react';
import {
    Typography,
    List,
    ListItem,
    Accordion,
    AccordionSummary,
    AccordionDetails, Select, MenuItem, Button,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {SiLAJavaConstraints} from "../../generated/silaModels";
import {useDispatch} from "react-redux";
import {update, add, remove} from "./store/dynamicSlice";
import EditableListItemText from "./EditableListItemText";

export interface SiLAJavaConstraintsDisplayProps {
    constraints: Partial<SiLAJavaConstraints>;
    path: string;
    isEditing?: boolean;
}

const siLAJavaConstraintOptions: {[k in keyof SiLAJavaConstraints]: string} = {
    length: 'Length',
    minimalLength: 'Minimal Length',
    maximalLength: 'Maximal Length',
    set: 'Set',
    pattern: 'Pattern',
    maximalExclusive: 'Maximal Exclusive',
    maximalInclusive: 'Maximal Inclusive',
    minimalExclusive: 'Minimal Exclusive',
    minimalInclusive: 'Minimal Inclusive',
    unit: 'Unit',
    contentType: 'Content Type',
    elementCount: 'Element Count',
    minimalElementCount: 'Minimal Element Count',
    maximalElementCount: 'Maximal Element Count',
    fullyQualifiedIdentifier: 'Fully Qualified Identifier',
    schema: 'Schema',
    allowedTypes: 'Allowed Types',
};


export const SiLAJavaConstraintsDisplay: React.FC<SiLAJavaConstraintsDisplayProps> = ({ constraints, path, isEditing }) => {
    const [selectedConstraint, setSelectedConstraint] = useState<keyof SiLAJavaConstraints>('length');
    const dispatch = useDispatch();

    function onTextChange(fieldPath: string, value: string) {
        dispatch(update({ path: [path, fieldPath].join('.'), value: value }))
    }

    function addConstraint(fieldPath: string) {
        dispatch(add({ path: [path, fieldPath].join('.'), value: '' }))
    }

    function removeConstraint(fieldPath: string) {
        dispatch(remove([path, fieldPath].join('.')));
    }

    // todo support non basic constraints

    return (
        <div>
            <List>
                {(constraints?.length != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Length"
                            secondary={constraints.length.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'length'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['length'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.minimalLength != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Minimal Length"
                            secondary={constraints.minimalLength.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'minimalLength'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['minimalLength'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.maximalLength != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Maximal Length"
                            secondary={constraints.maximalLength.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'maximalLength'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['maximalLength'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.pattern != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Pattern"
                            secondary={constraints.pattern.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'pattern'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['pattern'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.maximalExclusive != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Maximal Exclusive"
                            secondary={constraints.maximalExclusive}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'maximalExclusive'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['maximalExclusive'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.maximalInclusive != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Maximal Inclusive"
                            secondary={constraints.maximalInclusive}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'maximalInclusive'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['maximalInclusive'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.minimalExclusive != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Minimal Exclusive"
                            secondary={constraints.minimalExclusive}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'minimalExclusive'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['minimalExclusive'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.minimalInclusive != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Minimal Inclusive"
                            secondary={constraints.minimalInclusive}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'minimalInclusive'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['minimalInclusive'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.elementCount != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Element Count"
                            secondary={constraints.elementCount.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'elementCount'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['elementCount'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.minimalElementCount != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Minimal Element Count"
                            secondary={constraints.minimalElementCount.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'minimalElementCount'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['minimalElementCount'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.maximalElementCount != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Maximal Element Count"
                            secondary={constraints.maximalElementCount.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'maximalElementCount'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['maximalElementCount'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {(constraints?.fullyQualifiedIdentifier != null) && (
                    <ListItem>
                        <EditableListItemText
                            isEditing={isEditing}
                            primary="Fully Qualified Identifier"
                            secondary={constraints.fullyQualifiedIdentifier.toString()}
                            editType={'secondary'}
                            onTextChange={(s) => onTextChange(['constraints', 'fullyQualifiedIdentifier'].join('.'), s)}
                        />
                        {(isEditing &&
                            (
                                <Button
                                    color={'warning'}
                                    onClick={() => removeConstraint(['fullyQualifiedIdentifier'].join('.'))}
                                >
                                    Remove Constraint
                                </Button>
                            )
                        )}
                    </ListItem>
                )}
                {((!!constraints?.set?.value?.length || isEditing) && (
                    <Accordion variant={'outlined'}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Set Values</Typography>
                            {(constraints?.set?.value?.length && isEditing &&
                                (
                                    <Button
                                        color={'warning'}
                                        onClick={() => removeConstraint(['set'].join('.'))}
                                    >
                                        Remove Constraint
                                    </Button>
                                )
                            )}
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(constraints?.set?.value || []).map((val, index) => (
                                    <ListItem key={index}>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary={val}
                                            editType={'primary'}
                                            onTextChange={(s) => onTextChange(`constraints.set.value[${index}]`, s)}
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(constraints?.unit != null && (
                    <Accordion variant={'outlined'}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Unit</Typography>
                            {(isEditing &&
                                (
                                    <Button
                                        color={'warning'}
                                        onClick={() => removeConstraint(['unit'].join('.'))}
                                    >
                                        Remove Constraint
                                    </Button>
                                )
                            )}
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(constraints?.unit?.label != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="Label"
                                            secondary={constraints.unit.label}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'unit', 'label'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                                {(constraints?.unit?.factor != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="Factor"
                                            secondary={constraints.unit.factor.toString()}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'unit', 'factor'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                                {(constraints?.unit?.offset != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="Offset"
                                            secondary={constraints.unit.offset.toString()}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'unit', 'offset'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(constraints?.contentType != null && (
                    <Accordion variant={'outlined'}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Content Type</Typography>
                            {(isEditing &&
                                (
                                    <Button
                                        color={'warning'}
                                        onClick={() => removeConstraint(['contentType'].join('.'))}
                                    >
                                        Remove Constraint
                                    </Button>
                                )
                            )}
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(constraints?.contentType?.type != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="Type"
                                            secondary={constraints.contentType.type}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'contentType', 'type'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                                {(constraints?.contentType?.subtype != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="Subtype"
                                            secondary={constraints.contentType.subtype}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'contentType', 'subtype'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(constraints?.schema != null && (
                    <Accordion variant={'outlined'}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Schema</Typography>
                            {(isEditing &&
                                (
                                    <Button
                                        color={'warning'}
                                        onClick={() => removeConstraint(['schema'].join('.'))}
                                    >
                                        Remove Constraint
                                    </Button>
                                )
                            )}
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(constraints?.schema?.type != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="Type"
                                            secondary={constraints.schema.type}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'schema', 'type'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                                {(constraints?.schema?.url != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="URL"
                                            secondary={constraints.schema.url}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'schema', 'url'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                                {(constraints?.schema?.inline != null && (
                                    <ListItem>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary="Inline"
                                            secondary={constraints.schema.inline}
                                            editType={'secondary'}
                                            onTextChange={(s) => onTextChange(['constraints', 'schema', 'inline'].join('.'), s)}
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </AccordionDetails>
                    </Accordion>
                ))}
                {((!!constraints?.allowedTypes?.dataType?.length) && (
                    <Accordion variant={'outlined'}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Allowed Types</Typography>
                            {(isEditing &&
                                (
                                    <Button
                                        color={'warning'}
                                        onClick={() => removeConstraint(['allowedTypes'].join('.'))}
                                    >
                                        Remove Constraint
                                    </Button>
                                )
                            )}
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(constraints?.allowedTypes?.dataType || []).map((type, index) => (
                                    /* todo implement other types and provide an identifier to the key to avoid React's reconciliation process issues */
                                    <ListItem key={index}>
                                        <EditableListItemText
                                            isEditing={isEditing}
                                            primary={type.dataTypeIdentifier}
                                            editType={'primary'}
                                            onTextChange={(s) => onTextChange(`constraints.allowedTypes.dataType[${index}].dataTypeIdentifier`, s)}
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </AccordionDetails>
                    </Accordion>
                ))}
            </List>
            {
                (isEditing && (
                    <>
                        <Select
                            labelId="constraint-select-label"
                            id="constraint-select"
                            value={selectedConstraint}
                            label="Constraint"
                            onChange={
                                (e) => setSelectedConstraint(e.target.value as keyof SiLAJavaConstraints)
                            }
                        >
                            {Object.entries(siLAJavaConstraintOptions).map(([value, name]) => (
                                <MenuItem key={value} value={value}>
                                    {name}
                                </MenuItem>
                            ))}
                        </Select>
                        <Button
                            color={'primary'}
                            disabled={!!constraints?.[selectedConstraint]}
                            onClick={() => addConstraint(selectedConstraint)}>
                            Add Constraint
                        </Button>
                    </>
                ))
            }
        </div>
    );
};

export default SiLAJavaConstraintsDisplay;