import React from 'react';
import { Button } from '@mui/material';

interface XMLDownloaderProps {
    generateXml: () => Promise<string | undefined>;
}

const XMLDownloader: React.FC<XMLDownloaderProps> = ({ generateXml }) => {
    const handleCopyToClipboard = async () => {
        const xml = await generateXml();
        if (!xml) {
            return;
        }
        navigator.clipboard.writeText(xml)
            .then(() => {
                console.log('XML copied to clipboard successfully!');
            })
            .catch(err => {
                console.error('Could not copy XML to clipboard:', err);
            });
    };

    const handleDownload = async () => {
        const xml = await generateXml();
        if (!xml) {
            return;
        }
        const blob = new Blob([xml], { type: 'text/xml' });
        const a = document.createElement('a');
        const url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'document.xml';
        a.click();
        window.URL.revokeObjectURL(url);
    };

    return (
        <div>
            <Button onClick={handleCopyToClipboard} variant="contained" color="primary" style={{ marginRight: '10px' }}>
                Copy to Clipboard
            </Button>
            <Button onClick={handleDownload} variant="contained" color="primary">
                Download XML
            </Button>
        </div>
    );
};

export default XMLDownloader;