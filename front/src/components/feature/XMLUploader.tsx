import React, { useRef } from 'react';
import { Button, TextField } from '@mui/material';

const XMLUploader: React.FC<{onXML: (xml: string) => void}> = (props) => {
    const fileInputRef = useRef<HTMLInputElement>(null);

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = (e) => {
                props.onXML(e.target?.result as string);
            };
            reader.readAsText(file);
        }
    };

    const handlePaste = (event: React.ClipboardEvent<HTMLDivElement>) => {
        props.onXML(event.clipboardData.getData('text'));
    };

    const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        event.stopPropagation();
    };

    const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        event.stopPropagation();

        const file = event.dataTransfer.files[0];
        if (file && file.type === "text/xml") {
            const reader = new FileReader();
            reader.onload = (e) => {
                props.onXML(e.target?.result as string);
            };
            reader.readAsText(file);
        } else {
            console.warn("Invalid file type");
        }
    };

    return (
        <div onPaste={handlePaste} onDragOver={handleDragOver} onDrop={handleDrop} style={{ border: '1px dashed gray', padding: '20px', minHeight: '100px' }}>
            <TextField
                multiline
                rows={4}
                value={''}
                placeholder="Paste your XML here or drag & drop a file."
                variant="outlined"
                fullWidth
            />
            <Button variant="contained" component="label" style={{ marginTop: '10px' }}>
                Upload File
                <input
                    type="file"
                    hidden
                    onChange={handleFileChange}
                    ref={fileInputRef}
                    accept=".xml"
                />
            </Button>
        </div>
    );
};

export default XMLUploader;
