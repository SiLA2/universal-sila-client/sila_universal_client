import React, {useEffect, useState} from 'react';
import {Typography, Select, MenuItem, FormControl, SelectChangeEvent, SelectProps} from '@mui/material';
import {Box} from "@mui/material/";

type EditableSelectProps = SelectProps<string> & {
    options: string[];
    isEditing?: boolean;
    onTextChange: (newText: string) => void;
}

const EditableSelect: React.FC<EditableSelectProps> = ({ value, options, onTextChange, isEditing: edit, ...props }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [selectedValue, setSelectedValue] = useState<string | undefined>(value);

    useEffect(() => {
        setSelectedValue(value);
    }, [value]);

    const handleTypographyClick = () => {
        if (edit) {
            setIsEditing(true);
        }
    };

    const handleSelectChange = (e: SelectChangeEvent) => {
        const newValue = e.target.value;
        setSelectedValue(newValue);
        onTextChange(newValue);
        setIsEditing(false);
    };

    return (
        <Box sx={{display: 'flex', flexDirection: 'column'}}>
            {(props.label && (
                <Typography onClick={handleTypographyClick}>
                    {props.label}
                </Typography>
            ))}
            {
                isEditing ? (
                    <FormControl>
                        <Select
                            {...props}
                            value={selectedValue}
                            onChange={handleSelectChange}
                            placeholder={'Select a value'}
                            onBlur={() => setIsEditing(false)}
                            autoFocus
                        >
                            {options.map((option) => (
                                <MenuItem key={option} value={option}>
                                    {option}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                ) : (
                    <Box sx={{display: 'flex'}}>
                        <Typography onClick={handleTypographyClick}>
                            {selectedValue || 'Select a value'}
                        </Typography>
                    </Box>
                )
            }
        </Box>
    )
};

export default EditableSelect;