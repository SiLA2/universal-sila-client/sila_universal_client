import React from 'react';
import {
    Card,
    CardContent,
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Button,
    useTheme, Grid, alpha
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import SiLAJavaSiLAElementDisplay from "./SiLAJavaSiLAElementDisplay";
import {SiLAJavaProperty, SiLAJavaSiLAElement} from "../../generated/silaModels";
import EditableListItemText from "./EditableListItemText";
import {useDispatch} from "react-redux";
import {update} from "./store/dynamicSlice";
import EditableSelect from "./EditableSelect";

interface SiLAJavaPropertyDisplayProps {
    property: SiLAJavaProperty;
    dataTypeDefinition: SiLAJavaSiLAElement[];
    path: string;
    isEditing?: boolean;
    onRemove?: () => void;
}

const SiLAJavaPropertyDisplay: React.FC<SiLAJavaPropertyDisplayProps> = ({ property, dataTypeDefinition, path, onRemove, isEditing }) => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const typeDefinition = dataTypeDefinition.find(d => d.identifier === property.dataType.dataTypeIdentifier);

    function onTextChange(fieldPath: string, value: string) {
        dispatch(update({ path: [path, fieldPath].join('.'), value: value }))
    }

    return (
        <Card variant="outlined">
            <CardContent>
                <Grid container spacing={3} mb={2}>
                    {[
                        { primary: "Name", secondaryKey: "displayName", multiline: false, main: true },
                        { primary: "Identifier", secondaryKey: "identifier", multiline: false },
                        { primary: "Observable", secondaryKey: "observable", multiline: false, select: ['Yes', 'No'] },
                        { primary: "Description", secondaryKey: "description", multiline: true, xs: 12, sm: 12, md: 12 },
                    ].map((item, idx) => (
                        <Grid item xs={item.xs || 12} sm={item.sm || 6} md={item.md || 3} key={item.secondaryKey}>
                            <Card
                                elevation={3}
                                variant={'elevation'}
                                sx={{
                                    ...(!item.main ? {} : {
                                        backgroundColor: alpha(theme.palette.primary.main, 0.7),
                                        color: theme.palette.primary.contrastText,
                                    }),
                                    height: '100%'
                                }}>
                                <CardContent>
                                    {
                                        ((item.select) ? (
                                            <EditableSelect
                                                label={item.primary}
                                                isEditing={isEditing}
                                                options={item.select || []}
                                                value={property[item.secondaryKey as keyof SiLAJavaProperty] as string}
                                                defaultValue={property[item.secondaryKey as keyof SiLAJavaProperty] as string}
                                                onTextChange={(s) => onTextChange(item.secondaryKey, s)}
                                            />
                                        ) : (
                                            <EditableListItemText
                                                isEditing={isEditing}
                                                primary={item.primary}
                                                secondary={property[item.secondaryKey as keyof SiLAJavaProperty] as string}
                                                multiline={item.multiline}
                                                editType={'secondary'}
                                                onTextChange={(s) => onTextChange(item.secondaryKey, s)}
                                            />
                                        ))
                                    }
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                <Accordion>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography>Type</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <SiLAJavaSiLAElementDisplay
                            path={[path].join('.')}
                            isEditing={isEditing}
                            dataTypeDefinition={dataTypeDefinition}
                            element={{
                                identifier: "", // No direct description in DataType, can be expanded if needed.
                                displayName: "",
                                description: "",
                                dataType: property.dataType,
                                ...typeDefinition,
                            }}
                        />
                    </AccordionDetails>
                </Accordion>
                {(isEditing && (
                    <Button color={'warning'} onClick={onRemove}>Delete Property</Button>
                ))}
            </CardContent>
        </Card>
    );
};

export default SiLAJavaPropertyDisplay;