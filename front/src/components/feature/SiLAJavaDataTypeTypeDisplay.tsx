import React from 'react';
import {
    Card,
    CardContent,
    Typography,
    List,
    ListItem,
    Accordion,
    AccordionSummary,
    AccordionDetails, Button,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
    SiLAJavaDataTypeType,
    SiLAJavaSiLAElement
} from "../../generated/silaModels";
import SiLAJavaConstraintsDisplay from "./SiLAJavaConstraintsDisplay";
import SiLAJavaSiLAElementDisplay from "./SiLAJavaSiLAElementDisplay";
import EditableSelect from "./EditableSelect";
import {
    DefaultBasicDataType, DefaultConstrainedDataType,
    DefaultDataTypeIdentifierDataType, DefaultElement, DefaultListDataType,
    DefaultStructureDataType,
    SiLAJavaBasicTypes
} from "./defaultValues";
import {useDispatch} from "react-redux";
import {appendToArrayOrAdd, removeFromArray, update} from "./store/dynamicSlice";
import {Box} from "@mui/material/";

export interface SiLAJavaDataTypeTypeDisplayProps {
    dataType: Partial<SiLAJavaDataTypeType>;
    dataTypeDefinition: SiLAJavaSiLAElement[];
    path: string;
    isEditing?: boolean;
    onRemove?: () => void;
}

export const SiLAJavaDataTypeTypeDisplay: React.FC<SiLAJavaDataTypeTypeDisplayProps> = ({ dataType, dataTypeDefinition, path, isEditing, onRemove }) => {
    const typeDefinition = dataTypeDefinition.find(d => d.identifier === dataType.dataTypeIdentifier);
    const dt = dataType.dataTypeIdentifier ? typeDefinition?.dataType : dataType;
    const dispatch = useDispatch();

    function onTextChange(fieldPath: string, value: string) {
        dispatch(update({ path: [path, fieldPath].join('.'), value: value }))
    }

    function onSetType(type: SiLAJavaDataTypeType) {
        dispatch(update({ path: path, value: type }));
    }

    function onAddElement(fieldPath: string, value: any) {
        dispatch(appendToArrayOrAdd({ path: [path, fieldPath].join('.'), value: value }));
    }

    function onRemoveElement(fieldPath: string, index: number) {
        dispatch(removeFromArray({ path: [path, fieldPath].join('.'), index: index}));
    }

    return (
        <Card variant="outlined">
            <CardContent>
                {((isEditing) && (
                    <>
                        <Typography>Change to</Typography>
                        <Box>
                            <Button onClick={() => onSetType(DefaultBasicDataType)}>Basic Type</Button>
                            <Button onClick={() => onSetType(DefaultListDataType)}>List Type</Button>
                            <Button onClick={() => onSetType(DefaultStructureDataType)}>Structure Type</Button>
                            <Button onClick={() => onSetType(DefaultConstrainedDataType)}>Constrained Type</Button>
                            <Button onClick={() => onSetType(DefaultDataTypeIdentifierDataType)}>Data Type Identifier</Button>
                        </Box>
                    </>
                ))}
                {(dataType.dataTypeIdentifier != null && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Data Type Identifier</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <EditableSelect
                                isEditing={isEditing}
                                value={dataType.dataTypeIdentifier}
                                defaultValue={dataType.dataTypeIdentifier}
                                options={dataTypeDefinition.map(d => d.identifier)}
                                onTextChange={(s) => onTextChange('dataTypeIdentifier', s)}
                            />
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(dt?.basic && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Basic Type</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <EditableSelect
                                isEditing={isEditing && !typeDefinition}
                                value={dt.basic}
                                defaultValue={dt.basic}
                                options={SiLAJavaBasicTypes}
                                onTextChange={(s) => onTextChange('basic', s)}
                            />
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(dt?.list && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>List Type</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <SiLAJavaDataTypeTypeDisplay
                                isEditing={isEditing && !typeDefinition}
                                path={[path, 'list', 'dataType'].join('.')}
                                dataTypeDefinition={dataTypeDefinition}
                                dataType={dt.list.dataType}
                            />
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(dt?.structure && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Structure Type</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {dt.structure?.element.map((elem, index) => (
                                    <ListItem key={elem.identifier + index}>
                                        <SiLAJavaSiLAElementDisplay
                                            onRemove={() => onRemoveElement(['structure', 'element'].join('.'), index)}
                                            isEditing={isEditing && !typeDefinition}
                                            path={[path, `structure.element[${index}]`].join('.')}
                                            element={elem}
                                            dataTypeDefinition={dataTypeDefinition}
                                        />
                                    </ListItem>
                                ))}
                            </List>
                            {((isEditing && !typeDefinition) && (
                                <Button onClick={() => {onAddElement(['structure', 'element'].join('.'), DefaultElement)}}>
                                    Add Element
                                </Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}
                {(dt?.constrained && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography>Constrained Type</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <div>
                                <Typography variant="body2">Type</Typography>
                                <SiLAJavaDataTypeTypeDisplay
                                    isEditing={isEditing && !typeDefinition}
                                    path={[path, 'constrained', 'dataType'].join('.')}
                                    dataTypeDefinition={dataTypeDefinition}
                                    dataType={dt.constrained.dataType}
                                />
                                <Typography variant="body2">Constraints</Typography>
                                <SiLAJavaConstraintsDisplay
                                    isEditing={isEditing && !typeDefinition}
                                    path={[path, 'constrained', 'constraints'].join('.')}
                                    constraints={dt.constrained.constraints}
                                />
                            </div>
                        </AccordionDetails>
                    </Accordion>
                ))}
            </CardContent>
        </Card>
    );
};

export default SiLAJavaDataTypeTypeDisplay;