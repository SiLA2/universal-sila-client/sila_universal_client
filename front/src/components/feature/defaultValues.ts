import {
    SiLAJavaBasicType,
    SiLAJavaCommand,
    SiLAJavaConstrainedType,
    SiLAJavaDataTypeType, SiLAJavaDefinedExecutionError, SiLAJavaFeature,
    SiLAJavaListType,
    SiLAJavaMetadata,
    SiLAJavaProperty,
    SiLAJavaSiLAElement,
    SiLAJavaStructureType,
} from "../../generated/silaModels";

export const SiLAJavaBasicTypes: SiLAJavaBasicType[] = ["STRING", "INTEGER", "REAL", "BOOLEAN", "BINARY", "DATE", "TIME", "TIMESTAMP", "ANY"];

export const DefaultDataTypeIdentifier: string = ""

export const DefaultDataType: SiLAJavaDataTypeType = {
    "basic": "STRING",
    "list": null as any,
    "structure": null as any,
    "constrained": null as any,
    "dataTypeIdentifier": null as any
}

export const DefaultElement: SiLAJavaSiLAElement = {
    "identifier": "ANewElement",
    "displayName": "A New Element",
    "description": "A new element waiting to be described.",
    "dataType": DefaultDataType
}

export const DefaulBasicType: SiLAJavaBasicType = "STRING";
export const DefaultListType: SiLAJavaListType = {
    dataType: DefaultDataType
}
export const DefaultStructureType: SiLAJavaStructureType = {
    element: [DefaultElement]
}
export const DefaultConstrainedType: SiLAJavaConstrainedType = {
    constraints: {
        minimalLength: 10
    } as any,
    dataType: DefaultDataType
}



export const DefaultBasicDataType: SiLAJavaDataTypeType = {
    "basic": DefaulBasicType,
    "list": null as any,
    "structure": null as any,
    "constrained": null as any,
    "dataTypeIdentifier": null as any
}

export const DefaultListDataType: SiLAJavaDataTypeType = {
    "basic": null as any,
    "list": DefaultListType,
    "structure": null as any,
    "constrained": null as any,
    "dataTypeIdentifier": null as any
}

export const DefaultStructureDataType: SiLAJavaDataTypeType = {
    "basic": null as any,
    "list": null as any,
    "structure": DefaultStructureType,
    "constrained": null as any,
    "dataTypeIdentifier": null as any
}

export const DefaultConstrainedDataType: SiLAJavaDataTypeType = {
    "basic": null as any,
    "list": null as any,
    "structure": null as any,
    "constrained": DefaultConstrainedType,
    "dataTypeIdentifier": null as any
}

export const DefaultDataTypeIdentifierDataType: SiLAJavaDataTypeType = {
    "basic": null as any,
    "list": null as any,
    "structure": null as any,
    "constrained": null as any,
    "dataTypeIdentifier": DefaultDataTypeIdentifier
}
export const DefaultParameter: SiLAJavaSiLAElement = {
    "identifier": "ANewParameter",
    "displayName": "A New Parameter",
    "description": "A new parameter waiting to be described.",
    "dataType": DefaultDataType
}

export const DefaultResponse: SiLAJavaSiLAElement = {
    "identifier": "ANewResponse",
    "displayName": "A New Response",
    "description": "A new response waiting to be described.",
    "dataType": DefaultDataType
}

export const DefaultIntermediateResponse: SiLAJavaSiLAElement = {
    "identifier": "ANewIntermediateResponse",
    "displayName": "A New Intermediate Response",
    "description": "A new intermediate response waiting to be described.",
    "dataType": DefaultDataType
}


export const DefaultCommand: SiLAJavaCommand = {
    "identifier": "ANewCommand",
    "displayName": "A New Command",
    "description": "A new command waiting to be described.",
    "observable": "No",
    "parameter": [],
    "response": [],
    "intermediateResponse": [],
    "definedExecutionErrors": {
        "identifier": []
    }
}

export const DefaultProperty: SiLAJavaProperty = {
    "identifier": "ANewProperty",
    "displayName": "A New Property",
    "description": "A new property waiting to be described.",
    "observable": "No",
    "dataType": DefaultBasicDataType,
    "definedExecutionErrors": {
        "identifier": []
    }
}

export const DefaultMetadata: SiLAJavaMetadata = {
    "identifier": "ANewMetadata",
    "displayName": "A New Metadata",
    "description": "A new metadata waiting to be described.",
    "dataType": DefaultBasicDataType,
    "definedExecutionErrors": {
        "identifier": []
    }
}

export const DefaultDefinedExecutionError: SiLAJavaDefinedExecutionError = {
    "identifier": "ANewDefinedExecutionError",
    "displayName": "A New Defined Execution Error",
    "description": "A new defined execution error waiting to be described.",
}

export const DefaultDataTypeDefinition: SiLAJavaSiLAElement = {
    "identifier": "ANewDataTypeDefinition",
    "displayName": "A New Data Type Definition",
    "description": "A new data type definition waiting to be described.",
    "dataType": DefaultBasicDataType,
}

export const DefaultFeature: SiLAJavaFeature = {
    "identifier": "ANewFeature",
    "displayName": "A New Feature",
    "description": "A new feature waiting to be described.",
    "command": [],
    "property": [],
    "metadata": [],
    "definedExecutionError": [],
    "dataTypeDefinition": [],
    "locale": "",
    "siLA2Version": "1.1",
    "featureVersion": "1.0",
    "maturityLevel": "Draft",
    "originator": "org.example",
    "category": "test"
}