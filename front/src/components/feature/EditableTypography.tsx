import React, {useEffect, useState} from 'react';
import {Typography, TextField, TypographyProps} from '@mui/material';

interface EditableTypographyProps extends TypographyProps {
    text: string;
    onTextChange: (newText: string) => void;
    label?: string;
    isEditing?: boolean;
}

const EditableTypography: React.FC<EditableTypographyProps> = ({ variant, text, onTextChange, label, isEditing: edit, ...props }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [editableText, setEditableText] = useState(text);

    useEffect(() => {
        setEditableText(text);
    }, [text]);

    const handleTypographyClick = () => {
        if (edit) {
            setIsEditing(true);
        }
    };

    const handleTextFieldChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setEditableText(e.target.value);
    };

    const handleTextFieldBlur = () => {
        onTextChange(editableText);
        setIsEditing(false);
    };

    const handleTextFieldKeyDown = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            onTextChange(editableText);
            setIsEditing(false);
        }
    };

    return isEditing ? (
        <TextField
            value={editableText || 'Enter text'}
            defaultValue={editableText}
            label={label}
            onChange={handleTextFieldChange}
            onBlur={handleTextFieldBlur}
            onKeyDown={handleTextFieldKeyDown}
            autoFocus
            fullWidth
        />
    ) : (
        <>
            {(label && (
                <Typography variant={'subtitle1'} onClick={handleTypographyClick}>
                    {label}
                </Typography>
            ))}
            <Typography {...props}  variant={variant} onClick={handleTypographyClick}>
                {text || 'Click to enter a value'}
            </Typography>
        </>
    );
};

export default EditableTypography;