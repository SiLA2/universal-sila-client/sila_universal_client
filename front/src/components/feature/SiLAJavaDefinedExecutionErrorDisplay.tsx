import React from 'react';
import {AccordionDetails, alpha, Button, Card, CardContent, Grid, useTheme} from '@mui/material';
import {SiLAJavaDefinedExecutionError} from "../../generated/silaModels";
import EditableListItemText from "./EditableListItemText";
import {useDispatch} from "react-redux";
import {update} from "./store/dynamicSlice";
import EditableSelect from "./EditableSelect";
import {Typography} from "@mui/material/";

interface SiLAJavaDefinedExecutionErrorDisplayProps {
    definedExecutionErrors: SiLAJavaDefinedExecutionError[];
    error?: SiLAJavaDefinedExecutionError;
    errorIdentifier?: string;
    path: string;
    isEditing?: boolean;
    onRemove?: () => void;
}

const SiLAJavaDefinedExecutionErrorDisplay: React.FC<SiLAJavaDefinedExecutionErrorDisplayProps> = ({ error, errorIdentifier, definedExecutionErrors, path, isEditing, onRemove }) => {
    const errorDefinition = definedExecutionErrors.find(d => d.identifier === errorIdentifier);
    const dt = error ? error : errorDefinition;
    const theme = useTheme();
    const dispatch = useDispatch();

    function onTextChange( value: string, fieldPath?: string) {
        dispatch(update({ path: [path, fieldPath].filter(Boolean).join('.'), value: value }))
    }

    return (
        <Card variant="outlined">
            <CardContent>
                {
                    (error) ? (
                        <Grid container spacing={3} mb={2}>
                            {[
                                { primary: "Name", secondaryKey: "displayName", multiline: false, main: true },
                                { primary: "Identifier", secondaryKey: "identifier", multiline: false },
                                { primary: "Description", secondaryKey: "description", multiline: true, xs: 12, sm: 12, md: 12 },
                            ].map((item, idx) => (
                                <Grid item xs={item.xs || 12} sm={item.sm || 6} md={item.md || 3} key={item.secondaryKey}>
                                    <Card
                                        elevation={3}
                                        variant={'elevation'}
                                        sx={{
                                            ...(!item.main ? {} : {
                                                backgroundColor: alpha(theme.palette.primary.main, 0.7),
                                                color: theme.palette.primary.contrastText,
                                            }),
                                            height: '100%'
                                        }}>
                                        <CardContent>
                                            <EditableListItemText
                                                isEditing={isEditing}
                                                primary={item.primary}
                                                secondary={dt?.[item.secondaryKey as keyof SiLAJavaDefinedExecutionError] || 'Unknown Defined Execution Error'}
                                                multiline={item.multiline}
                                                editType={'secondary'}
                                                onTextChange={(s) => onTextChange(s, item.secondaryKey)}
                                            />
                                        </CardContent>
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                    ) : (
                        (dt) ? (
                            <AccordionDetails>
                                <EditableSelect
                                    isEditing={isEditing}
                                    value={dt.identifier}
                                    defaultValue={dt.identifier}
                                    options={definedExecutionErrors.map(d => d.identifier)}
                                    onTextChange={(s) => onTextChange(s)}
                                />
                            </AccordionDetails>
                        ) : (
                            <Typography>
                                Invalid Defined Execution Errors!
                            </Typography>
                        )
                    )
                }
                {(isEditing && (
                    <Button color={'warning'} onClick={onRemove}>Delete Element</Button>
                ))}
            </CardContent>
        </Card>
    );
};

export default SiLAJavaDefinedExecutionErrorDisplay;