import {SiLAJavaFeature} from "../../generated/silaModels";
import React, {useState} from "react";
import {
    Accordion, AccordionDetails,
    AccordionSummary, Alert,
    alpha,
    Button,
    Card,
    CardContent,
    Grid,
    Typography,
    useTheme
} from "@mui/material";
import List from "@mui/material/List";
import SiLAJavaCommandDisplay from "./SiLAJavaCommandDisplay";
import SiLAJavaPropertyDisplay from "./SiLAJavaPropertyDisplay";
import SiLAJavaMetadataDisplay from "./SiLAJavaMetadataDisplay";
import SiLAJavaDefinedExecutionErrorDisplay from "./SiLAJavaDefinedExecutionErrorDisplay";
import SiLAJavaSiLAElementDisplay from "./SiLAJavaSiLAElementDisplay";
import {appendToArrayOrAdd, removeFromArray, update} from "./store/dynamicSlice";
import {useDispatch} from "react-redux";
import {
    DefaultCommand, DefaultDataTypeDefinition,
    DefaultDefinedExecutionError,
    DefaultMetadata,
    DefaultProperty
} from "./defaultValues";
import EditableListItemText from "./EditableListItemText";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import FeatureSerializer from "../../utils/parser/featureSerializer";
import XMLDownloader from "./XMLDownloader";
import {getBackEndpoint} from "../../utils/endpoint";

export interface SiLAJavaFeatureDisplayProps {
    feature: SiLAJavaFeature;
    path: string;
    isEditing?: boolean;
    onRemove?: () => void;
}

export const SiLAJavaFeatureDisplay: React.FC<SiLAJavaFeatureDisplayProps> = ({ feature, path, isEditing }) => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const [xmlValidationError, setXmlValidationError] = useState<string | undefined>();

    function onRemoveElement(fieldPath: string, index: number) {
        dispatch(removeFromArray({ path: [path, fieldPath].join('.'), index: index}));
    }

    function onAddCommand() {
        dispatch(appendToArrayOrAdd({ path: [path, 'command'].join('.'), value: DefaultCommand}));
    }

    function onAddProperty() {
        dispatch(appendToArrayOrAdd({ path: [path, 'property'].join('.'), value: DefaultProperty}));
    }

    function onAddMetadata() {
        dispatch(appendToArrayOrAdd({ path: [path, 'metadata'].join('.'), value: DefaultMetadata}));
    }

    function onAddDefinedExecutionError() {
        dispatch(appendToArrayOrAdd({ path: [path, 'definedExecutionError'].join('.'), value: DefaultDefinedExecutionError}));
    }

    function onAddDataTypeDefinition() {
        dispatch(appendToArrayOrAdd({ path: [path, 'dataTypeDefinition'].join('.'), value: DefaultDataTypeDefinition}));
    }

    function onTextChange(fieldPath: string, value: string) {
        dispatch(update({ path: [path, fieldPath].join('.'), value: value }))
    }

    const validateXml = (xml: string) => {
        return fetch(getBackEndpoint('/api/xml/validate/feature'), {
            method: 'POST',
            headers: {
                'Content-Type': 'text/plain', // or 'application/xml'
            },
            body: xml,
        })
            .then(response => response.text())
            .then(data => {
                if (data === "true") {
                    setXmlValidationError(undefined);
                    return undefined;
                } else {
                    throw new Error(data)
                }
            })
            .catch(error => {
                setXmlValidationError(error?.toString?.() || "Unknown error");
                return error;
            });
    };

    return (
        <Card>
            <CardContent>
                <Grid container spacing={3} mb={2}>
                    {[
                        { primary: "Feature Name", secondaryKey: "displayName", multiline: false, main: true },
                        { primary: "Identifier", secondaryKey: "identifier", multiline: false },
                        { primary: "Locale", secondaryKey: "locale", multiline: false },
                        { primary: "SiLA2 Version", secondaryKey: "siLA2Version", multiline: false },
                        { primary: "Maturity Level", secondaryKey: "maturityLevel", multiline: false },
                        { primary: "Originator", secondaryKey: "originator", multiline: false },
                        { primary: "Category", secondaryKey: "category", multiline: false },
                        { primary: "Description", secondaryKey: "description", multiline: true, xs: 12, sm: 12, md: 12 },
                    ].map((item, idx) => (
                        <Grid item xs={item.xs || 12} sm={item.sm || 6} md={item.md || 3} key={item.secondaryKey}>
                            <Card
                                elevation={3}
                                variant={'elevation'}
                                sx={{
                                    ...(!item.main ? {} : {
                                        backgroundColor: alpha(theme.palette.primary.main, 0.7),
                                        color: theme.palette.primary.contrastText,
                                    }),
                                    height: '100%'
                                }}
                            >
                                <CardContent>
                                    <EditableListItemText
                                        isEditing={isEditing}
                                        primary={item.primary}
                                        multiline={item.multiline}
                                        secondary={feature[item.secondaryKey as keyof SiLAJavaFeature] as string}
                                        editType={'secondary'}
                                        onTextChange={(s) => onTextChange(item.secondaryKey, s)}
                                    />
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                {((!!feature.command?.length || isEditing) && (
                    <Accordion elevation={2}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="h6">Commands</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(feature.command || []).map((cmd, index) => (
                                    <SiLAJavaCommandDisplay
                                        definedExecutionErrors={feature.definedExecutionError}
                                        onRemove={() => onRemoveElement('command', index)}
                                        isEditing={isEditing}
                                        path={[path, `command[${index}]`].join('.')}
                                        key={cmd.identifier + index}
                                        dataTypeDefinition={feature.dataTypeDefinition}
                                        command={cmd}
                                    />
                                ))}
                            </List>
                            {(isEditing && (
                                <Button variant={'contained'} color={'success'} onClick={onAddCommand}>Add Command</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}

                {((!!feature.property?.length || isEditing) && (
                    <Accordion elevation={2}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="h6">Properties</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(feature.property || []).map((prop, index) => (
                                    <SiLAJavaPropertyDisplay
                                        onRemove={() => onRemoveElement('property', index)}
                                        isEditing={isEditing}
                                        path={[path, `property[${index}]`].join('.')}
                                        key={prop.identifier + index}
                                        dataTypeDefinition={feature.dataTypeDefinition}
                                        property={prop}
                                    />
                                ))}
                            </List>
                            {(isEditing && (
                                <Button variant={'contained'} color={'success'} onClick={onAddProperty}>Add Property</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}

                {((!!feature.metadata?.length || isEditing) && (
                    <Accordion elevation={2}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="h6">Metadatum</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(feature.metadata || []).map((meta, index) => (
                                    <SiLAJavaMetadataDisplay
                                        onRemove={() => onRemoveElement('metadata', index)}
                                        isEditing={isEditing}
                                        path={[path, `metadata[${index}]`].join('.')}
                                        key={meta.identifier + index}
                                        definedExecutionErrors={feature.definedExecutionError}
                                        dataTypeDefinition={feature.dataTypeDefinition}
                                        metadata={meta}
                                    />
                                ))}
                            </List>
                            {(isEditing && (
                                <Button variant={'contained'} color={'success'} onClick={onAddMetadata}>Add Metadata</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}

                {((!!feature.definedExecutionError?.length || isEditing) && (
                    <Accordion elevation={2}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="h6">Defined Execution Errors</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(feature.definedExecutionError || []).map((error, index) => (
                                    <SiLAJavaDefinedExecutionErrorDisplay
                                        definedExecutionErrors={[]}
                                        onRemove={() => onRemoveElement('definedExecutionError', index)}
                                        isEditing={isEditing}
                                        path={[path, `definedExecutionError[${index}]`].join('.')}
                                        key={error.identifier + index}
                                        error={error}
                                    />
                                ))}
                            </List>
                            {(isEditing && (
                                <Button variant={'contained'} color={'success'} onClick={onAddDefinedExecutionError}>Add Defined Execution Error</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}

                {((!!feature.dataTypeDefinition?.length || isEditing) && (
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="h6">Data Type Definitions</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                                {(feature.dataTypeDefinition || []).map((element, index) => (
                                    <SiLAJavaSiLAElementDisplay
                                        onRemove={() => onRemoveElement('dataTypeDefinition', index)}
                                        isEditing={isEditing}
                                        path={[path, `dataTypeDefinition[${index}]`].join('.')}
                                        key={element.identifier + index}
                                        dataTypeDefinition={feature.dataTypeDefinition}
                                        element={element}
                                    />
                                ))}
                            </List>
                            {(isEditing && (
                                <Button variant={'contained'} color={'success'} onClick={onAddDataTypeDefinition}>Add Data Type Definition</Button>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ))}
                {
                    (xmlValidationError && (
                        <Alert severity={'warning'}>
                            {xmlValidationError}
                        </Alert>
                    ))
                }
                <XMLDownloader generateXml={async () => {
                    try {

                        const featureXml = FeatureSerializer.serialize(feature) || '';
                        if (await validateXml(featureXml) !== undefined) {
                            return undefined;
                        }
                        return featureXml;
                    } catch (e) {
                        setXmlValidationError(typeof e === "object" ? JSON.stringify(e) : e?.toString?.() || "Unknown error");
                        return undefined;
                    }
                }}></XMLDownloader>
            </CardContent>
        </Card>
    );
};
