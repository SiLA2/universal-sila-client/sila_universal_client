import React, {useEffect, useState} from 'react';
import {ListItemText, ListItemTextProps, TextField} from '@mui/material';

interface EditableListItemTextProps extends ListItemTextProps {
    onTextChange: (newText: string) => void;
    editType: 'primary' | 'secondary';
    multiline?: boolean;
    isEditing?: boolean;
}

const EditableListItemText: React.FC<EditableListItemTextProps> = ({ primary, secondary, editType, onTextChange, multiline, isEditing: edit, ...props }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [editedText, setEditedText] = useState<string | undefined>( ((editType === 'primary' ? primary : secondary) || '').toString());

    useEffect(() => {
        if (editType === 'primary') {
            setEditedText(primary?.toString?.());
        } else {
            setEditedText(secondary?.toString?.());
        }
    }, [primary, secondary, editType]);

    const handleTextClick = () => {
        if (edit) {
            setIsEditing(true);
        }
    };

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setEditedText(e.target.value);
    };

    const handleTextFieldBlur = () => {
        onTextChange(editedText?.toString?.() || '');
        setIsEditing(false);
    };

    const handleTextFieldKeyDown = (e: React.KeyboardEvent) => {
        if (e.key === 'Enter') {
            onTextChange(editedText?.toString?.() || '');
            setIsEditing(false);
        }
    };

    return isEditing ? (
        <TextField
            value={editedText || 'Enter text'}
            label={editType === 'primary' ? secondary : primary}
            onChange={handleTextChange}
            onBlur={handleTextFieldBlur}
            onKeyDown={handleTextFieldKeyDown}
            rows={multiline ? 6 : 1}
            multiline={multiline}
            autoFocus
            fullWidth
        />
    ) : (
        <ListItemText
            {...props}
            primary={editType === 'primary' ? (editedText || 'Enter text') : primary}
            secondary={editType === 'secondary' ? (editedText || 'Enter text') : secondary}
            sx={{
                ...props.sx,
                whiteSpace: 'pre-wrap',
                '& .MuiTypography-root': {
                    overflowWrap: 'break-word',
                    whiteSpace: 'pre-wrap'
                }
            }}
            onClick={handleTextClick}
        />
    );
};

export default EditableListItemText;