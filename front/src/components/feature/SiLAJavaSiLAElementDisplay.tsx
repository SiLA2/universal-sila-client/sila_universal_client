import React from 'react';
import {
    Card,
    CardContent,
    Button,
    useTheme,
    Grid,
    alpha
} from '@mui/material';
import SiLAJavaDataTypeTypeDisplay from "./SiLAJavaDataTypeTypeDisplay";
import {SiLAJavaSiLAElement} from "../../generated/silaModels";
import EditableListItemText from "./EditableListItemText";
import {useDispatch} from "react-redux";
import {update} from "./store/dynamicSlice";

interface SiLAJavaSiLAElementDisplayProps {
    element: Partial<SiLAJavaSiLAElement>;
    dataTypeDefinition: SiLAJavaSiLAElement[];
    path: string;
    isEditing?: boolean;
    onRemove?: () => void;
}

const SiLAJavaSiLAElementDisplay: React.FC<SiLAJavaSiLAElementDisplayProps> = ({ element, dataTypeDefinition, path, onRemove, isEditing }) => {
    const theme = useTheme();
    const dispatch = useDispatch();

    function onTextChange(fieldPath: string, value: string) {
        dispatch(update({ path: [path, fieldPath].join('.'), value: value }))
    }

    return (
        <Card variant="outlined" sx={{width: '100%'}}>
            <CardContent>
                <Grid container spacing={3} mb={2}>
                    {[
                        { primary: "Name", secondaryKey: "displayName", multiline: false, main: true },
                        { primary: "Identifier", secondaryKey: "identifier", multiline: false },
                        { primary: "Description", secondaryKey: "description", multiline: true, xs: 12, sm: 12, md: 12 },
                    ].map((item, idx) => (
                        <Grid item xs={item.xs || 12} sm={item.sm || 6} md={item.md || 3} key={item.secondaryKey}>
                            <Card
                                elevation={3}
                                variant={'elevation'}
                                sx={{
                                    ...(!item.main ? {} : {
                                        backgroundColor: alpha(theme.palette.primary.main, 0.7),
                                        color: theme.palette.primary.contrastText,
                                    }),
                                    height: '100%'
                                }}>
                                <CardContent>
                                    <EditableListItemText
                                        isEditing={isEditing}
                                        primary={item.primary}
                                        secondary={element[item.secondaryKey as keyof SiLAJavaSiLAElement] as string}
                                        multiline={item.multiline}
                                        editType={'secondary'}
                                        onTextChange={(s) => onTextChange(item.secondaryKey, s)}
                                    />
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
                {(element.dataType && (
                    <SiLAJavaDataTypeTypeDisplay
                        path={[path, 'dataType'].join('.')}
                        isEditing={isEditing}
                        dataTypeDefinition={dataTypeDefinition}
                        dataType={element.dataType}/>
                ))}
                {((onRemove && isEditing) && (
                    <Button color={'warning'} onClick={onRemove}>Delete Element</Button>
                ))}
            </CardContent>
        </Card>
    );
};

export default SiLAJavaSiLAElementDisplay;