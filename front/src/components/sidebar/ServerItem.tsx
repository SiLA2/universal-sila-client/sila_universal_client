import {ListItemButton} from "@mui/material";
import ListItemText from "@mui/material/ListItemText";
import {ListItemIcon, Theme, Tooltip, TooltipProps} from "@mui/material/";
import {NavLink} from "react-router-dom";
import {SxProps} from "@mui/system";
export interface ServerItemProps {
    id?: string;
    pathEnd: string;
    titleEnd: string;
    serverUuid: string;
    serverName: string;
    icon: React.ReactNode;
}

export function ServerItem({ id, pathEnd, titleEnd, serverUuid, serverName, icon}: ServerItemProps): JSX.Element {
    return (
        <ListItemButton
            id={id}
            sx={listItemButtonStyle}
            key={`server/${pathEnd}`}
            component={NavLink}
            to={`/server/${serverUuid}/${pathEnd}`}>
            <Tooltip key={`server/${pathEnd}/tooltip`} {...tooltipProps} title={serverName + ` ${titleEnd}`}>
                <ListItemIcon key={`server/${pathEnd}/icon`} sx={listItemIconStyle}>
                    {icon}
                </ListItemIcon>
            </Tooltip>
            <ListItemText key={`server/${pathEnd}/text`} primary={titleEnd}/>
        </ListItemButton>
    );
}

const listItemButtonStyle: SxProps<Theme> = {
    color: 'white',
    "&.active": {
        background: '#ffffff26',
    }
}

const listItemIconStyle: SxProps<Theme> = {
    color: 'white',
}

const tooltipProps: Partial<TooltipProps> = {
    placement: "right",
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};