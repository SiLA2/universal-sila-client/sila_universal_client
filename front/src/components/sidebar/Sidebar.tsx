import * as React from "react";
import MuiDrawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import {
    HelpCircle,
    Play,
    Settings,
    Tool,
    User,
    ChevronsRight,
    ChevronsLeft,
    Terminal,
    List as ListIcon
} from 'react-feather';
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import {ListItemIcon, Theme, Tooltip, TooltipProps, useTheme} from "@mui/material/";
import {NavLink} from "react-router-dom";
import {getServerColor, getServerInitial} from "../../utils/server";
import {ServerOpenKeyValueState} from "../../store/front/server/open/types";
import {useDispatch, useSelector} from "react-redux";
import {getServerOpen} from "../../store/front/server/open/selector";
import {getServerByIdSelector} from "../../store/sila/manager/server/selector";
import {UscServer} from "../../generated/silaModels";
import logo from "../../assets/image/logo.png";
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import {getAppSettings} from "../../store/front/app/setting/selector";
import {setAppThemeMode} from "../../store/front/app/setting/actions";
import {ClosedSidebarWidth, ExpandedSideBarWidth} from "../ThemeProviderComponent";
import ServerLogoBadge from "../common/ServerLogoBadge";
import {AppState} from "../../store/rootReducer";
import {SxProps} from "@mui/system";
import {ListItemButton} from "@mui/material";
import {NavItem} from "./NavItem";
import {ServerItem} from "./ServerItem";
import PluginsComponentRenderer from "../../plugin/PluginsComponentRenderer";

export interface SidebarProps {
    open: boolean,
    setOpen: React.Dispatch<React.SetStateAction<boolean>>,
    hidden: boolean
}

export default function Sidebar(props: SidebarProps): JSX.Element {
    const theme = useTheme();
    const dispatch = useDispatch();
    const appSettings = useSelector(getAppSettings);
    const serverOpen: ServerOpenKeyValueState = useSelector(getServerOpen);
    const currentServer: UscServer | undefined = useSelector((state: AppState) => getServerByIdSelector(state, serverOpen.serverUuidOpen || ''))
    const serverInitials = getServerInitial(currentServer?.name || 'NA');
    const baseItems = [
        { path: "/help", title: "Help", icon: <HelpCircle /> },
        { path: "/settings", title: "Settings", icon: <Settings /> },
        { path: "/tools", title: "Tools", icon: <Tool /> },
    ];

    const advancedUserItems = [
        { path: "/dev", title: "Dev", icon: <Terminal /> },
        { path: "/workspace", title: "Workspace", icon: <User /> }
    ];

    const handleThemeSwitch = () => {
        dispatch(setAppThemeMode({darkMode: !appSettings.darkMode}))
    };

    const handleDrawerSwitch = () => {
        props.setOpen((prev: boolean) => !prev);
    };

    return (
            <MuiDrawer variant="permanent" open={props.open} sx={styledDrawer(props.open, props.hidden)}
                       PaperProps={{sx: {borderRight: 0, background: theme.palette.primary.main, color: 'white'}}}>
                <List sx={{paddingTop: '7px', paddingBottom: 0}}>
                    <ListItemButton
                        sx={{...listItemButtonStyle, margin: 0, padding: 0, paddingTop: '7px', paddingLeft: '13px'}}
                        onClick={() => {props.setOpen(!props.open)}}
                        key={'server/info'} component={NavLink} to={'/dashboard'}>
                        <ListItemIcon sx={{...listItemIconStyle, height: '41px'}}>
                            <img style={{height: '32px', width: '32px'}} src={logo} alt={"Universal SiLA Client Logo"}/>
                        </ListItemIcon>
                        <ListItemText primary={'Universal SiLA Client'}/>
                    </ListItemButton>
                </List>
                {currentServer && (
                    <>
                        <Divider/>
                        <List>
                            <ServerItem
                                id="sidebar-current-server-configuration"
                                pathEnd="info"
                                titleEnd="Configuration"
                                icon={
                                    <ServerLogoBadge
                                        sx={{transform: 'translate(-8px, 0px)'}}
                                        status={currentServer.status}
                                        serverInitials={serverInitials}
                                        serverColor={getServerColor(currentServer.serverUuid)}/>
                                }
                                serverName={currentServer.name}
                                serverUuid={currentServer.serverUuid}
                            />
                            <ServerItem
                                id="sidebar-current-server-properties"
                                pathEnd="property"
                                titleEnd="Properties"
                                icon={<ListIcon/>}
                                serverName={currentServer.name}
                                serverUuid={currentServer.serverUuid}
                            />
                            <ServerItem
                                id="sidebar-current-server-commands"
                                pathEnd="command"
                                titleEnd="Commands"
                                icon={<Play/>}
                                serverName={currentServer.name}
                                serverUuid={currentServer.serverUuid}
                            />
                            <PluginsComponentRenderer componentName={'PluginSidebarServerItem'} forwardProps={{
                                currentServer: currentServer
                            }}/>
                        </List>
                    </>
                )}
                <Divider/>
                <List>
                    {baseItems.map(item => (
                        <NavItem key={item.path} {...item} />
                    ))}
                    {appSettings.isAdvancedUser && advancedUserItems.map(item => (
                        <NavItem key={item.path} {...item} />
                    ))}
                </List>
                <Divider/>
                <List>
                    <ListItemButton sx={listItemButtonStyle} key={'chevron'} onClick={handleThemeSwitch}>
                        <Tooltip {...tooltipProps} title={'Switch theme'}>
                            <ListItemIcon sx={listItemIconStyle}>
                                {!appSettings.darkMode ? (
                                    <Brightness4Icon/>
                                ) : (
                                    <Brightness7Icon/>
                                )}
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary={(appSettings.darkMode ? ('Dark') : ('Light')) + ' mode'}/>
                    </ListItemButton>
                    <PluginsComponentRenderer componentName={'PluginSidebarNavItem'} forwardProps={{
                        currentServer: currentServer
                    }}/>
                </List>
                <Divider/>
                <List>
                    <ListItemButton sx={listItemButtonStyle} key={'chevron'} onClick={handleDrawerSwitch}>
                        <Tooltip {...tooltipProps} title={'Open sidebar'}>
                            <ListItemIcon sx={listItemIconStyle}>
                                {!props.open ? (
                                    <ChevronsRight/>
                                ) : (
                                    <ChevronsLeft/>
                                )}
                            </ListItemIcon>
                        </Tooltip>
                        <ListItemText primary={'Collapse sidebar'}/>
                    </ListItemButton>
                </List>
            </MuiDrawer>
    );
}

const openedMixin = (theme: Theme): SxProps<Theme> => ({
    width: ExpandedSideBarWidth,
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
    }),
    overflowX: "hidden"
});

const closedMixin = (theme: Theme, hidden: boolean): SxProps<Theme> => ({
    transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: hidden ? 0 : ClosedSidebarWidth,
});

const getDrawableStyle: (theme: Theme, open: boolean, hidden: boolean) => SxProps<Theme> = (theme, open, hidden) => {
    if (open) {
        return {
            ...openedMixin(theme),
            "& .MuiDrawer-paper": openedMixin(theme)
        }
    }
    return {
        ...closedMixin(theme, hidden),
        "& .MuiDrawer-paper": closedMixin(theme, hidden)
    };
}

const styledDrawer: (open: boolean, hidden: boolean) => SxProps<Theme> = (open, hidden) => ((theme) => {
    const style: SxProps<Theme> = {
        width: ExpandedSideBarWidth,
        flexShrink: 0,
        whiteSpace: "nowrap",
        boxSizing: "border-box",
        ...getDrawableStyle(theme, open, hidden)
    };

    return {
        ...style
    }
});

const listItemButtonStyle: SxProps<Theme> = {
    color: 'white',
    "&.active": {
        background: '#ffffff26',
    }
}

const listItemIconStyle: SxProps<Theme> = {
    color: 'white',
}

const tooltipProps: Partial<TooltipProps> = {
    placement: "right",
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};

