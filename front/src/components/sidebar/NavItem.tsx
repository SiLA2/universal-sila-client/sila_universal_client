import {ListItemButton} from "@mui/material";
import ListItemText from "@mui/material/ListItemText";
import {ListItemIcon, Theme, Tooltip, TooltipProps} from "@mui/material/";
import {NavLink} from "react-router-dom";
import {SxProps} from "@mui/system";
export interface NavItemProps {
    path: string;
    title: string;
    icon: React.ReactNode;
}

export function NavItem({ path, title, icon }: NavItemProps): JSX.Element {
    return (
        <ListItemButton sx={listItemButtonStyle} key={path + '/item-button'} component={NavLink} to={path}>
            <Tooltip key={path + '/tooltip'} {...tooltipProps} title={title}>
                <ListItemIcon key={path + '/list-icon'} sx={listItemIconStyle}>
                    {icon}
                </ListItemIcon>
            </Tooltip>
            <ListItemText key={path + '/list-text'} primary={title}/>
        </ListItemButton>
    );
};

const listItemButtonStyle: SxProps<Theme> = {
    color: 'white',
    "&.active": {
        background: '#ffffff26',
    }
}

const listItemIconStyle: SxProps<Theme> = {
    color: 'white',
}

const tooltipProps: Partial<TooltipProps> = {
    placement: "right",
    componentsProps: {
        tooltip: {
            sx: {
                fontSize: '1rem'
            }
        }
    }
};

