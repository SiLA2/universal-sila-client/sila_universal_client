export function getServerInitial(name: string): string {
    return (getServerInitialCustom(name, 3));
}

export function getServerInitialCustom(name: string, nbInitial: number): string {
    return name.split(' ').map(i => i.charAt(0)).join('').toUpperCase().substr(0, nbInitial);
}

export function getFullyQualifiedCallId(call: {serverUuid: string, fullyQualifiedFeatureId: string | undefined, commandIdentifier: string | undefined}): string {
    return call.serverUuid + '/' + (call.fullyQualifiedFeatureId || '') + '/' + (call.commandIdentifier || '');
}

export function getServerColor(uuid: string): string {
    const serverColors = [
        "#ff000059",
        "#00ff0059",
        "#0000ff59",
        "#ffff0059",
        "#ff00ff59",
        "#00ffff59",
        "#7f7f7f59",
        "#ff7f0059",
        "#7fff0059",
        "#ff007f59",
        "#7f00ff59",
        "#00ff7f59",
        "#7f7f0059",
        "#007fff59",
        "#ff7f7f59",
        "#7f7f7f59"
    ];
    let hash = 0;
    for (let i = 0; i < uuid.length; i++) {
        hash = uuid.charCodeAt(i) + ((hash << 5) - hash);
    }
    return serverColors[hash % serverColors.length];
}
