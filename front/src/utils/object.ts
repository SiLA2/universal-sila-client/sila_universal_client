// Compact arrays with null entries; delete keys from objects with null value
import {attributesToCamelCase, toPascalCase} from "./string";

export function removeNulls(obj: any): any {
    const isArray = obj instanceof Array;
    for (const k in obj){
        if (obj[k]===null) {
            isArray ? obj.splice(k as any as number, 1) : delete obj[k];
        } else if (typeof obj[k]=="object") {
            removeNulls(obj[k]);
        }
    }
    return obj;
}

export type AnyObject = { [key: string]: any };

export function convertKeysToPascalCase(obj: AnyObject): AnyObject {
    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    if (Array.isArray(obj)) {
        return obj.map(item => convertKeysToPascalCase(item));
    }

    return Object.keys(obj).reduce((acc: AnyObject, key) => {
        const pascalKey = toPascalCase(key);
        acc[pascalKey] = convertKeysToPascalCase(obj[key]);
        return acc;
    }, {});
}

export function cleanObject<T>(obj: T): Partial<T> {
    if (Array.isArray(obj)) {
        return obj
            .map(v => (v && typeof v === 'object' ? cleanObject(v) : v))
            .filter(v => v !== null && v !== undefined) as unknown as Partial<T>;
    } else {
        return Object.entries(obj || {}).reduce((acc, [key, value]) => {
            if (value === null || value === undefined) return acc; // Skip null/undefined values
            return {
                ...acc,
                [key]: value && typeof value === 'object' ? cleanObject(value) : value,
            };
        }, {}) as Partial<T>;
    }
}

export function convertKeyAttributesToCamelCase<T extends Record<string, any>>(obj: T, keys: (keyof T)[]): void {
    keys.forEach(key => {
        const keyStr = key as string;
        if (!keyStr.includes(':')) {
            return;
        }
        const camelCaseAttribute = attributesToCamelCase(keyStr);

        if (keyStr !== camelCaseAttribute && keyStr in obj) {
            obj[camelCaseAttribute as keyof T] = obj[key];
            delete obj[keyStr];
        }
    });
}

