import {getBackUrl} from "./localstorage";

export function getBackEndpoint(endpoint: string): string {
    // todo clean potential double dash
    let backUrl = getBackUrl();

    if (backUrl) {
        try {
            const url = new URL(backUrl);
            if (endpoint.length > 0 && endpoint.charAt(0) === '/') {
                return url.origin + endpoint;
            }
            return url.origin + '/' + endpoint;
        } catch (e) {
            console.log('Provided back url is invalid ' + backUrl + ' using fallback url instead.')
        }
    }
    if (endpoint.length > 0 && endpoint.charAt(0) === '/') {
        return endpoint;
    }
    return '/' + endpoint;
}

export function getWebsocketEndpoint(): string {
    return getBackEndpoint('websocketsila')
}

export function getPluginEndpoint(jsEntryPoint: string): string {
    return getBackEndpoint('/plugins/' + jsEntryPoint)
}