import {UscFeature} from "../generated/silaModels";

export function sortCoreFeatureVersionDesc(
    features: UscFeature[],
    relativeIdentifier: string,
    featureVersions: string[]
): UscFeature[] {
    return features
        .filter(
            (f) =>
                (f.category === 'core' || f.category === 'core.commands') &&
                f.originator === 'org.silastandard' &&
                featureVersions.includes(f.featureVersion) &&
                f.relativeIdentifier === relativeIdentifier
        )
        .sort((a, b) => parseFloat(b.featureVersion) - parseFloat(a.featureVersion));
}

export function findHighestCoreFeatureVersion(
    features: UscFeature[],
    relativeIdentifier: string,
    featureVersions: string[]
): UscFeature | undefined {
    return sortCoreFeatureVersionDesc(features, relativeIdentifier, featureVersions)?.[0];
}
