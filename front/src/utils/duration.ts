import {Duration} from "luxon";
import {ProtoDuration} from "../generated/SiLAFramework";
import {DurationUnit} from "luxon/src/duration";


export function protoDurationDatetime(protoTs: ProtoDuration): Duration {
    try {
        return Duration.fromObject({
            second: protoTs.seconds || 0,
            millisecond: Math.round((protoTs.nanos || 0) / 1000000),
        });
    } catch (e: any) {
        return Duration.invalid(e);
    }
}

export function toHuman(dur: Duration, smallestUnit: DurationUnit = "seconds"): string {
    const units: DurationUnit[] = ["years", "months", "days", "hours", "minutes", "seconds", "milliseconds", ];
    const smallestIdx = units.indexOf(smallestUnit);
    const entries = Object.entries(
        dur.shiftTo(...units).normalize().toObject()
    ).filter(([_unit, amount], idx) => amount > 0 && idx <= smallestIdx);
    const dur2 = Duration.fromObject(
        entries.length === 0 ? { [smallestUnit]: 0 } : Object.fromEntries(entries)
    );
    return dur2.toHuman();
}