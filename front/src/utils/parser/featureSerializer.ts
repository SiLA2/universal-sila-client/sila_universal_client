import xmlFormat from 'xml-formatter';
import { SiLAJavaFeature } from '../../generated/silaModels';
import {AnyObject, cleanObject, convertKeysToPascalCase} from "../object";
import {toPascalCase} from "../string";
const { XMLBuilder} = require("fast-xml-parser");
export default class FeatureSerializer {
    static readonly header = '<?xml version="1.0" encoding="utf-8"?>\n';

    static serialize(feature: SiLAJavaFeature): string {
        const {
            category,
            featureVersion,
            maturityLevel,
            originator,
            siLA2Version,
            locale,
            ...featureWithoutAttributes
        } = cleanObject(feature);

        const featureWithAttributes = {
            ...featureWithoutAttributes,
            '@_Category': category,
            '@_FeatureVersion': featureVersion,
            '@_MaturityLevel': maturityLevel,
            '@_Originator': originator,
            '@_SiLA2Version': siLA2Version,
            '@_Locale': locale,
            '@_xmlns': (featureWithoutAttributes as any)['@_xmlns'] || "http://www.sila-standard.org",
            '@_xmlns:xsi': (featureWithoutAttributes as any)['@_xmlns:xsi'] || "http://www.w3.org/2001/XMLSchema-instance",
            '@_xsi:schemaLocation': (featureWithoutAttributes as any)['@_xsi:schemaLocation'] || "http://www.sila-standard.org https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd"
        }
        const builder = new XMLBuilder({
            ignoreAttributes : false,
            ignoreNameSpace : false,
            allowBooleanAttributes : false,
            parseNodeValue : true,
            parseAttributeValue : true,
            trimValues: true,
            parseTrueNumberOnly: true,
            arrayMode: false, //"strict".
        });

        const cleanedFormatedFeature = toLowerCaseForBasicType(convertSiLAKeysToPascalCase(featureWithAttributes));
        console.log(cleanedFormatedFeature);
        const xmlContent = builder.build({
            Feature: cleanedFormatedFeature
        });
        return xmlFormat(FeatureSerializer.header + xmlContent, {
            collapseContent: true
        });
    }
}

function toLowerCaseForBasicType(obj: any): any {
    if (Array.isArray(obj)) {
        return obj.map(toLowerCaseForBasicType);
    } else if (typeof obj === 'object' && obj !== null) {
        const newObj: Record<string, any> = {};
        for (const [key, value] of Object.entries(obj)) {
            if (key.toLowerCase() === 'basic' && typeof value === "string") {
                newObj[key] = toPascalCase(value.toLowerCase());
                console.log(newObj[key]);
            } else {
                newObj[key] = toLowerCaseForBasicType(value);
            }
        }
        return newObj;
    }
    return obj;
}

function convertSiLAKeysToPascalCase(obj: AnyObject): AnyObject {
    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    if (Array.isArray(obj)) {
        return obj.map(item => convertKeysToPascalCase(item));
    }

    return Object.keys(obj).reduce((acc: AnyObject, key) => {
        if (key.startsWith('@_')) {
            acc[key] = obj[key];
        } else {
            const pascalKey = toPascalCase(key);
            acc[pascalKey] = convertKeysToPascalCase(obj[key]);
        }

        return acc;
    }, {});
}