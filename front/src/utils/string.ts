export function stringToCamelCase(str: string): string {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match: string, index: number) => {
        if (+match === 0) {
            return ""; // or if (/\s+/.test(match)) for white spaces
        }
        return index === 0 ? match.toLowerCase() : match.toUpperCase();
    });
}

export function toPascalCase(str: string): string {
    return str.replace(/(^\w|-\w)/g, matches => matches.charAt(matches.length - 1).toUpperCase()).replace(/-/g, '');
}

export function attributesToCamelCase(str: string): string {
    // First, handle any special prefix characters that should not be altered
    const specialPrefixMatch = str.match(/^[@_]+/);
    const specialPrefix = specialPrefixMatch ? specialPrefixMatch[0] : '';

    // Remove the special prefix before processing the camel casing
    const stringWithoutPrefix = specialPrefix ? str.slice(specialPrefix.length) : str;

    return (
        specialPrefix +
        stringWithoutPrefix
            .split(':') // split by colon to handle XML namespaces
            .map((part, index) => {
                if (index === 0) return part; // Do not change the first part before any colon

                // Split by underscores, hyphens, and whitespace, and camelCase each segment
                return part.replace(/[_-].|^\w/g, (match, offset) =>
                    offset > 0 ? match.toUpperCase() : match.toLowerCase()
                );
            })
            .join(':') // rejoin with colon
    );
}