type NotificationType = 'error' | 'warning' | 'info' | 'success';

interface ServerInfo {
    host: string;
    port: number;
    certificate: string | null;
}

interface Notification {
    type: NotificationType;
    message: string;
    serverInfo?: ServerInfo;
}

class NotificationService {
    private static instance: NotificationService;
    private listeners: ((notification: Notification) => void)[] = [];

    private constructor() {}

    public static getInstance(): NotificationService {
        if (!NotificationService.instance) {
            NotificationService.instance = new NotificationService();
        }
        return NotificationService.instance;
    }

    public addListener(listener: (notification: Notification) => void): void {
        this.listeners.push(listener);
    }

    public removeListener(listener: (notification: Notification) => void): void {
        this.listeners = this.listeners.filter(l => l !== listener);
    }

    public showNotification(type: NotificationType, message: string, serverInfo?: ServerInfo): void {
        const notification: Notification = { type, message, serverInfo };
        this.listeners.forEach(listener => listener(notification));
    }
}

export default NotificationService.getInstance();