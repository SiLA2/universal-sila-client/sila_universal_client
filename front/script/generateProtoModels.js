const protoToTs = require('@pbts/core');
const fs = require('fs');
const path = require('path');

const protoFolder = path.dirname(__dirname) + '/src/generated/';
const tsOutFolder = protoFolder;
const protoDirectory = fs.readdirSync(protoFolder);
const protoFiles = protoDirectory.filter((elm) => (elm.match(/.*\.(proto?)/ig)));
let silaFrameWorkInclude = '';
let binaryTransferInclude = '';

function convertProtoToTs(filePath) {
    const fileName = path.basename(filePath);
    const relativeFilePath = path.relative(protoFolder, filePath);
    const tsFilePath = path.join(tsOutFolder, relativeFilePath.replace(/\.proto$/, '.ts'));
    const folderDepth = Math.max(relativeFilePath.split(path.sep).length - 1, 0);
    console.log('Converting ' + relativeFilePath + ' to typescript model')
    const fileContent = fs.readFileSync(filePath).toString();
    const pathToRoot = "./" + "../".repeat(folderDepth);
    let tsProtoModel = protoToTs.parseProto(fileContent.replace(/ int64 /g, ' int32 '), {isParamOptional: true});

    if (fileName !== 'SiLAFramework.proto') {
        tsProtoModel = tsProtoModel
            .replace(/sila2\.org\.silastandard.*?\.(?=[A-Z])/gm, 'Proto')
    }
    tsProtoModel = tsProtoModel
        .replace(/SILA/g, 'SiLA')
        .replace(/export enum .?(?=[A-Z])/gm, 'export enum Proto')
        .replace(/export interface ?(?=[A-Z])/gm, 'export interface Proto')
        .replace(/(?=:).*?[a-z]\./gm, ': Proto')
        .replace(/\b(?!Proto)(?!Promise<|[a-z])(?!.*\?:+)(.[a-z].*?)(?=[)>;])/gm, 'Proto$1')
    if (fileName === 'SiLAFramework.proto') {
        const iterator = tsProtoModel.matchAll(/Proto\w+(?= {)/gm);
        const exported = [];
        for (let regExpMatchArray of iterator) {
            exported.push(regExpMatchArray[0]);
        }
        silaFrameWorkInclude = `import {${exported.filter(i => i !== 'ProtoErrorType').join(', ')}} from \'./SiLAFramework\';\n\n`;
    }
    if (fileName === 'SiLABinaryTransfer.proto') {
        const iterator = tsProtoModel.matchAll(/Proto\w+(?= {)/gm);
        const exported = [];
        for (let regExpMatchArray of iterator) {
            exported.push(regExpMatchArray[0]);
        }
        binaryTransferInclude = `import {${[...new Set(exported)].join(', ')}} from \'./SiLABinaryTransfer\';\n\n`;
    }
    let contentToWrite = tsProtoModel;
    if (fileName !== 'SiLAFramework.proto') {
        contentToWrite = silaFrameWorkInclude.replace('./SiLAFramework', `${pathToRoot}SiLAFramework`) + contentToWrite;
    }
    if (fileName === 'SiLACloudConnector.proto') {
        contentToWrite = binaryTransferInclude.replace('./SiLABinaryTransfer', `${pathToRoot}SiLABinaryTransfer`) + contentToWrite;
    }
    contentToWrite = '/* tslint:disable */\n' + '/* eslint-disable */\n' + contentToWrite;

    fs.writeFileSync(
        tsFilePath,
        contentToWrite
    );
}

function processFolder(folderPath) {
    const files = fs.readdirSync(folderPath);

    files.forEach((fileName) => {
        const filePath = path.join(folderPath, fileName);
        const stats = fs.statSync(filePath);

        if (stats.isDirectory()) {
            processFolder(filePath); // Recursively process subfolders
        } else if (stats.isFile() && fileName.endsWith('.proto')) {
            convertProtoToTs(filePath);
        }
    });
}

// they need to be processed in the following order to generate the imports
convertProtoToTs(protoFolder + 'SiLAFramework.proto');
convertProtoToTs(protoFolder + 'SiLABinaryTransfer.proto');
convertProtoToTs(protoFolder + 'SiLACloudConnector.proto');
processFolder(protoFolder + 'sila2');