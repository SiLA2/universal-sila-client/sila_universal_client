const fs = require('fs');
const path = require('path');

const libPath = path.resolve(__dirname, '../lib/');
const rootPath = path.resolve(__dirname, '../');

fs.copyFileSync(rootPath + '/src/generated/silaModels.d.ts', libPath + "/generated/silaModels.d.ts", );
