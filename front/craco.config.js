const CracoSwcPlugin = require('craco-swc');
const logWebpackConfigPlugin = require("./craco-plugin-webpack-config");
const overrideJestConfig = require("./craco-plugin-jest-config");
const path = require("path");

// Array of dependencies that need to be resolved from outside src/
const externalDependencies = ['redux', 'react-redux', 'redux-saga', 'reselect', 'axios', 'web-vitals', '@reduxjs/toolkit'];

module.exports = {
    webpack: {
        configure: (webpackConfig, { env, paths }) => {
            // Allow importing from outside of src directory
            webpackConfig.resolve.plugins = webpackConfig.resolve.plugins.filter(
                (plugin) => plugin.constructor.name !== 'ModuleScopePlugin'
            );

            // Add aliases for external dependencies
            const aliases = externalDependencies.reduce((acc, dep) => {
                acc[dep] = path.resolve(__dirname, `node_modules/${dep}`);
                return acc;
            }, {});

            webpackConfig.resolve.alias = {
                ...webpackConfig.resolve.alias,
                ...aliases
            };

            // Log the resolved paths for our external dependencies
            externalDependencies.forEach(dep => {
                console.log(`${dep} resolved to:`, webpackConfig.resolve.alias[dep]);
            });

            return webpackConfig;
        }
    },
    plugins: [
        { plugin: logWebpackConfigPlugin, options: { preText: "Will extend the webpack config:" } },
        { plugin: overrideJestConfig, options: { preText: "Will extend the jest config:" } },
        {
            plugin: CracoSwcPlugin,
            options: {
                swcLoaderOptions: {
                    jsc: {
                        externalHelpers: true,
                        target: 'es2017',
                        transform: {
                            react: {
                                runtime: 'automatic'
                            }
                        },
                        loose: false,
                        // Requires v1.2.50 or upper and requires target to be es2016 or upper.
                        keepClassNames: false,
                        parser: {
                            syntax: 'typescript',
                            jsx: true,
                            dynamicImport: true,
                            exportDefaultFrom: true,
                            functionBind: false,
                            exportNamespaceFrom: false,
                            decorators: false,
                            decoratorsBeforeExport: false,
                            topLevelAwait: false,
                            importMeta: false,
                            privateMethod: false
                        },
                    },
                    minify: true
                },
            },
        }
    ],
};