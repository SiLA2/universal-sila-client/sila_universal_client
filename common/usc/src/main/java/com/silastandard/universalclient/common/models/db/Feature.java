package com.silastandard.universalclient.common.models.db;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.silastandard.universalclient.common.models.dto.DataTypeDefinition;
import com.silastandard.universalclient.common.models.dto.DefinedExecutionError;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

// todo: look at @ToString
@Builder
@Entity
@Table(name="FEATURE")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@TypeDef(name = "json", typeClass = JsonType.class)
//@EqualsAndHashCode(onlyExplicitlyIncluded = true) // can't be implemented because ID is assigned by DB
public class Feature {

    public static Feature fromSiLA(
            @NonNull final Server server,
            @NonNull final sila_java.library.core.models.Feature silaFeature
    ) {
        final Feature persistentFeature =
                Feature.builder()
                        .server(server)
                        .featureVersion(silaFeature.getFeatureVersion())
                        .relativeIdentifier(silaFeature.getIdentifier())
                        .originator(silaFeature.getOriginator())
                        .locale(silaFeature.getLocale())
                        .displayName(silaFeature.getDisplayName())
                        .maturityLevel(silaFeature.getMaturityLevel())
                        .category(silaFeature.getCategory())
                        .sila2Version(silaFeature.getSiLA2Version())
                        .description(silaFeature.getDescription())
                        .build();
        persistentFeature.setDefinedExecutionErrors(
                silaFeature.getDefinedExecutionError()
                        .stream()
                        .map(DefinedExecutionError::fromSiLA)
                        .collect(Collectors.toSet())
        );
        persistentFeature.setDataTypeDefinitions(
                silaFeature.getDataTypeDefinition()
                        .stream()
                        .map(DataTypeDefinition::fromSiLA)
                        .collect(Collectors.toSet())
        );
        persistentFeature.setCommands(
                silaFeature.getCommand()
                        .stream()
                        .map((command) -> Command.fromSiLA(persistentFeature, command))
                        .collect(Collectors.toSet())
        );
        persistentFeature.setProperties(
                silaFeature.getProperty()
                        .stream()
                        .map((property) -> Property.fromSiLA(persistentFeature, property))
                        .collect(Collectors.toSet())
        );
        persistentFeature.setMetadatas(
                silaFeature.getMetadata()
                        .stream()
                        .map((metadata) -> Metadata.fromSiLA(persistentFeature, metadata))
                        .collect(Collectors.toSet())
        );
        return persistentFeature;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ID", unique=true, nullable=false)
    @EqualsAndHashCode.Include
    private long id;

    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY ,cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    private Server server;

    @Column(name="RELATIVE_IDENTIFIER", nullable=false)
    private String relativeIdentifier;

    @Column(name="DISPLAY_NAME", nullable=false)
    private String displayName;

    @Column(name="DESCRIPTION", nullable=false, columnDefinition="TEXT")
    private String description;

    @Column(name="LOCALE", nullable=false)
    private String locale;

    @Column(name="SILA2_VERSION", nullable=false)
    private String sila2Version;

    @Column(name="FEATURE_VERSION", nullable=false)
    private String featureVersion;

    @Column(name="MATURITY_LEVEL", nullable=false)
    private String maturityLevel;

    @Column(name="ORIGINATOR", nullable=false)
    private String originator;

    @Column(name="CATEGORY", nullable=false)
    private String category;

    @Column(name="EXECUTION_ERRORS", columnDefinition = "JSON")
    @Type(type = "json")
    private Set<DefinedExecutionError> definedExecutionErrors = new HashSet<>(0);

    @Column(name="DATA_TYPE_DEFINITIONS", columnDefinition = "JSON")
    @Type(type = "json")
    private Set<DataTypeDefinition> dataTypeDefinitions = new HashSet<>(0);

    @JsonManagedReference
    @OneToMany(fetch=FetchType.LAZY, mappedBy="feature", orphanRemoval = true, cascade = {javax.persistence.CascadeType.ALL})
    @Cascade({CascadeType.ALL})
    @Fetch(FetchMode.JOIN)
    private Set<Command> commands = new HashSet<>(0);

    @JsonManagedReference
    @OneToMany(fetch=FetchType.LAZY, mappedBy="feature", orphanRemoval = true, cascade = {javax.persistence.CascadeType.ALL})
    @Cascade({CascadeType.ALL})
    @Fetch(FetchMode.JOIN)
    private Set<Metadata> metadatas = new HashSet<>(0);

    @JsonManagedReference
    @OneToMany(fetch=FetchType.LAZY, mappedBy="feature", orphanRemoval = true, cascade = {javax.persistence.CascadeType.ALL})
    @Cascade({CascadeType.ALL})
    @Fetch(FetchMode.JOIN)
    private Set<Property> properties = new HashSet<>(0);

    @Column(name = "FULLY_QUALIFIED_IDENTIFIER", nullable = false, updatable = false)
    private String fullyQualifiedIdentifier;

    private void setFullyQualifiedIdentifier(
            @NonNull final String originator,
            @NonNull final String category,
            @NonNull final String relativeIdentifier,
            @NonNull final String featureVersion
    ) {
        this.fullyQualifiedIdentifier = this.generateFullyQualifiedIdentifier(originator, category, relativeIdentifier, featureVersion);
    }

    private String generateFullyQualifiedIdentifier(
            @NonNull final String originator,
            @NonNull final String category,
            @NonNull final String relativeIdentifier,
            @NonNull final String featureVersion
    ) {
        final int endOfMajorVersion = featureVersion.indexOf(".");
        final String majorVersion = (endOfMajorVersion < 0) ? featureVersion : featureVersion.substring(0, endOfMajorVersion);

        return String.join(
                "/" ,
                Arrays.asList(originator, category, relativeIdentifier, "v" + majorVersion)
        );
    }

    public String getRuntimeFullyQualifiedIdentifier() {
        return this.generateFullyQualifiedIdentifier(originator, category, relativeIdentifier, featureVersion);
    }

    @PrePersist
    @PreUpdate
    @Transactional
    public void updateFullyQualifiedIdentifier() {
        setFullyQualifiedIdentifier(this.originator, this.category, this.relativeIdentifier, this.featureVersion);
    }
}