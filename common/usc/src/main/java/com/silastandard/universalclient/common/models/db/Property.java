package com.silastandard.universalclient.common.models.db;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.json.JsonType;
import lombok.*;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import sila_java.library.core.models.DefinedExecutionErrorList;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

// todo: look at @ToString
@Entity
@Table(name = "PROPERTY")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@TypeDef(name = "json", typeClass = JsonType.class)
//@EqualsAndHashCode(onlyExplicitlyIncluded = true) // can't be implemented because ID is assigned by DB
public class Property {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    @EqualsAndHashCode.Include
    private long id;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, cascade = {javax.persistence.CascadeType.PERSIST})
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "FEATURE_ID")
    private Feature feature;

    @Column(name = "RELATIVE_IDENTIFIER", nullable = false)
    private String relativeIdentifier;

    @Column(name = "DISPLAY_NAME", nullable = false)
    private String displayName;

    @Column(name = "DESCRIPTION", nullable = false, columnDefinition = "TEXT")
    private String description;

    @Column(name = "OBSERVABLE", nullable = false)
    private Boolean observable;

    @Column(name = "EXECUTION_ERRORS", columnDefinition = "JSON")
    @Type(type = "json")
    private DefinedExecutionErrorList definedExecutionErrors;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "property", orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    @JsonManagedReference
    private Set<Call> calls = new HashSet<>(0);

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "property", orphanRemoval = true, cascade = {javax.persistence.CascadeType.ALL})
    @Cascade({CascadeType.ALL})
    @Fetch(FetchMode.JOIN)
    @JsonManagedReference
    private Response response;

    public static Property fromSiLA(
            @NonNull final Feature feature,
            @NonNull final sila_java.library.core.models.Feature.Property silaProperty
    ) {
        Property persistentProperty = new Property();

        persistentProperty.setObservable(
                silaProperty.getObservable() != null &&
                        silaProperty.getObservable().toLowerCase(Locale.ROOT).equals("yes")
        );
        persistentProperty.setDescription(silaProperty.getDescription());
        persistentProperty.setDisplayName(silaProperty.getDisplayName());
        persistentProperty.setRelativeIdentifier(silaProperty.getIdentifier());
        persistentProperty.setFeature(feature);
        persistentProperty.setDefinedExecutionErrors(silaProperty.getDefinedExecutionErrors());
        persistentProperty.setResponse(Response.fromSiLA(persistentProperty, silaProperty.getDataType()));
        return persistentProperty;
    }

    public String getFullyQualifiedIdentifier() {
        return this.feature.getFullyQualifiedIdentifier() + "/Property/" + this.relativeIdentifier;
    }
}