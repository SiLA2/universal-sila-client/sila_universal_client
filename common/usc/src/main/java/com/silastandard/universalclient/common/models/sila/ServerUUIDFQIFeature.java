package com.silastandard.universalclient.common.models.sila;

import lombok.Getter;
import lombok.NonNull;

import java.util.UUID;

@Getter
public class ServerUUIDFQIFeature {
    private final UUID serverUuid;
    private final String originator;
    private final String category;
    private final String featureIdentifier;
    private final String featureMajorVersion;

    public ServerUUIDFQIFeature(
            @NonNull final UUID serverUuid,
            @NonNull final String originator,
            @NonNull final String category,
            @NonNull final String featureIdentifier,
            @NonNull final String featureMajorVersion
    ) {
        if (!featureMajorVersion.startsWith("v")) {
            throw new RuntimeException("Feature Major Version must start with \"v\"");
        }
        this.serverUuid = serverUuid;
        this.originator = originator;
        this.category = category;
        this.featureIdentifier = featureIdentifier;
        this.featureMajorVersion = featureMajorVersion;
    }

    public String getFullyQualifiedFeatureIdentifier() {
        return String.join("/", originator, category, featureIdentifier, featureMajorVersion);
    }
}
