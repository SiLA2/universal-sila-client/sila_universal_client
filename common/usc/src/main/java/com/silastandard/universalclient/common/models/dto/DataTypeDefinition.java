package com.silastandard.universalclient.common.models.dto;

import lombok.*;
import sila_java.library.core.models.DataTypeType;


@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class DataTypeDefinition {
    private String relativeIdentifier;
    private String displayName;
    private String description;
    private DataTypeType dataType;

    public static DataTypeDefinition fromSiLA(
            @NonNull final sila_java.library.core.models.SiLAElement siLAElement
    ) {
        final DataTypeDefinition persistentDataTypeDefinition = new DataTypeDefinition();
        persistentDataTypeDefinition.setDataType(siLAElement.getDataType());
        persistentDataTypeDefinition.setDescription(siLAElement.getDescription());
        persistentDataTypeDefinition.setRelativeIdentifier(siLAElement.getIdentifier());
        persistentDataTypeDefinition.setDisplayName(siLAElement.getDisplayName());
        return persistentDataTypeDefinition;
    }
}
