package com.silastandard.universalclient.common.models.sila;

import com.google.protobuf.DynamicMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sila_java.library.manager.models.SiLACall;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObservableIntermediateResponse {
    SiLACall baseCall;
    DynamicMessage responseType;
}
