package com.silastandard.universalclient.common.models.db;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

// todo: look at @ToString
@Builder
@Entity
@Table(name="SERVER")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Server {

    @Id
    @Column(name="SERVER_UUID", unique=true, nullable=false)
    @EqualsAndHashCode.Include
    private String serverUuid;

    @Column(name="VERSION", nullable=false)
    private String version;

    @Column(name = "JOINED", columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime joined;

    @Column(name = "STATUS", nullable = false, length = 32)
    @Enumerated(EnumType.STRING)
    private ServerStatus status;

    @Column(name = "NEGOTIATION_TYPE", nullable = false, length = 32)
    @Enumerated(EnumType.STRING)
    private sila_java.library.manager.models.Server.NegotiationType negotiationType;

    @Column(name = "CONNECTION_TYPE", nullable = false, length = 32)
    @Enumerated(EnumType.STRING)
    private sila_java.library.manager.models.Server.ConnectionType connectionType;

    @Column(name = "SERVER_HOST", nullable = false, length = 1024)
    private String serverHost;

    @Column(name = "PORT", nullable = false)
    private int port;

    @Column(name="CERTIFICATE_AUTHORITY", nullable=true, columnDefinition="TEXT")
    private String certificateAuthority;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name="SERVER_TYPE", nullable=false)
    private String serverType;

    @Column(name="DESCRIPTION", nullable=false, columnDefinition="TEXT")
    private String description;

    @Column(name="VENDOR_URL", nullable=false, length = 1024)
    private String vendorUrl;

    @JsonManagedReference
    @ManyToMany(mappedBy = "servers", fetch=FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    private Set<Client> clients = new HashSet<>(0);

    @JsonManagedReference
    @OneToMany(fetch=FetchType.LAZY, mappedBy="server", orphanRemoval = true, cascade = {CascadeType.ALL})
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    private Set<Feature> features = new HashSet<>(0);

    public static Server fromSiLA(@NonNull final sila_java.library.manager.models.Server silaServer) {
        // @ Mapping POC
        // todo: refactor to Mappers
        final Server persistentServer = Server.builder()
                .serverHost(silaServer.getHost())
                .serverUuid(silaServer.getConfiguration().getUuid().toString())
                .serverType(silaServer.getInformation().getType())
                .description(silaServer.getInformation().getDescription())
                .joined(LocalDateTime.ofInstant(silaServer.getJoined().toInstant(), ZoneId.systemDefault()))
                .name(silaServer.getConfiguration().getName())
                .negotiationType(silaServer.getNegotiationType())
                .connectionType(silaServer.getConnectionType())
                .port(silaServer.getPort())
                .certificateAuthority(silaServer.getCertificateAuthority())
                .status(ServerStatus.valueOf(silaServer.getStatus().toString()))
                .vendorUrl(silaServer.getInformation().getVendorURL())
                .version(silaServer.getInformation().getVersion())
                .build();
        persistentServer.setFeatures(
                silaServer.getFeatures()
                        .stream()
                        .map((feature) -> Feature.fromSiLA(persistentServer, feature))
                        .collect(Collectors.toSet())
        );
        return persistentServer;
    }
}