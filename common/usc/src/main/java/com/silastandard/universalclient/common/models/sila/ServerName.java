package com.silastandard.universalclient.common.models.sila;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
// todo see if you can't use sila_java equivalent as DTO
public class ServerName {
    private String serverIdentifier;
    private String newServerName;
}
